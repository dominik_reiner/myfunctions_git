""" Functions to Manipulate directories """
import shutil, os, numpy as np, re, pdb, time

def getdirs(mydir, mystr = False, fullpath = False, onlylast = False):
    """
    Get a list of folders or files in a defined directory, all wih a common string
        Arguments:
        ----------
    mydir:          str, dir of interest
    mystr:          str, common string of files. False if none
    fullpath:   bool, whether to attach full directory to file names or not
    onlylast:   bool, whether to return only the last value of the list or not
        Returns:
        --------
    mylist:         np.array type string with all folders or files of interest
    """
    if os.path.isdir(mydir):
        mylist = os.listdir(mydir)
    else:
        mydir = str(input('Dir doesnt exist. Type a valid one: '))
        mylist = os.listdir(mydir) #Here I can use the function to constrain user input
    new_list = []

    #%% If filter names according to a string:
    if mystr:
        for ii in mylist:
            if mystr in ii:
                if fullpath == True:
                    new_list.append(os.path.join(mydir, ii))
                else:
                    new_list.append(ii)
        mylist = np.copy(np.array([new_list]))

    elif not mystr:
        if fullpath == True:
            for ii in mylist:
                new_list.append(os.path.join(mydir, ii))

            mylist = np.copy(np.array([new_list]))

    mylist = mylist.flatten()
    if onlylast == True:
        return mylist[-1], mydir
    elif not onlylast:
        return mylist, mydir

def gendirtree(homedir, proc_idx =-9999, mode='fl_'):
    """
    Create directory tree if does not exist, and return paths as a tuple. If individual
    folders exists, they are not overwritten. Construct also dictionary for better handling the paths
        Arguments:
        ----------
    homedir:        str, host directory
    proc_idx:       float, index of the current realization. Default is -9999 no process
                    folder and its subfolders are created
    mode:           string, either fl_ or tr_. Default 'fl_'
        Returns:
        --------
    Dictionary with the name and their corresponding paths.
    If dirtree was already created it can still create individual processes folders
    """
    assert os.path.isdir(homedir), 'Wrong home directory'
    fmt_string = '%.5d' %(proc_idx)
    fld_real = 'realizations'
    fld_allproc = 'allproc'
    fld_curproc = 'proc_%s' %(fmt_string)
    fld_heads = 'hydheads'
    fld_kkkfiles = 'kkkfiles'
    fld_modObs = 'modobs'
    fld_recdata = 'datarecieved'
    fld_mode = '%s%s'%(mode, fmt_string)
    mypaths = [ os.path.join(homedir, fld_real, fld_allproc),
                os.path.join(homedir, fld_real, fld_allproc, fld_curproc),
                os.path.join(homedir, fld_real, fld_allproc, fld_curproc, fld_mode),
                os.path.join(homedir, fld_real, fld_heads),
                os.path.join(homedir, fld_real, fld_kkkfiles),
                os.path.join(homedir, fld_real, fld_modObs),
                os.path.join(homedir, fld_real, fld_recdata),
                os.path.join(homedir, fld_real, fld_recdata, fld_heads),
                os.path.join(homedir, fld_real, fld_recdata, fld_kkkfiles),
                os.path.join(homedir, fld_real, fld_recdata, fld_modObs)]

    for ii in mypaths:
        if '-9999' in ii:
            pass
        else:
            if not os.path.exists(ii):
                os.makedirs(ii)

    mypath_dict = {}
    mylabels = ['fld_allproc', 'fld_curproc', 'fld_mode', 'fld_heads', 'fld_kkkfiles', \
                'fld_modobs', 'fld_recdata', 'fld_recheads', 'fld_reckkk', 'fld_recmodobs']

    for id, curpath in enumerate(mypaths):
        mypath_dict[mylabels[id]] = curpath

    return mypath_dict

def init_dir(ii, mymode, homedir):
    """
    Initialize directories and dir variables used along the whole process
        Argments:
        ---------
    ii:         int, process id (ii is always python index)
    mymode:     str, 'fl_' or 'tr_'
    homedir:    home directory of the whole project
        Returns:
        --------
    NoReal
    dir_dict
    fmt_string
    kfile
    """
    NoReal = ii + 1                                     # To correct the zero index from python
    dir_dict = gendirtree(homedir, proc_idx = NoReal, mode = mymode) # Get a dict with the directories
    fmt_string = '%.5d' % NoReal                         # Process ID to mantain order
    kfile = 'kkkGaus_%s.dat' % fmt_string                # Name of the current kkk filename
    kkkFile = os.path.join(dir_dict['fld_mode'], kfile)

    return NoReal, dir_dict, fmt_string, kfile

def str_infile(myfile, mystr):
    """
    Find the string that follows a defined string
        Arguments:
        ----------
    myfile:         str, full path of the file of interest
    mystr:          str, string BEFORE the one of interest
        Returns:
        --------
    String after the one given in the arguments
    """
    with open(myfile, 'r') as fp:
        for line in fp:
            match = re.search(mystr+ '([^,]+)', line)
            if match:
                next_str =  match.group(1)
    if next_str:
        return next_str
    else:
        print('Given string not found in file')

def numberfiles(mydir):
    """
    Count the number of files within a directory. If the folder does not exists
    It will be created automatically.
        Arguments:
        ----------
    mydir:      str, full directory location
        Returns:
        --------
    numfiles:   float, number of existing files
    """
    try:
        numfiles = len(os.listdir(mydir))
    except:
        os.makedirs(mydir)
        numfiles = 0
        print('The folder < %s > does not exist and will be created.' % mydir)

    return numfiles

def cpfolder(sourcedir, destdir):
    """ Copy a file or directory
    """
    shutil.copytree(sourcedir, destdir)

##def collectGstatOutput(process):
##    fileOfInterest = 'gstatOutputGauss.dat'
##    myCodeDir = myMainDir()
##    allgstatFields = os.path.join(myCodeDir, 'gstatFields')
##    os.rename(os.path.join(process, fileOfInterest), os.path.join(process, 'gstatOutputGauss%s.dat' % process[-5:]))
##    shutil.move(os.path.join(process, 'gstatOutputGauss%s.dat' % process[-5:]), os.path.join(allgstatFields))
##    shutil.rmtree(process)

def join_data_files(mydir, dim = 2, mystr = False):
    """
    Join datasets from different files to perform any kind of operation
    on all of them (e.g. rotate points)
        Arguments:
        ----------
    mydir:      str, directory where files are stored
    dim:        int, number of dimensions (columns) in the files
    mystr:      str, identifier of files of interest in case the folder contains additional files
                Default = False if all files
        Returns:
    jointdata:              np.array with joint datasets
    [mylist, lengths]:      np.array with the list of files the number of entries of each one
    """
    #%% Hands on the files to work with:
    mylist, mydir = getdirs(mydir, mystr = mystr, fullpath = False, onlylast = False)

    #%% Start loading each file and track the num of entries of each file:
    alldata = np.zeros((1,dim))
    lengths =np.array([])

    for curFile in mylist:

        curdata = np.loadtxt(os.path.join(mydir, curFile))
        alldata = np.concatenate((alldata,curdata))
        lengths = np.append(lengths,curdata.shape[0])

    return np.delete(alldata,0, axis = 0), [mylist, lengths]

def rmfiles(myfiles, fmt_string, process_path):
    """
    Remove unnecessary files to save disk space
        Arguments:
        ----------
    myfiles:        List, strings to identify all files to be KEPT
    fmt_string:     str, process ID
    process_path:   str, directory to perform the deletion
        Returns:
        --------
    mypaths:        str, full path of file(s) of the process in execution, not deleted ????
    """
    # Read all folders within proper process number:
    mypaths, dummydir = getdirs(process_path, mystr = fmt_string, fullpath = True, onlylast = False)
    newFolders, dummydir = getdirs(process_path, mystr = fmt_string, fullpath = False, onlylast = False)
    newFolders = ['_' + s for s in newFolders]

    for jj in range(0,len(mypaths)):

        for curname in myfiles:
            mycurfile, dummydir = getdirs(mypaths[jj], mystr = curname, fullpath = False, onlylast = False)
            temp = os.path.join(mypaths[jj], '..',newFolders[jj])

            if not os.path.exists( temp):
                os.makedirs(temp)

            for ii in mycurfile:
                shutil.copy(os.path.join(mypaths[jj],ii), os.path.join(temp,ii))
    try:
        shutil.rmtree(mypaths[jj])
        time.sleep(1.)
        os.rename(os.path.abspath(temp), mypaths[jj])
    except PermissionError:
        raise
    return mypaths

def readFieldData():
    print('Under construction')


