# Define initial settings:
import os, numpy as np, sys
import pycleaner.miscel as miscel
from myplot import xyplot
import pycleaner.smooth as mysmooth
import pycleaner.rm_outlier as rmout
import pycleaner.menu as menu
import pycleaner.fitfun as fitfun

def procData(mytime, y, cur_dataset):
    #%% Start processing the data:
    flag = 'no'
    time = np.copy(mytime)     # Make a copy to keep safe original times
    value = np.copy(y)      # Make a copy to keep safe original values

    while flag == 'no':
        mytime = np.copy(time)
        yData = np.copy(value)
        # Plot initial stage of data:
        xyplot(time, value, xData_new = '', yData_new = '', mytitle = cur_dataset)
        # Show Process menu:
        recordDevice = menu.procmenu()

        if recordDevice == 0:
            flag = 'yes'
        #--------------#
        # Kalman Filter
        if recordDevice == 1:
            try:
                mytsdev =float( raw_input('Measurement Standard deviation (Default 0.1): ') )
            except:
                mytsdev = 0.1
            myobj = mysmooth.filters(mytime, yData)
            tr_time, tr_yData, error = myobj.kalman(meas_std_dev = mytsdev, out_spread = False, mytitles = cur_dataset, plotting = True)
            flag, time, value = menu.conf_menu(tr_time, tr_yData, mytime, yData)

        ###############################################################################################
        #--------------#
        # Remove signal noise using Butter filter
        elif recordDevice == 2:
            #x = value
            try:
                m =  float(raw_input('Critical frequency (Default 0.3):'))
            except:
                m = 0.3
            myobj = mysmooth.filters(mytime, yData)
            tr_time, tr_yData = myobj.butter(n = m, mytitles = cur_dataset, plotting = True)
            flag, time, value = menu.conf_menu(tr_time, tr_yData,mytime, yData)

        ###############################################################################################
        #--------------#
        # Smoothing using regular window
        elif recordDevice == 3:
            window = menu.check_userinput('Which window? (flat, hanning, hamming, bartlett, blackman, gaus): ', myoptions = ['flat', 'hanning', 'hamming', 'bartlett', 'blackman', 'gaus'])
            if window == 'gaus':
                try:
                    p = float(raw_input('Shape parameter.\n1:identical to gaussian\n0.55:Laplace distribution\n(Default = 1): '))
                except:
                    p = 1
                try:
                    sigma = float(raw_input('Standard deviation (Default = 1): '))
                except:
                    sigma = 1
                myargs = [p, sigma]
            else:
                myargs = ''

            try:
                window_len = int(raw_input('Length of smoothing window (Default 4):'))
            except:
                window_len = 4

            myobjs = mysmooth.windows(mytime, yData, window)
            tr_time, tr_yData = myobjs.reg_window(window_len, mytitles = cur_dataset, plotting = True, iter_args = myargs)
            flag, time, value = menu.conf_menu(tr_time, tr_yData, mytime, yData)

        ###############################################################################################
        #--------------#
        # Smoothing using recursive window
        elif recordDevice == 4:
            window = menu.check_userinput('Which window? (flat, hanning, hamming, bartlett, blackman, gaus): ', myoptions = ['flat', 'hanning', 'hamming', 'bartlett', 'blackman', 'gaus'])
            if window == 'gaus':
                try:
                    p = float(raw_input('Shape parameter.\n1:identical to gaussian\n0.55:Laplace distribution\n(Default = 1): '))
                except:
                    p = 1
                try:
                    sigma = float(raw_input('Standard deviation (Default = 1): '))
                except:
                    sigma = 1
                myargs = [p, sigma]
            else:
                myargs = ''

            try:
                numIter = int(raw_input('Number of iterations (Default 1):'))
            except:
                numIter = 1
            try:
                ini_wind_len = int(raw_input('Initial window length (Default 4):'))
            except:
                ini_wind_len = 4
            try:
                win_len_step = int(raw_input('Step increment for the window length (Default 2):'))
            except:
                win_len_step = 2

            myobjs = mysmooth.windows(mytime, yData, window)
            tr_time, tr_yData, D, error = myobjs.rec_window(numIter, ini_wind_len, win_len_step, mytitles = cur_dataset, plotting = True, iter_argum = myargs)
            flag, time, value = menu.conf_menu(tr_time, tr_yData,mytime, yData)

        ###############################################################################################
        #--------------#
        # Remove outliers using the distance of a point to a fitted function using splines
        elif recordDevice == 5:
            try:
                mymultiplier = float(raw_input('Give a factor for the distance. The smaller the more strict in \ndefining a point as an outlier (default 3.0): '))
            except:
                mymultiplier = 3.0
            try:
                sporder = int(raw_input('Order of the spline (between 1 and 5, default 3): '))
            except:
                sporder = 3
            myobjss = rmout.outlier_detection(mytime, yData)
            tr_time, tr_yData = myobjss.dist2_fit(order = sporder, multiplier = mymultiplier, mytitles = cur_dataset, plotting = True)
            flag, time, value = menu.conf_menu(tr_time, tr_yData,mytime, yData)
        ###############################################################################################
        #--------------#
        # Remove outliers using the distance of a point to a its previous and consecutive points
        elif recordDevice == 6:
            try:
                mymultiplier = float(raw_input('Give a factor for the distance. The smaller the more strict in \ndefining a point as an outlier (default = 3.): '))
            except:
                mymultiplier = 3.0
            myobjss = rmout.outlier_detection(mytime, yData)
            tr_time, tr_yData = myobjss.dist2_yval(multiplier = mymultiplier, mytitles = cur_dataset, plotting = True)
            flag, time, value = menu.conf_menu(tr_time, tr_yData,mytime, yData)
        ###############################################################################################
        #--------------#
        # Distance to mean values (step-wise)
        elif recordDevice == 7:
            try:
                mymultiplier = float(raw_input('Give a factor for the distance. The smaller the more strict in \ndefining a point as an outlier (default 3.): '))
            except:
                mymultiplier = 3.0
            mystepsize = int(raw_input('How big should the stepsize be? Max = len(ydata):'))
            myobjss = rmout.outlier_detection(mytime, yData)
            tr_time, tr_yData = myobjss.dist2_meanstd(multiplier = mymultiplier, step_size = mystepsize, mytitles = cur_dataset, plotting = True )
            flag, time, value = menu.conf_menu(tr_time, tr_yData,mytime, yData)

        ###############################################################################################
        #--------------#
        # Distance to modified Zscores (step-wise)
        elif recordDevice == 8:
            mystepsize = int(raw_input('How big should the stepsize be? Max = len(ydata):'))
            try:
                mythresh = float(raw_input('Define threshold to remove outliers. Default = 3.5:'))
            except:
                mythresh = 3.5
            myobjss = rmout.outlier_detection(mytime, yData)
            tr_time, tr_yData = myobjss.mod_Zscore(step_size = mystepsize, thresh = mythresh, mytitles = cur_dataset, plotting = True)
            flag, time, value = menu.conf_menu(tr_time, tr_yData,mytime, yData)

        ###############################################################################################
        #--------------#
        # Fit the generalized inverse gaussian distribution
        elif recordDevice == 9:
            tr_time, tr_yData = fitfun.call_invgaus(mytime, yData, mytitles = cur_dataset, plotting = True)
            flag, time, value = menu.conf_menu(tr_time, tr_yData,mytime, yData)

        ###############################################################################################
        #--------------#
        # Smooth using spline interpolation and arbitrary anchor points: Seems that it does not work properly with pick events.....
        elif recordDevice == 10:
            try:
                s = float(raw_input('Smoothing factor (Default 0.25):\nCloser to 0 will interpolate closer to data points'))
            except:
                s = 0.25
            #Positive smoothing factor used to choose the number of knots. Number of knots will be increased until the smoothing condition is satisfied:
            #sum((w[i] * (y[i]-spl(x[i])))**2, axis=0) <= s
            tr_time, tr_yData = fitfun.spline_interpArbPoints(mytime, yData, s, mytitles = cur_dataset, plotting = True)
            flag, time, value = menu.conf_menu(tr_time, tr_yData,mytime, yData)

        ###############################################################################################
        #--------------#
        # Get evenly distributed data using splines:
        elif recordDevice == 11:
            try:
                smooth = float( raw_input('Smoothing factor (0 = no smoothing, Default 0):') )
            except:
                smooth = 0
            try:
                degree = float(raw_input('Degree of the smoothing spline (Default 3). \nMust be <= 5: '))
            except:
                degree = 3
            time_array = raw_input('New time series? \nNo: empty\yes: initial,final+timestep, timestep ')
            try:
                time_array = np.arange(float(time_array.split(',')[0]), float(time_array.split(',')[1]), float(time_array.split(',')[2]))
            except:
                print ('Using the input time series...')
                time_array = ''
            tr_time, tr_yData = fitfun.get_evendata(mytime, yData, myknots = time_array, s = smooth, k = degree, mytitles = cur_dataset, plotting = True)
            flag, time, value = menu.conf_menu(tr_time, tr_yData,mytime, yData)
        ###############################################################################################


        #--------------#
        # Use Jacknife resampling scheme:
        elif recordDevice == 12:
            try:
                myfactor = float(raw_input('Percentage of deviation (Default = 0.1), \nif fit imporves more than this factor the point will be defined as an outlier:'))
            except:
                myfactor = 0.1
            tr_time, tr_yData = fitfun.jackknife(mytime, yData, multiplier = myfactor, mytitles = cur_dataset, plotting = True)
            flag, time, value = menu.conf_menu(tr_time, tr_yData,mytime, yData)
        ###############################################################################################

        #--------------#
        # Set nan
        elif recordDevice == 13:
            tr_time, tr_yData = miscel.ReplaceForNAN(mytime, yData, mytitles = cur_dataset, plotting = True)
            flag, time, value = menu.conf_menu(tr_time, tr_yData,mytime, yData)

        ###############################################################################################
        #--------------#
        # Remove data points
        elif recordDevice == 14:
            tr_time, tr_yData = miscel.remove_datapairs(mytime, yData, mytitles = cur_dataset, plotting = True)
            flag, time, value = menu.conf_menu(tr_time, tr_yData,mytime, yData)

        ###############################################################################################
        #--------------#
        # Trim the dataset
        elif recordDevice == 15:
            print cur_dataset
            #mydata = value
            ini_time = float(raw_input('Initial time [seconds]:'))
            final_time = float(raw_input('Final time [seconds]:'))
            time_step = float(raw_input('Time step [seconds]:'))
            tr_DATE, tr_time,reset_time, tr_yData = miscel.trimData(mytime, yData, ini_time, final_time, time_step, time_date = '', mytitles = cur_dataset, plotting  = True)
            flag, time, value = menu.conf_menu(reset_time, tr_yData,mytime, yData)

        ###############################################################################################
        #--------------#
        # Add/subtract a specified float
        elif recordDevice == 16:
            #print 'Pick events do not work properly with Ipython notebook, do it in a different way...'
            #mydata = np.c_[time,value]
            tr_time, tr_yData = miscel.add_floatvalue(mytime, yData, mytitles = cur_dataset, plotting  = True)
            flag, time, value = menu.conf_menu(tr_time, tr_yData,mytime, yData)

        ###############################################################################################
        #--------------#
        # Get Mean Value and standard deviation
        elif recordDevice == 17:
            print 'The standard deviation can be used as an error measurement value...'
            my_mean, my_std = miscel.get_mean_std(mytime, yData)
            #flag, time, value = DF.conf_menu(tr_time, tr_yData,mytime, yData)

        ###############################################################################################
        #--------------#
        # Select representative points from the whole dataset, using either normal or log time scaling
        elif recordDevice == 18:
            tr_time, tr_yData = miscel.get_reppoints(mytime, yData, mytitles = cur_dataset, plotting = True)
            flag, time, value = menu.conf_menu(tr_time, tr_yData, mytime, yData)

        ###############################################################################################
        #--------------#
        # Get Cumulative distributions
        elif recordDevice == 19:
            norm =bool( raw_input('Normalized data?\n -empty: no, \n1: yes') )
            tr_time, tr_yData = miscel.cumsum(mytime, yData, mytitles = cur_dataset, normalized = norm, plotting = True)
            flag, time, value = menu.conf_menu(tr_time, tr_yData,mytime, yData)

        ###############################################################################################
        #--------------#
        # Get Cumulative distributions
        elif recordDevice == 20:
            tr_time, tr_yData = miscel.detrend_lin(mytime, yData, mytitles = cur_dataset, plotting = True)
            flag, time, value = menu.conf_menu(tr_time, tr_yData,mytime, yData)

        if flag == 'yes':
                joint_dataset = np.c_[time, value]

    return joint_dataset, recordDevice