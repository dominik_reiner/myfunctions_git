#-------------------------------------------------------------------------------
# Name:        menu
# Purpose:     build up the menu to guide the user through the options
# Author:      eesl
# Created:     02-07-2015
# Copyright:   (c) IRTG-eesl 2015
#-------------------------------------------------------------------------------
import sys, os, numpy as np, matplotlib.pyplot as plt, time, platform, pdb
##import scipy.stats as sst
##import scipy
##import scipy.interpolate as si
##import scipy.signal as ssg
##import time, h5py
##import Tkinter as tki
##from scipy import interpolate
##from myplot import xyplot

def print_menu(myPath, myfile):
    """
    Reads a file and prints its content. Comment lines containing # are ignored
        Arguments:
        ----------
    myPath:         str, directory where the file to read is located
    myfile:         str, file name
        Returns:
        --------
    n_valid_lin:    int, number of valid written lines
    """
    with open(os.path.join(myPath,myfile)) as myfile:
        mytext = myfile.readlines()

    return len([exec(tt) for tt in mytext if '#' not in tt])

def errorHandlingMenu(NoOptions = 3):
    """
    Robust error handling, only integers are accepted and only within the range of
    the number of menu options
        Arguments:
        ----------
    NoOptions:      int, number of options on the menu. Default = 3
        Returns:
        --------
    user_input:     int, option selected by the user
    """
    is_valid = 0

    # Waits for valid input in while...not
    while not is_valid :
            try :
                    user_input = int ( input('Enter your choice [1-%d] : ' %(NoOptions)) )
                    is_valid = 1 # set it to 1 to validate input and to terminate the while..not loop
            except ValueError as e :
                    print ("'%s' is not an integer." % e.args[0].split(": ")[1])

    if user_input == 0:
        sys.exit('Exiting...')

    while user_input > NoOptions:

            try :
                user_input = int ( input('Enter your choice [1-%d] : ' %NoOptions) )
                if user_input > NoOptions:
                    print ("'%s' is not a valid integer." % (user_input))

            except ValueError as e :
                print ("'%s' is not an integer." % e.args[0].split(": ")[1])

    return user_input

def check_userinput(question, myoptions = ''):
    """
    Checks the user input before sending the input to the function call. In this way,
    the answers of the user are constrained and an error is not returned due to wrong
    user input
        Arguments:
        ----------
    question:       str, question that wants to be asked to the user
    myoptions:      list of str, available options to be consider in the answer
        Returns:
        --------
    answer:         str, with the right answer provided by the user
    """
    is_valid = 0
    while not is_valid :
        answer = input(question)
        if answer == '':
            print ('Empty answer, try again!')
            pass
        elif any(answer in s for s in myoptions):
            is_valid = 1 # set it to 1 to validate input and to terminate the while..not loop
        else:
            print ('%s is not a valid answer!'%(answer))
    return answer

def default_answer(question, def_value, mytype):
    '''
    Check user input and transform it to proper type, if wrong value type is provided
    default values are taken. This is to control user's input when it is not possible
    to restrict the answers
        Arguments:
        ----------
    question:           str, question for the user
    def_value:          default value
    mytype:             type of expected answer, e.g. int, str, float
        Returns:
        --------
    tr_answer:          proper answer or given default value
    '''
    try:
        myanswer = input(question)
        tr_answer = eval('mytype(myanswer)')
    except:
        print('Error in input value...\ntaking default: %s' %(def_value))
        tr_answer = def_value

    return tr_answer

def check_dir():
    """
    Check if the directory provided by the user exists or not
    """
    exists = 0
    while not exists :
        mydir = str(input('Type the source directory: '))

        if (os.path.exists(mydir) == True) and (len(os.listdir(mydir)) > 0):
            exists = 1
        elif (os.path.exists(mydir) == False):
            print ('Directory does not exists! Try again')
            pass
        elif len(os.listdir(mydir)) == 0:
            print ('Empty directory, try again')
            pass
    return mydir

def conf_menu(tr_time, tr_yData, mytime, yData):
    """
    Prints the menu that confirms if the transformed dataset is to be kept or not
        Arguments:
        ----------
    tr_time:    numpy array, transformed x values
    tr_yData:   numpy array, transformed y values
    mytime:     numpy array, original x values
    yData:      numpy array, original y values
        Returns:
        --------
    flag:                   str, keep transformde dataset flag
    xdata_new, ydata_new:   np arrays
    """
    is_valid = 0
    keep = check_userinput('Keep transformation? (yes,no): ', myoptions = ['yes', 'y', 'no', 'n'])

    if (keep =='no') or (keep =='n'):
        print ('Taking previous dataset')
        ydata_new = np.copy(yData)
        xdata_new = np.copy(mytime)

    elif (keep =='yes') or (keep =='y'):
        ydata_new = np.copy(tr_yData)
        xdata_new = np.copy(tr_time)

    flag = check_userinput('Done with this dataset? (yes, no): ', myoptions = ['yes', 'no'])

    return flag, xdata_new, ydata_new, keep


def MainMenu():
    """
    Construct the main menu. Define whether load data from txt files within
    a folder or from a hdf5 database.
        Arguments:
        ----------
    level: Str, level of the Menu to be used. Main, pumptest, slugtest
        Returns:
        --------
    myxfile:        str list, full directory of X values (if applicable)
    myYfiles:       str list, full directory with the file(s) of interest
    how2input:      int, 1 if loading from txt, 2 if loading from hdf5
    """

    main_menu = print_menu(os.path.join(os.curdir,'pycleaner', 'menus'), 'menu_main.txt')

    how2input = errorHandlingMenu(NoOptions = 2)

    if how2input == 1: # Read files within a Folder
        filesource = 'txt'
        #pdb.set_trace()
        loadsettings = check_userinput('Use loading settings from previous session?(y/n): ', myoptions = ['y','n', 'yes', 'no'])

        if loadsettings == 'y':
            #pdb.set_trace()
            try:
                try:
                   mysets = np.genfromtxt(os.path.join(os.path.dirname(os.path.realpath(__file__)),'loadset'), dtype = 'str')
                except OSError as e :
                    print ('Loadset file NOT FOUND!')

                mydir = mysets[0]
                if len(mysets) > 0:
                    print ('Working directory from previous session: < %s >' %mydir)
                xytogether = mysets[1]
                xfile = mysets[2]
                skiprow = int(mysets[3])

                columns2use = []
                for ii in range (0, len(mysets[4])):
                    columns2use.append(int(mysets[4][ii]))
                #columns2use = list(map(int, mysets[4]))

                temp = check_userinput('Use directory from previous session?(y/n): ', myoptions = ['y','n'])
                if temp == 'n':
                    mydir = str(input('Type the source directory: '))

            except:
                print ('No settings file, you will have to go through all menu!')
                loadsettings = 'n'

        if loadsettings == 'n':
            mydir = check_dir()

        fileslist = [] # Do not take files with 'modif' string nor 'png'
        for ii in os.listdir(mydir):
            if ('modif' in ii) or ('.png' in ii):
                pass
            else:
                fileslist.append(os.path.join(mydir, ii))

        if loadsettings == 'n':
            print ('Almost there :-), just some additional information about your files...')
            xytogether = check_userinput('Are both X and Y values in every file? (y/n): ', myoptions = ['y','n'])
            if xytogether == 'n':
                xfile = input('Type the string that diferentiates the file with X values from the rest: ')
            elif xytogether == 'y':
                xfile = 'dummy'
            try:
                skiprow = int(input('Skip rows? how many? headers count! Default 0): '))
            except:
                skiprow = 0
            columns2use = list(input('Columns to use? (Default 0,1...type e.g. 01): '))
            if columns2use == []:
                columns2use = [0,1]
            else:
                columns2use = list(map(int, columns2use))
        myYfiles = []

        for ii in fileslist:
            if xfile in ii:
                myxfile = ii
            else:
                myYfiles.append(ii)
        try:
            len(myxfile)
        except:
            myxfile = ''
        # Save settings for next time:
        savesettings = check_userinput('Save loading settings for future sessions?(y/n): ', myoptions = ['y','n'])
        if savesettings == 'y':
            with open(os.path.join(os.path.dirname(os.path.realpath(__file__)),'loadset'), 'w+') as mysetfile:
                #mysetfile.write(how2input)
                mysetfile.write('%s\n%s\n%s\n'%(mydir, xytogether, xfile))
                mysetfile.write('%s\n%s%s'%(skiprow, columns2use[0], columns2use[1]))
        else:
            pass
    # Until here is all related to load data from several txtfiles within a folder.
    if how2input == 2:
        filesource = 'hdf5'
        myxfile = None
        mydir = check_dir()
        myfile = str(input('File name: '))
        myYfiles = os.path.join(mydir, myfile)

    return myxfile, myYfiles, filesource, skiprow, columns2use

def testMenu(level = ''):
    """"
    Prints the different type of test data that can be analyzed
        Arguments:
        ----------
    level:      str, type of data to load. e.g. pt: pumping test,
                st: slug test, tt: tracer test
        Returns:
        --------
    recordDevice, device, typeData
    """
    assert level != '', sys.exit('Assign a string to testMenu level variable...')

    if level == 'pt':
        typeData = 'heads'
        pt_menu = print_menu(os.path.join(os.curdir,'pycleaner', 'menus'), 'menu_pumptest.txt')
        recordDevice = errorHandlingMenu(NoOptions = 3)

        if recordDevice == 1: device = 'Fiber'
        if recordDevice == 2: device = 'Diver'
        if recordDevice == 3: device = 'Manual'

    elif level == 'st':
        typeData = 'slugheads'
        st_menu = print_menu(os.path.join(os.curdir,'pycleaner', 'menus'), 'menu_slugtest.txt')
        recordDevice = errorHandlingMenu(NoOptions = 3)
        if recordDevice == 1: device = 'Pneumatic'
        if recordDevice == 2: device = 'Manual'
        if recordDevice == 3: device = 'XXXXXX'

    elif level == 'tt':
        typeData = 'concentration'
        tt_menu = print_menu(os.path.join(os.curdir,'pycleaner', 'menus'), 'menu_tracertest.txt')
        recordDevice = errorHandlingMenu(NoOptions = 3)
        if recordDevice == 1: device = '19channel'
        if recordDevice == 2: device = 'XXXXXX'
        if recordDevice == 3: device = 'Manual'

    else:
        sys.exit('Wrong level, type either < pt, st or tt >')

    return recordDevice, device, typeData

def procmenu():

    """
    Show the menu options to process data
    """

    proc_func = print_menu(os.path.join(os.curdir,'pycleaner', 'menus'), 'menu_procfunc.txt')
    recordDevice = errorHandlingMenu(NoOptions = 20)

    device = None

    return recordDevice