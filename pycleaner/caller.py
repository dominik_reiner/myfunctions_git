# Define initial settings:
import numpy as np
import pycleaner.miscel as miscel
from .myplot import xyplot
import pycleaner.smooth as mysmooth
import pycleaner.rm_outlier as rmout
import pycleaner.menu as menu
import pycleaner.fitfun as fitfun
import pdb

def procData(mytime, y, cur_dataset,my_log_file):
    #%% Start processing the data:
    flag = 'no'
    time = np.copy(mytime)     # Make a copy to keep safe original times
    value = np.copy(y)      # Make a copy to keep safe original values

    # create logfile
    #logfile=open('logfile.txt','a')
    while (flag == 'no') or (flag == 'n'):

        mytime = np.copy(time)
        yData = np.copy(value)
        # Plot initial stage of data:
        xyplot(time, value, xData_new = '', yData_new = '', mytitle = cur_dataset)
        # Show Process menu:
        recordDevice = menu.procmenu()
        print('(Working with >> %s) ' %(cur_dataset))

        if recordDevice == 0:
            flag = 'yes'
        # Start list of functions, they have the same order as in the menu...
        #--------------#
        # 1) Kalman Filter
        if recordDevice == 1:
            mytsdev = menu.default_answer('Measurement Standard deviation (Default 0.1): ', 0.1, float)

            myobj = mysmooth.filters(mytime, yData)
            tr_time, tr_yData, error = myobj.kalman(meas_std_dev = mytsdev, out_spread = False, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData, mytime, yData)
            my_dict='Kalman Filter(1) \n'+'Measurement Standard deviation: mytsdev=' +str(mytsdev)+'\n'
        ###############################################################################################

        #--------------#
        # 2) Remove signal noise using Butter filter
        elif recordDevice == 2:
            #x = value
            m = menu.default_answer('Critical frequency (Default 0.3): ', 0.3, float)

            myobj = mysmooth.filters(mytime, yData)
            tr_time, tr_yData = myobj.butter(n = m, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            my_dict='Butter filter(2) \nButterworth Critical frequenzy: m= %s \n' % m
        ###############################################################################################

        #--------------#
        # 3) Smoothing using regular window
        elif recordDevice == 3:
            window = menu.check_userinput('Which window? (flat, hanning, hamming, bartlett, blackman, gaus): ', myoptions = ['flat', 'hanning', 'hamming', 'bartlett', 'blackman', 'gaus'])
            if window == 'gaus':
                p = menu.default_answer('Shape parameter.\n1:identical to gaussian\n0.55:Laplace distribution\n(Default = 1): ', 1, float)
                sigma = menu.default_answer('Standard deviation (Default = 1): ', 1, float)
                myargs = [p, sigma]
            else:
                myargs = ''

            window_len = menu.default_answer('Length of smoothing window (Default 4):', 4, int)

            myobjs = mysmooth.windows(mytime, yData, window)
            tr_time, tr_yData = myobjs.reg_window(window_len, mytitles = cur_dataset, plotting = True, iter_args = myargs)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData, mytime, yData)
            if window=='gaus':
                my_dict='Smoothing with regular window(3) \ntype: %s \nLength of smoothing window: window_len= %s\nShape parameter: p= %s \nStandard deviation: sigma= %s \n' % (window,window_len,p,sigma)
            else:
                my_dict='Smoothing with regular window(3) \ntype: %s \nLength of smoothing window: window_len= %s\n' %(window,window_len)

        ###############################################################################################

        #--------------#
        # 4) Smoothing using recursive window
        elif recordDevice == 4:
            window = menu.check_userinput('Which window? (flat, hanning, hamming, bartlett, blackman, gaus): ', myoptions = ['flat', 'hanning', 'hamming', 'bartlett', 'blackman', 'gaus'])
            if window == 'gaus':
                p = menu.default_answer('Shape parameter.\n1:identical to gaussian\n0.55:Laplace distribution\n(Default = 1): ', 1, float)
                sigma = menu.default_answer('Standard deviation (Default = 1): ', 1, float)
                myargs = [p, sigma]
            else:
                myargs = ''

            numIter = menu.default_answer('Number of iterations (Default 1): ', 1, int)
            ini_wind_len = menu.default_answer('Initial window length (Default 4): ', 4, int)
            win_len_step = menu.default_answer('Step increment for the window length (Default 2): ', 2, int)

            myobjs = mysmooth.windows(mytime, yData, window)
            tr_time, tr_yData, D, error = myobjs.rec_window(numIter, ini_wind_len, win_len_step, mytitles = cur_dataset, plotting = True, iter_argum = myargs)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            if window=='gaus':
                my_dict='Smoothing with recursive window(4)\ntype: gaus\nShape parameter: p= %s\nStandard deviation: sigma= %s\n' %(p,sigma)
            else:
                my_dict='Smoothing with recursive window(4)\ntype: %s\nNumber of iterations: numIter= %s\nInitial window length: ini_wind_len= %s\nStep increment for the window length: win_len_step= %s\n' %(window,numIter,ini_wind_len,win_len_step)
        ###############################################################################################

        #--------------#
        # 5) Remove outliers using the distance of a point to a fitted function using splines
        elif recordDevice == 5:
            mymultiplier = menu.default_answer('Give a factor for the distance. The smaller the more strict in \ndefining a point as an outlier (default 3.0): ', 3, float)
            sporder = menu.default_answer('Order of the spline (between 1 and 5, default 3): ', 3, int)

            myobjss = rmout.outlier_detection(mytime, yData)
            tr_time, tr_yData = myobjss.dist2_fit(order = sporder, multiplier = mymultiplier, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            my_dict='Outlier removal by distance from spline fit(5)\nstrictness: mymultiplier= %s\norder of spline: sporder= %s\n' %(mymultiplier,sporder)
        ###############################################################################################

        #--------------#
        # 6) Remove outliers using the distance of a point to a its previous and consecutive points
        elif recordDevice == 6:
            mymultiplier = menu.default_answer('Give a factor for the distance. The smaller the more strict in \ndefining a point as an outlier (default = 3.): ', 3, float)

            myobjss = rmout.outlier_detection(mytime, yData)
            tr_time, tr_yData = myobjss.dist2_yval(multiplier = mymultiplier, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            my_dict='outlier removal by distance from points(6)\nstrictness: mymultiplier= %s\n' %mymultiplier
        ###############################################################################################

        #--------------#
        # 7) Distance to mean values (step-wise)
        elif recordDevice == 7:
            mymultiplier = menu.default_answer('Give a factor for the distance. The smaller the more strict in \ndefining a point as an outlier (default = 3.): ', 3, float)
            mystepsize = menu.default_answer('How big should the stepsize be? Default = Max = len(ydata): ', len(yData), int)

            myobjss = rmout.outlier_detection(mytime, yData)
            tr_time, tr_yData = myobjss.dist2_meanstd(multiplier = mymultiplier, step_size = mystepsize, mytitles = cur_dataset, plotting = True )
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            my_dict='outlier removal by distance from mean values(7)\nstrictness: mymultiplier= %s\nstepsize: mystepsize= %s\n' %(mymultiplier,mystepsize)
        ###############################################################################################

        #--------------#
        # 8) Distance to modified Zscores (step-wise)
        elif recordDevice == 8:
            mystepsize = menu.default_answer('How big should the stepsize be? Default = Max = len(ydata): ', len(yData), int)
            mythresh = menu.default_answer('Define threshold to remove outliers. Default = 3.5: ', 3.5, float)

            myobjss = rmout.outlier_detection(mytime, yData)
            tr_time, tr_yData = myobjss.mod_Zscore(step_size = mystepsize, thresh = mythresh, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            my_dict='distance to modified Zscores(8)\nstepsize: mystepsize= %s\nthreshold: mythresh= %s\n' %(mystepsize,mythresh)
        ###############################################################################################

        #--------------#
        # 9) Fit the generalized inverse gaussian distribution
        elif recordDevice == 9:
            tr_time, tr_yData, a, b, p, shift = fitfun.call_invgaus(mytime, yData, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            my_dict='fit to generalized inverse gaussian distribution(9)\nParametera: a= %s\nParameter b: b= %s\nParameter p: p= %s\nShift: shift= %s\n' %(a,b,p,shift)
        ###############################################################################################

        #--------------#
        # 10) Smooth using spline interpolation and arbitrary anchor points: Seems that it does not work properly with pick events.....
        elif recordDevice == 10:
            s = menu.default_answer('Smoothing factor (Default 0.25)\nCloser to 0 will interpolate closer to data points: ', 0.25, float)

            #Positive smoothing factor used to choose the number of knots. Number of knots will be increased until the smoothing condition is satisfied:
            #sum((w[i] * (y[i]-spl(x[i])))**2, axis=0) <= s
            tr_time, tr_yData = fitfun.spline_interpArbPoints(mytime, yData, s, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            my_dict='Smoothing with spline-interpolation(10)\nsmoothing factor: s= %s\n' % s

        ###############################################################################################

        #--------------#
        # 11) Get evenly distributed data using splines:
        elif recordDevice == 11:
            smooth = menu.default_answer('Smoothing factor (0 = no smoothing, Default 0): ', 0.0, float)
            degree = menu.default_answer('Degree of the smoothing spline (Default 3). \nMust be <= 5: ', 3, int)
            time_array = input('New time series? \nNo: empty\yes: initial,final+timestep, timestep ')

            try:
                time_array = np.arange(float(time_array.split(',')[0]), float(time_array.split(',')[1]), float(time_array.split(',')[2]))
            except:
                print ('Using input time series...')
                time_array = ''
            tr_time, tr_yData = fitfun.get_evendata(mytime, yData, myknots = time_array, s = smooth, k = degree, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            if time_array=='':
                my_dict='get evenly distributed data(11)\nsmoothing factor: smooth= %s\norder of spline: degree= %s\ntime series: time_array= input array was used\n ' %(smooth,degree)
            else:
                my_dict='get evenly distributed data(11)\nsmoothing factor: smooth= %s\norder of spline: degree= %s\ntime series: time_array= %s\n ' %(smooth,degree,time_array)

        ###############################################################################################

        #--------------#
        # 12) Use Jacknife resampling scheme:
        elif recordDevice == 12:

            tr_time, tr_yData = fitfun.jackknife(mytime, yData, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            my_dict='Jackknife function(12)\n'

        ###############################################################################################

        #--------------#
        # 13) Set nan
        elif recordDevice == 13:
            tr_time, tr_yData, xindices, byrange = miscel.ReplaceForNAN(mytime, yData, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            my_dict='Set nan(13)\nindices of values set nan: xindices= %s\nrange set nan?: byrange= %s\n' %(xindices, byrange)

        ###############################################################################################

        #--------------#
        # 14) Remove data points
        elif recordDevice == 14:
            tr_time, tr_yData, xindices, byrange = miscel.remove_datapairs(mytime, yData, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            my_dict='Remove data points(14)\nindices of removed datapoints: xindices= %s\nrange removed?: byrange= %s' %(xindices, byrange)

        ###############################################################################################


        #--------------#
        # 15) Trim the dataset
        elif recordDevice == 15:
            #mydata = value
            ini_time = menu.default_answer('Initial time [seconds], Default = to: ', mytime[0], float)
            final_time = menu.default_answer('Final time [seconds], Default = tf: ', mytime[-1], float)
            ##time_step = menu.default_answer('Time step [seconds], Default = tf - (tf-1): ', mytime[-1] - mytime[-2], float)

            tr_DATE, tr_time, tr_yData = miscel.trimData(mytime, yData, ini_time, final_time, time_date = '', mytitles = cur_dataset, plotting  = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData, mytime, yData)
            my_dict='trim dataset(15)\ntime to start trimming: ini_time= %s\ntime to stop trimming: final_time= %s\n' %(ini_time,final_time)

        ###############################################################################################

        #--------------#
        # 16) Add/subtract a specified float
        elif recordDevice == 16:
            #print 'Pick events do not work properly with Ipython notebook, do it in a different way...'
            #mydata = np.c_[time,value]
            tr_time, tr_yData, xindices, myvalue = miscel.add_floatvalue(mytime, yData, mytitles = cur_dataset, plotting  = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            my_dict='add/subtract a float(16)\nfirst and last indices of time series to get added/subtracted: xindices= %s\nadded/subtracted value: myvalue= %s\n' %(xindices,myvalue)

        ###############################################################################################

        #--------------#
        # 17) Get Mean Value and standard deviation
        elif recordDevice == 17:
            print('The standard deviation can be used as an error measurement value...')
            my_mean, my_std, xindices = miscel.get_mean_std(mytime, yData)
            #flag, time, value = DF.conf_menu(tr_time, tr_yData,mytime, yData)
            my_dict='get mean value and standard deviation(17)\nselected range: xindices= %s\nmean value: my_mean= %s\nstandard deviation: my_std= %s\n' %(xindices,my_mean,my_std)

        ###############################################################################################

        #--------------#
        # 18) Select representative points from the whole dataset, using either normal or log time scaling
        elif recordDevice == 18:
            tr_time, tr_yData, logscale, npoints, myid = miscel.get_reppoints(mytime, yData, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData, mytime, yData)
            if logscale=='':
                my_dict='select representative points(18)\nindices of points= %s\n' %myid
            else:
                my_dict='select representative points(18)\nlogscale used: logscale= %s\nnumber of points: npoints= %s\n' %(logscale,npoints)
        ###############################################################################################

        #--------------#
        # 19) Get Cumulative distributions
        elif recordDevice == 19:
            norm =bool( input('Normalized data?\n -empty: no, \n1: yes') )
            tr_time, tr_yData = miscel.cumsum(mytime, yData, mytitles = cur_dataset, normalized = norm, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            my_dict='Get cumulative distribution(19)\nnormalized data: %s\n' %(norm)

        ###############################################################################################

        #--------------#
        # 20) Get Cumulative distributions
        elif recordDevice == 20:
            tr_time, tr_yData, xindices = miscel.detrend_lin(mytime, yData, mytitles = cur_dataset, plotting = True)
            flag, time, value, keep = menu.conf_menu(tr_time, tr_yData,mytime, yData)
            my_dict='Remove linear trend(20)\nselected range: xindices= %s\n' %xindices


        if keep == 'y':
            my_log_file=my_log_file+30*'-'+'\n'+my_dict
            #logfile.write(30*'-'+'\n')
            #logfile.write(my_dict)
        else: pass

        if (flag == 'yes') or (flag == 'y'):
                joint_dataset = np.c_[time, value]
    #logfile.close()
    #pdb.set_trace()
    return joint_dataset, recordDevice, my_log_file