#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      IRTG
#
# Created:     01-07-2015
# Copyright:   (c) IRTG 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import os, numpy as np, matplotlib.pyplot as plt, time, platform

def xyplot(xData_old, yData_old, xData_new = '', yData_new = '', mytitle = ''):
    if platform.system() == 'Linux':
        plt.ion()

    plt.figure(figsize=(7,5))

    if (xData_new == '') and (yData_new== ''):
        plt.plot(xData_old, yData_old, 'b.', ms= 5, label = 'Starting plot' )
    else:
        plt.plot(xData_old, yData_old, '.b',ms= 5, label = 'Starting plot')
        plt.plot(xData_new, yData_new, '.r', ms = 3, label = 'Modified plot')
    ##plt.plot(points, 'ro', new_points,'bx')
    if mytitle != '':
        plt.title(mytitle)
    plt.xlabel('X value')
    plt.ylabel('Y value')
    plt.grid(True)
    plt.legend(loc = 0)
    plt.show()

    if platform.system() == 'Windows':
        plt.close()
