#-------------------------------------------------------------------------------
# Name:        single_outliers
# Purpose:     remove single outliers using several methods
# Author:      eesl
# Created:     01-07-2015
# Copyright:   (c) IRTG-eesl 2015
#-------------------------------------------------------------------------------
import os, numpy as np, matplotlib.pyplot as plt, time, platform
from scipy import interpolate
from .myplot import xyplot
import pdb

class outlier_detection(object):
    """ Class with several methods to detect outliers in a time or signal data series
    Arguments:
        mytime:                 numpy array with x-axis values of the data series
        myData:                 numpy array with the y-axis values of the data series
    """
    multiplier_default = 3
    stepsize_default = 10

    def __init__(self, mytime, yData):
        self.mytime = mytime
        self.yData = yData

    def dist2_fit(self, order = 3, multiplier = multiplier_default, mytitles = '', plotting = False):
        """  Fit an spline function to input data and define outliers based on
        the difference between each point and the corresponding fitted function value.
        Returns a new array with "nan" where outliers were identified.
        Arguments:
            order(opt.):            int, order of the spline. Default = 3
            multiplier (opt.):      float, factor affecting the mean difference between fit
                                    and data. the smaller the more restrictive in defining
                                    a point as an outlier. Should be > 0, default = 3
            mytitles (opt.):        str, title of the plot. Default = empty
            plotting (opt.):        plot stuff or not. Default = False
        Returns:
            yData_mod:      new dataset with nan where outlier was defined
        """
        if mytitles != '':
            mytitles = '%s\n%s. Spline order=%s, Factor=%s' %(mytitles, 'Distance to an spline fit', order, multiplier)
        starttime = time.time()

##        fit = interpolate.UnivariateSpline(self.mytime , self.yData, k = order, s = len(self.mytime)*30) # 30 is chosen arbitrarily
        #pdb.set_trace()
        fit_x = np.linspace(0, len(self.mytime), round( len(self.mytime) / 10))
        fit = interpolate.UnivariateSpline(self.mytime , self.yData, k = order)
        fit_of_fit = np.interp(self.mytime, fit_x, fit(fit_x) )
        #plt.plot(self.mytime, self.yData, '.k')
        #plt.plot(self.mytime, fit(self.mytime), '.r')

        #differences = np.abs(np.subtract(fit(self.mytime), self.yData))
        try:
            differences = np.abs(np.subtract(fit_of_fit(self.mytime), self.yData))
        except:
            differences = np.abs(np.subtract(fit_of_fit, self.yData))
        mean_differences = np.mean(differences)

        yData_mod = np.copy(self.yData)

        yData_mod[np.where(differences > multiplier*mean_differences)] = np.nan

        tottime = time.time() - starttime
        print("Run time: %5.3f seconds" %(tottime))

        if plotting == True:
            xyplot(self.mytime, self.yData, xData_new = self.mytime, yData_new = yData_mod, mytitle = mytitles)

        return self.mytime, yData_mod

    def dist2_yval( self, multiplier = multiplier_default, mytitles = '', plotting = False ):
        """ Defines outliers based on the difference between the current point i and the
        the next point i+1, and point i+2. Returns a new array with "nan" where outliers
        were identified.
        Arguments:
            multiplier (opt.):      float, the smaller the more restrictive in defining a point
                                    as an outlier. Should be > 0, default=3
            mytitles (opt.):        str, title of the plot. Default = empty
            plotting (opt.):        plot stuff or not. Default = False
        Returns:
            yData_mod:      new dataset with nan where outlier was defined
        """
        starttime = time.time()

        #calculate distances between points
        distances=np.array([])

        for i in range(1,len(self.yData)):

            distances = np.r_[distances, np.abs(self.yData[i] - self.yData[i-1])]


        #is outlier when the difference between current point i and point i+1 is more than X times
        #the difference between point i+1 and point i+2
        outlierID = np.array([])

        for i in range(2, len(self.yData) - 2):

            if distances[i-1] > multiplier*distances[i-2] and distances[i] > multiplier*distances[i+1]:
                outlierID = np.r_[outlierID, i]

        outlierID = outlierID.astype(int)
        yData_mod=np.copy(self.yData)

        yData_mod[outlierID] = np.nan

        if plotting == True:
            xyplot(self.mytime, self.yData, xData_new = self.mytime, yData_new = yData_mod, mytitle = mytitles)

        return self.mytime, yData_mod

    def dist2_meanstd(self, multiplier = multiplier_default, step_size = stepsize_default, mytitles = '', plotting = False ):
        """ Defines an outlier by comparing the distance of a value to the mean of a part
        of the data to the standard deviation of that part. Returns a new array with
        "nan" where outliers were identified.
        Arguments:
            multiplier (opt.):      float, the smaller the more restrictive in defining a point
                                    as an outlier. Should be > 0, default=3
            step_size (opt.):       the function works stepwise on parts of the data, step_size gives
                                    the size of such a part. Default = 10
            mytitles (opt.):        str, title of the plot. Default = empty
            plotting (opt.):        plot stuff or not. Default = False
        Returns:
            yData_mod:      new dataset with nan where outlier was defined
        """
        outlierID=np.array([]).astype(int)

        for i in range(step_size, len(self.yData), step_size):

            std = np.std(self.yData[i-step_size:i])
            mean = np.mean(self.yData[i-step_size:i])

            for n in range(i-step_size, i):

                if np.abs(self.yData[n] - mean) > multiplier * std:
                    outlierID = np.r_[outlierID, n]

        yData_mod = np.copy(self.yData)
        #print outlierID
        yData_mod[outlierID] = np.nan

        if plotting == True:
            xyplot(self.mytime, self.yData, xData_new = self.mytime, yData_new = yData_mod, mytitle = mytitles)

        return self.mytime, yData_mod

    def mod_Zscore(self, step_size = stepsize_default, thresh = 3.5, mytitles = '', plotting = False):
        """ Define an outlier based on a modified z score estimation.
        Arguments:
            step_size (opt.):   the function works stepwise on parts of the data, step_size gives
                                the size of such a part. Default = 10
            thresh(opt.         float, threshold to compare against the mod z-score. Observations with
                                a modified z-score (based on the median absolute deviation) greater
                                than this value will be classified as outliers. Iglewicz-Hoaglin Method
                                set thresh = 3.5
            mytitles (opt.):        str, title of the plot. Default = empty
            plotting (opt.):        plot stuff or not. Default = False
                                    plotting: Boolean, if True plots will be shown
        Returns:
            yData_mod:      new dataset with nan where outlier was defined
        References:
            Boris Iglewicz and David Hoaglin (1993), "Volume 16: How to Detect and
            Handle Outliers", The ASQC Basic References in Quality Control:
            Statistical Techniques, Edward F. Mykytka, Ph.D., Editor.
        """
        if step_size == len(self.yData):
            step_size = step_size - 1
        thresh = float(thresh)
        #Fix dimensionality?
        if len(self.yData.shape) == 1:
            self.yData = self.yData[:,None]

        yData_mod = np.copy(self.yData)

        for i in range(step_size, len(self.yData), step_size):

            # Calculate the Z scores according to definition
            median = np.median(self.yData[i-step_size:i], axis = 0)
            diff = np.sum((self.yData[i-step_size:i] - median)**2, axis=-1)
            diff = np.sqrt(diff)
            med_abs_deviation = np.median(diff)

            modified_z_score = 1.0 * diff / med_abs_deviation#0.6745 * diff / med_abs_deviation
            mask = modified_z_score > thresh
            outlierID = np.where(mask == True)[0] + (i - step_size)
            #print 'The mean absolute deviation value was estimated to be %.4e' %med_abs_deviation

            yData_mod[outlierID] = np.nan


        if plotting == True:
            xyplot(self.mytime, self.yData, xData_new = self.mytime, yData_new = yData_mod, mytitle = mytitles)

        #return modified_z_score > thresh
        return self.mytime, yData_mod.squeeze()
