#-------------------------------------------------------------------------------
# Name:        fitfun
# Purpose:     functions based on fitting parametric (also splines) functions
#               to the data
# Author:      eesl
# Created:     16-07-2015
# Copyright:   (c) IRTG-eesl 2015
#-------------------------------------------------------------------------------
import numpy as np, matplotlib.pyplot as plt
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import scipy.special as ssp
import scipy.interpolate as si
from . import menu
from .myplot import xyplot
from .miscel import pick_points

def call_invgaus(xData, yData, mytitles = '', plotting = False):
    """ Caller for the invgaus function, see below """
    # Generate random number from a gaussian (normal) distribution
    gasu = np.random.uniform(0,3,len(xData))
    gasu.sort()
    global gasu
    # Parameters for the Generalized inverse Gaussian. Just if default values
    # need to be changed
    inputparam = menu.check_userinput('Keep default initial guess of parameter values? (y/n)', myoptions = ['y', 'n'])
    if inputparam == 'n':
        a = float(input('Parameter a (default 2.0): '))
        b = float(input('Parameter b (default 1.0): '))
        p = float(input('Parameter p (default -2.0): '))
        shift = float(input('Shift (default 2.0): '))
    # Performing optimization with curve_fit
    flag = True
    while flag:
        try:
            if inputparam == 'n':
                popt, pcov = curve_fit(invgaus, gasu, yData, p0 = (a,b,p, shift) )
            if inputparam == 'y':
                popt, pcov = curve_fit(invgaus, gasu, yData)
            flag = False
        except RuntimeError:
            print('Optimized parameters not found, making another attempt...')

    print('Optimized Parameters: a = %s, b = %s, p = %s, shift = %s'%(popt[0], popt[1], popt[2], popt[3]))
    print('Covariance matrix of parameters: \n %s'  %pcov)

    # Run the model with optimized parameters:
    fx = invgaus(xData, popt[0], popt[1], popt[2], popt[3])

    if plotting == True:
        xyplot(xData, yData, xData_new = xData, yData_new = fx, mytitle = mytitles)

    assert len(xData) == len(fx), 'Array dimensions do not match!'
    return xData, fx, a, b, p, shift

def invgaus(xData, a = 2., b = 1., p = -1., shift = 2.):
    """ Fitting a parametric function using the generalized inverse gaussian
        distribution. It uses the modified bessel function.
        Link: https://en.wikipedia.org/wiki/Generalized_inverse_Gaussian_distribution
    Arguments:
        a(opt.):        float, parameter a, see equation in Wikipedia link
        b(opt.):        float, parameter b, see equation in Wikipedia link
        p(opt.):        float, parameter c, see equation in Wikipedia link
        shift(opt.):    float, shift in time of the BTC
    Returns:
        Optimized BTC
    """
##    gasu = np.random.uniform(0,xx,len(time))
##    gasu.sort()
    # Compute Modified bessel function of second kind
    K = ssp.kv(p, gasu)
    fx = (a/b)**(p/2.) * (1./(2.*K*(np.sqrt(a*b)))) * ( (gasu-shift) **(p-1.))*np.exp(-((a* (gasu-shift) )+(b/ (gasu-shift) )/2.))
    fx[np.where(gasu<=shift)] = 0
    #print a, b, p, shift#, K
    return fx


def spline_interpArbPoints(xData,yData, s, mytitles = '', plotting = False):
    """ Perform spline interpolation using selected points as anchors. The interpolated values
        will replace original ones.
    Arguments:
        xData:      Array with the x values
        yData:      Array with the y values
        mytitle:    Str, name of the dataset (for plotting options)
    Returns:
        Modified yvalues array.
    """
    #print 'You have selected 5. Spline interpolation using arbitrary points'
    print('Select the anchors to be used for spline interpolation')

    #labelString = 'Spline arbitrary points'
    figspline = plt.figure('Pick the points')
    axspline = figspline.add_subplot(111)

    xindices_spline = pick_points(np.transpose([xData,yData]), mytitles)
    xindices_spline = np.sort(xindices_spline)


    s2 =si.UnivariateSpline(xData[xindices_spline],yData[xindices_spline], s = 0.25)
    # s closer to zero will interpolate closer to all data points

    # Replace just the range of interest (interpolated range):
    my_xrange = xData[xindices_spline[0]:xindices_spline[-1]]

    yData_spline = np.copy(s2(my_xrange))

    ydata_mod = np.copy(yData)
    ydata_mod[xindices_spline[0]:xindices_spline[-1]] = yData_spline

    if plotting == True:
        xyplot(xData, yData, xData_new = xData, yData_new = ydata_mod, mytitle = mytitles)

    return  xData, ydata_mod

def get_evendata(mytime, mydata, myknots = '',s = 0, k = 3, mytitles = '', plotting = False):
    """ Get interpolated values at a nan y value, using spline cubic interpolation.
        If no smoothing is desired a value of s=0 should be given
     Arguments:
         mytime:      Array with the x values
         mydata:      Array with the y values
         mytitles:    Str, dataset identifier
         plotting:    Boolean, plot stuff or not. Default = False
         s:          Float, smoothing factor. 0 if no smoothing is desired
                     sum((w[i] * (y[i]-spl(x[i])))**2, axis=0) <= s
    Return:
        mytime, y2:     1-D numpy arrays of evenly distributed data with
                        interpolated values where they were missing
    """
    # Take out those x values corresponding to a nan in y, for the initial spline interpolation
    ynew = mydata[np.where(np.isnan(mydata)== False)]
    xnew = mytime[np.where(np.isnan(mydata)== False)]

    # Perform the interpolation without any smoothing (s = 0), so it pass through all datapoints
    tck = si.splrep(xnew, ynew, s=s, k = int(k))
    # Get interpolated values at the evenly spaced times:
    if myknots == '':
        myknots = mytime
    y2 = si.splev(myknots, tck, der=0)

    if plotting == True:
        xyplot(mytime, mydata, xData_new = myknots, yData_new = y2, mytitle = mytitles)

    return myknots, y2

def jackknife(xData, yData, mytitles = '', plotting = False):
    """ Outlier detection based on polynomial fit, using the Jackknife resampling technique. The
        fitting is performed using spline interpolation of order 1. If the rmse of the fit is smaller than a
        certain fraction of the original fit, the point left out will be defined as an outlier
    Arguments:
        xData:       Array with the x values
        yData:       Array with the y values
        multiplier:  Float, factor that multiplies the mean residual from the fit, if deviation exceeds the
                     percentage of the mean value, the point will be identified as outlier. Default = 0.1
        mytitles:    Str, dataset identifier
        plotting:    Boolean, plot stuff or not. Default = False

    """
    print('\n>If you have NaN, the result is not useful!')
    # Create new datasets
    smooth_factor = len(xData) * 0.05
    new_X = np.linspace(0, xData[-1], num = len(yData))
    y_new = np.copy(yData)

    # Estimate first rmse between fit and real values for the whole set:
    interp_full = si.UnivariateSpline(xData, yData, k = 1, s = smooth_factor)
    fit_full = np.interp(xData, new_X, interp_full(new_X))
    rmse_full = np.sqrt(np.mean((fit_full-yData)**2))

    # Now for the resampling scheme:
    rootmeansquares = np.array([])

    for i in range(0,len(yData)):
        y=np.delete(yData,i)
        x=np.delete(xData,i)

        # Interpolate the values
        myinterp = si.UnivariateSpline(x, y, k = 1, s = smooth_factor)
        fit=np.interp(x, new_X, myinterp(new_X))
        # Estimate the root mean squared error (residuals)
        rmse = np.sqrt(np.mean((fit-y)**2))
        rootmeansquares = np.r_[rootmeansquares, rmse]
    ##    plt.plot(xData, yData, 'cs-', ms = 6)
    ##    plt.plot(x,y, 'b-o', ms = 4)
    ##    plt.plot(x, fit, 'r-.', ms = 4)
    ##    plt.show()
    stdev=np.std(rootmeansquares)
    y_new[np.where(rootmeansquares < (rmse_full - stdev))[0]] = np.nan

    if plotting == True:
        xyplot(xData, yData, xData_new = xData, yData_new = y_new, mytitle = mytitles)

    return xData, y_new