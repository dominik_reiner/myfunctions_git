#-------------------------------------------------------------------------------
# Name:        miscel
# Purpose:     miscelaneus of functions not categorized or simply defined as def
# Author:      eesl
# Created:     02-07-2015
# Copyright:   (c) IRTG-eesl 2015
#-------------------------------------------------------------------------------
import warnings, matplotlib.cbook
warnings.filterwarnings('ignore', category = matplotlib.cbook.mplDeprecation)
#import sys
import os, numpy as np, matplotlib.pyplot as plt, platform
#import scipy.stats as sst
#import scipy
#import scipy.interpolate as si
#import scipy.signal as ssg
#import time
import h5py
#import Tkinter as tki
from .myplot import xyplot
from . import menu
import matplotlib.mlab as mlab

def rename_files(givenPath, myfolder, keep_str):
    '''
    Rename the files within a folder. Selection of files to be reneamed is based
    on a common string, if they do not contain it, they will be deleted.
        Arguments:
        ----------
    givenPath:      str, path to folder
    myfolder:       str, folder name
    keep_str:       str, string contained by all files to be kept and renamed
        Returns:
        --------
    '''
    mypath = os.path.join(givenPath, myfolder)

    for ii in os.listdir(mypath):

        myfile = os.path.join( myfolder, ii)

        if keep_str not in ii:
            os.remove(myfile)
        elif keep_str in ii:

            os.rename(myfile, os.path.join(mypath, ii.split('_modif')[0] + '.txt'))
    print('Files containing %s have been renamed, and the rest deleted' %keep_str)

def commas_to_points(data_path, folders):
    """Change commas to points in all files with in a defined directory
    Arguments:
        data_path: str, directory containing all folders to be scanned
        folders: list, all folders to be scanned
    Returns:
        All commas within the content of all files are changed to points as
        a decimal separator.
        """

    for cur_folder in folders:

        outpath = os.path.join(data_path, cur_folder)
        files = os.listdir(os.path.join(data_path, cur_folder))

        for cur_file in files:

            newfilename = 'new_' + cur_file
            X = open(os.path.join(outpath,cur_file),'r')
            Y = open(os.path.join(outpath, newfilename),'w')

            for line in X.readlines():

                newline = line.replace(',','.')
                Y.write('%s'%(newline))

            Y.close()
            X.close()
            os.remove(os.path.join(outpath,cur_file))
            os.rename(os.path.join(outpath,newfilename),os.path.join(outpath,cur_file))

    print('All commas of all files have been changed to points')

def pick_points(mydata, mytitle = ''):
    """
    Pick points within a plot with X,Y data, by clicking on
    the corresponding points. Supports both Windows and Linux systems
        Arguments:
        ----------
    mydata:     np array, two dim array (X,Y)
    mytitle:    str, title for the plot (name of the dataset)
        Returns:
        --------
    List with the indices of the selected points
    """

    print('Select the points (or range) of interest. Close window to continue...')
    xindices = []


    def onpick(event):

        thisline = event.artist
        xdata = thisline.get_xdata()
        ydata = thisline.get_ydata()
        ind = event.ind

        xindices.append(ind[-1])
        if (len(xindices) % 2 is not 0): # if len xindices is odd
            print ('1st. X = %s, Y = %s (idx:%s)' % (xdata[ind[-1]], ydata[ind[-1]], ind[-1]))

        elif (len(xindices) % 2 is 0): # if len xindices is even
            print ('2nd. X = %s, Y = %s (idx:%s)' % (xdata[ind[-1]],ydata[ind[-1]],ind[-1]))

        if platform.system() == 'Windows':
            fig.canvas.draw()
    fig = plt.figure('Pick the points')
    # Create figure:
    if platform.system() == 'Linux':

        plt.grid(True)
        fig.canvas.mpl_connect('pick_event',onpick)
        pt, = plt.plot(mydata[:,0],mydata[:,1],'b.',markersize = 5, mew = 0.1 ,picker = 2., label = 'Raw Data')

        plt.show()
        print('You have 20 seconds...')
        plt.pause(20)

    elif platform.system() == 'Windows':
        ax = fig.add_subplot(111)
        plt.grid(True)

        # Plot stuff:
        line, = ax.plot(mydata[:,0],mydata[:,1],'b.',markersize = 5, mew = 0.1 ,picker = 2., label = 'Raw Data')
        ax.plot(mydata[:,0],mydata[:,1],'b--')
        ax.set_title(mytitle)

        line.figure.canvas.mpl_connect('pick_event',onpick)
        plt.show()

    return xindices


#    if platform.system() == 'Linux':
#        def onpick_linux(event):
#            thisline = event.artist
#            xdata = thisline.get_xdata()
#            ydata = thisline.get_ydata()
#            ind = event.ind
#            #pdb.set_trace()
#            xindices.append(ind[-1])
#            if (len(xindices) % 2 is not 0): # if len xindices is odd
#                print ('1st. X = %s, Y = %s (idx:%s)' % (xdata[ind[-1]], ydata[ind[-1]], ind[-1]))
#
#            elif (len(xindices) % 2 is 0): # if len xindices is even
#                print ('2nd. X = %s, Y = %s (idx:%s)' % (xdata[ind[-1]],ydata[ind[-1]],ind[-1]))
#
#        #pdb.set_trace()
#        fig.canvas.mpl_connect('pick_event',onpick_linux)
#
#        pt, = plt.plot(mydata[:,0],mydata[:,1],'b.',markersize = 5, mew = 0.1 ,picker = 2., label = 'Raw Data')
#
#        plt.show()
#
#        plt.pause(15)
#
#    if platform.system() == 'Windows':
#
#        ax = fig.add_subplot(111)
#        plt.grid(True)
#
#        # Plot stuff:
#        line, = ax.plot(mydata[:,0],mydata[:,1],'b.',markersize = 5, mew = 0.1 ,picker = 2., label = 'Raw Data')
#        ax.plot(mydata[:,0],mydata[:,1],'b--')
#        ax.set_title(mytitle)
#
#
#        # Handle the picking event. Return indices rather than actual X value:
#        def onpick(event):
#            thisline = event.artist
#            xdata = thisline.get_xdata()
#            ydata = thisline.get_ydata()
#            ind = event.ind
#
#            xindices.append(ind[-1])
#            if (len(xindices) % 2 is not 0): # if len xindices is odd
#                print ('1st. X = %s, Y = %s (idx:%s)' % (xdata[ind[-1]], ydata[ind[-1]], ind[-1]))
#
#            elif (len(xindices) % 2 is 0): # if len xindices is even
#                print ('2nd. X = %s, Y = %s (idx:%s)' % (xdata[ind[-1]],ydata[ind[-1]],ind[-1]))
#
#            fig.canvas.draw()
#
#        line.figure.canvas.mpl_connect('pick_event',onpick)
#        plt.show()
#        #plt.close()



def trimData(time, mydata, ini_time, final_time, time_date = '', mytitles = '', plotting  = True):
    """ Trim a dataset using specified initial and final times. The function reset the times, assigning
        a value of zero to the NEW initial data point. If Time in date format is given, it returns an
        additional array that contains the date (hour,minutes,seconds) that correspond to the NEW initial
        data point till the very last one.
    Arguments:
        time:       Array with the time span of the input dataset
        mydata:     Array, dataset to be trimmed
        ini_time:   Float, time (NOT date format) to start trimming the dataset
        final_time: Float, time (NOT date format) to finish trimming the dataset
        time_date(Optional):  Array with the time span (same dimensions as time) in date format
    Return:
        trimed_time_date:   If provided it contains the trimmed time series in date format
        trimed_time:        Trimmed time series
        trimed_data:        Trimed dataset (e.g. drawdown)
    """
    # Trim time series using initial and final time values:
    trimed_time = time[(time >= ini_time) & (time <= final_time )]

    # Reset time series:
##    if trimed_time[0] == 0:
##        trimed_time = np.arange(0,len(trimed_time)*time_step,time_step)

    # Trim time series in date format:
    try:
        trimed_time_date = time_date[(time >= ini_time) & (time <= final_time )]
    except:
        trimed_time_date = 'Notprovided'
        pass

    # Trim the dataset:
    trimed_data = mydata[(time >= ini_time) & (time <= final_time )]
##
##    # Correct dimensions if they do not match:
##    if (len(reset_time) != len(trimed_data)) and (len(reset_time) > len(trimed_data)):
##        reset_time = reset_time[0:len(trimed_data)]
##
##    if (len(reset_time) != len(trimed_data)) and (len(reset_time) < len(trimed_data)):
##        trimed_data = trimed_data[0:len(trimed_data)]

    if plotting == True:
        xyplot(time, mydata, xData_new = trimed_time, yData_new = trimed_data, mytitle = mytitles)

    return trimed_time_date, trimed_time, trimed_data


def remove_datapairs(mytime, mydata, mytitles = '', byrange = False, plotting = False):
    """ Remove either a single data pair or  data pairs within a range, from the dataset.
        It handles only either a single point or a range!!!
    Arguments:
        mydata:     2D - Array, dataset to work with (X and Y)
        mytitle:    Str, dataset identifier
    Return:
        2-D numpy array. New data set with the selected points removed.
    """
    if mytitles != '':
        mytitles = '%s\n%s' %(mytitles, 'Remove points from data series')
    byrange = menu.check_userinput('Replace a whole range? (n/y)\nIf no, single points will be replaced:', myoptions = ['y', 'n'])
    #print 'You have selected 4. Remove data points'
    print('The data pairs within selected range will be removed from data series, select just one range at a time')
    temp = np.copy(mydata)
    timetemp = np.copy(mytime)
    xindices = pick_points(np.transpose((timetemp, temp)), mytitles)

    if byrange == 'n':
        timetemp = np.delete(timetemp, xindices)
        temp = np.delete(temp,xindices)
    elif (byrange == 'y'):
        timetemp = np.delete(timetemp, np.arange(xindices[0],xindices[-1]+1))
        temp = np.delete(temp, np.arange(xindices[0],xindices[-1]+1))

    if plotting == True:
        xyplot(mytime, mydata, xData_new = timetemp, yData_new = temp, mytitle = mytitles)
    return timetemp, temp, xindices, byrange

def ReplaceForNAN(mytime, mydata, mytitles = '', plotting = False):
    """ Replace either a single data pair or data pairs within a range, with NAN.
        It handles only either a single point or a range!!!
    Arguments:
        mydata:     2D - Array, dataset to work with (X and Y)
        mytitles:    Str, dataset identifier
    Return:
        2-D numpy array. New data set with the selected points changed to NAN.
    """
    if mytitles != '':
        mytitles = '%s\n%s' %(mytitles, 'Replace points for NaN')

    byrange = menu.check_userinput('Replace a whole range? (n/y)\nIf no, single points will be replaced:', myoptions = ['y', 'n'])
    #mymethod = bool(raw_input('Replace whole range?\n->empty: no (single points)\n->1: yes'))
    print('All selected points will be substituted with a np.nan element')
    temp = np.copy(mydata)
    xindices = pick_points(np.transpose((mytime,mydata)), mytitles)

    if (len(xindices) > 1) and (byrange == 'y'):
        temp[xindices[0]:xindices[-1]+1] = np.nan
    elif byrange == 'n':
        temp[xindices] = np.nan

    data_mod = np.array(temp)

    if plotting == True:
        xyplot(mytime, mydata, xData_new = mytime, yData_new = data_mod, mytitle = mytitles)

    return mytime, data_mod, xindices, byrange

def detrend_lin(mytime, mydata, mytitles = '', plotting = False):
    """ Remove a linear trend from a data series
    Arguments:
        mytime:     2D Array, X values
        mydata:     2D - Array, Y values
        mytitles:    Str, dataset identifier
        plotting:   Boolean, plot or not
    """
    tempData = np.copy(mydata)
    flag = 'repeat'
    while flag == 'repeat':
        xindices = pick_points(np.transpose((mytime,tempData)), mytitles)

        if len(xindices) > 2:
            print('Select EXACTLY two points!!')
        if len(xindices) < 2:
            print('Select EXACTLY two points')
        if len(xindices) == 2:
            flag ='norepeat'

    if len(xindices) == 2:
        tempData[xindices[0]:xindices[-1]+1] = mlab.detrend_linear(tempData[xindices[0]:xindices[-1]+1])
    if plotting == True:
        xyplot(mytime, mydata, xData_new = mytime, yData_new = tempData, mytitle = mytitles)

    return mytime, tempData, xindices


def add_floatvalue(mytime, mydata, mytitles = '', plotting  = False):
    """ Add or subtract a specified value to a subset of the original dataset
        Note: select only a range of data at a time (i.e. pick just one pair
        of points)
    Arguments:
        mydata: 2D - Array, dataset to work with (X and Y)
    Return:
        2-D numpy array with the provided value added to the selected subset.
    """
    tempData = np.copy(mydata)
    flag = 'repeat'
    while flag == 'repeat':
        xindices = pick_points(np.transpose((mytime,tempData)), mytitles)
        if len(xindices) > 2:
            print('Select MAXIMUM two points!!')
        if len(xindices) <= 2:
            flag ='norepeat'
        if len(xindices) == 0:
            print('Select at least one point!')

    myvalue = float(input('Enter the value to add(+) or subtract(-):\ntype -999 to set it automatically'))
    if myvalue == -999:
        mynum = 20
        aux = tempData[xindices[0]-mynum:xindices[0]]
        aux2 =tempData[xindices[1]:xindices[1]+mynum]
        baseMean =np.mean(np.r_[aux,aux2])
        shiftMean = np.mean(tempData[xindices[0]:xindices[1]])
        myvalue = baseMean - shiftMean

    if len(xindices) == 1:
        tempData[xindices[0]] = tempData[xindices[0]] + myvalue
    if len(xindices) == 2:
        tempData[xindices[0]:xindices[-1]+1] = tempData[xindices[0]:xindices[-1]+1] + myvalue
    if plotting == True:
        xyplot(mytime, mydata, xData_new = mytime, yData_new = tempData, mytitle = mytitles)

    return mytime, tempData, xindices, myvalue

def get_mean_std(mytime, mydata, mytitles = ''):
    """ Return the mean value of a certain range of the graph
    Arguments:
        mydata:     2D - Array, dataset to work with (X and Y)
        mytitles:    Str, dataset identifier
    Return:
        2-D numpy array.
    """
    print('Select a range from where to estimate mean value')
    temp = np.copy(mydata)
    xindices = pick_points(np.transpose((mytime,mydata)), mytitles)

    my_mean = np.mean(temp[xindices[0]:xindices[-1]+1])
    my_std = np.std(temp[xindices[0]:xindices[-1]+1])
    print('Mean value = %s, Std Dev = %s' %(my_mean, my_std))
    return my_mean, my_std, xindices

def cumsum(mytime, mydata, mytitles = '', normalized = False, plotting = False):
    """ Estimate cumulative distribution of values
    Arguments:
        mydata:     2D - Array, dataset to work with (X and Y)
        mytitles:    Str, dataset identifier
    Return:
        2-D numpy array. Return the cumulative sum of the elements along a given axis.
    """
    ynew = np.copy(mydata)
    mycumsum = np.cumsum(ynew)

    if normalized == True:
        mycumsum = mycumsum/np.max(mycumsum)
    if plotting == True:
        xyplot(mytime, ynew/np.max(ynew), xData_new = mytime, yData_new = mycumsum, mytitle = mytitles)

    return mytime, mycumsum


def tempmoments():
    print('Under Construction')

def get_reppoints(mytime, mydata, mytitles = '', plotting = False):
    """ Get representative datapoints of the given curve. Default number of points = 100. The selection
        can be done based on a log scale of time axis, in order to get log spacing selection.
     Arguments:
        mytime:
        mydata:     2D - Array, dataset to work with (X and Y)
        logscale:   Boolean, if True: perform data selection based on a log scale in x axis
        npoints:   Integer, number of points to select from the curve, default = 100
    Return:
        numpy arrays, selected x and y values
    """
    tempdata = np.copy(mydata)

    mymethod = menu.check_userinput('How to select the points? \n1: Manual\n2: Automatic', myoptions = ['1','2'])

    if mymethod == '1':
        myid = pick_points(np.transpose([mytime,mydata]), mytitles)
        logscale=''
        npoints=''
    elif mymethod == '2':
        question ='Log transform before selecting data?(yes/no)'; myoptions = ['yes','no']
        logscale = menu.check_userinput(question, myoptions = myoptions)
        npoints = int(input('How many points you want to get back?\n(type an integer)'))

        if 'n' in logscale:
            temptime = np.copy(mytime)
        elif 'y' in logscale:
            #remove initial time value equal to zero
            temptime = np.log(mytime[1:])
            tempdata = tempdata[1:]

        spacing = np.arange(temptime[0], temptime[-1], (temptime[-1]- temptime[0])/npoints)
        myid = np.zeros(spacing.shape)

        for ii in range(0,len(spacing)):

            myid[ii] = np.where(np.min(abs(spacing[ii]-temptime))== abs(spacing[ii]-temptime))[0][0]
        myid = np.unique(myid.astype(int))

    temptime = mytime[myid]
    tempdata = mydata[myid]

    if plotting == True:
        xyplot(mytime, mydata, xData_new = temptime, yData_new = tempdata, mytitle = mytitles)

    return temptime, tempdata, logscale, npoints, myid
""" --------------------------- """


def fiberopticData(codePath,basePath,raw_folder,date,raw_data_path,tests_folders,basefile,personInCharge,device,answer,storagePath):
    """ Function that stores pumping data recorded with fiber optic. If numbers in original files have commas instead of points, tt
        should be set equal to Y.
    Arguments:
        codePath:
        basePath:
        raw_folder:
        date:
        raw_data_path:
        tests_folders:
        basefile:
        personInCharge:
        device:
        answer:
        storagePath:
    Returns:

    """
    # Show a window with links to Template files:
    PumpGUI = tki.Tk()
    PumpGUI.geometry('+500+300')
    egFile_fiberData = os.path.join(codePath,'_TemplateFiles', 'example_pt_fiberoptics.txt')
    egFile_pumping = os.path.join(codePath,'_TemplateFiles','example_pt_INFO_pumpingwells_fiber.txt')
    egFile_obs = os.path.join(codePath,'_TemplateFiles','example_pt_INFO_observationwells_fiber.txt')

    PumpGUI.title('Fiber Optic Measurements')
    button_egFile = tki.Button(PumpGUI, text = 'Template recorded data', command = lambda actualfile = egFile_fiberData: os.startfile(actualfile)).grid(row=4,column = 1, columnspan = 4)
    button_egFile_pumping = tki.Button(PumpGUI, text = 'Template INFO_pump...', command = lambda actualfile1 = egFile_pumping: os.startfile(actualfile1)).grid(row=5,column = 1, columnspan = 4)
    button_egFile_obs= tki.Button(PumpGUI, text = 'Template INFO_obs...', command = lambda actualfile2 = egFile_obs: os.startfile(actualfile2)).grid(row=6,column = 1, columnspan = 4)
    closeButton = tki.Button(PumpGUI, text = 'Exit', command = PumpGUI.destroy).grid(row = 5, column = 5)

    labelList = tki.Label(PumpGUI,text = 'You will need to prepare the following files:').grid(row = 1, column = 1, columnspan = 3)
    labelList1 = tki.Label(PumpGUI,text = 'Open the template files to see details:').grid(row = 3, column = 1, columnspan = 3)
    myList = tki.Listbox(PumpGUI, width = 40, height = 3)
    FilesNeeded = ['INFO_pumpingwells_date.txt','INFO_observationwells_date.txt']


    for index in range(1,len(FilesNeeded)+1,1):
        myList.insert(index,FilesNeeded[index-1])
    myList.grid(row = 2,column = 1)

    PumpGUI.mainloop()

    # Choose wether to change commas for points in files:
    tt = str(input('Change commas to points in all files?(y/n)' ))

    if tt == 'y':
        commas_to_points(raw_data_path,tests_folders)
    elif tt == 'n':
        print('Continue to store the data...')

    f = h5py.File(os.path.join(storagePath, basefile), 'a') # 'a': Open read-write (create if doesn't exist)

    """ Node Check: Master (date) group: Has the date when the tests were performed """
    node_check = '/%s' % (date)

    if node_check in f:
        check = str(input('Master group <%s> already exists.\nA new folder will be appended to the existing database.\nContinue anyway?(y/n)' %(date)))

        if check == 'n':
            f.flush()
            f.close()
            print('Check again the date of the test you want to add to the database')
            raise Exception

    testdate = f.require_group(date)
    """ """
    # Define attributes of the MASTER (DATE) GROUP:
    testdate.attrs['Person(s) in charge '] = personInCharge
    testdate.attrs['Measurement device'] = device
    """ """
    #==========================================================================#
    # Read ONLY the proper folders: start with "well_"
    #==========================================================================#
    for cur_test in tests_folders:
        if cur_test.startswith("well_"):

            """ Node Check: Pumping test group """
            node_check = '/%s/%s' %(date, cur_test)
            if node_check in testdate:
                check = str(input('SUBgroup <%s> already exists.\nNew data will be appended to the existing folder.\nContinue anyway?(y/n)' %(cur_test)))
                while check not in answer:
                    print("'%s' is not a valid answer." % (check))
                    check = str(input('SUBgroup <%s> already exists.\nNew data will be appended to the existing folder.\nContinue anyway?(y/n)' %(cur_test)))
                if check == 'n':
                    f.flush()
                    f.close()
                    print('Data loading error')
                    raise EOFError

            """ """
            cur_test_name = testdate.require_group(cur_test)
            """ """
            print('Creating attributes of pumping well %s ...'%(cur_test))
            #===============================================#
            # LOAD attributes of Pumping wells (WELL GROUP):
            #===============================================#
            loadWellIDs, Recovery = np.loadtxt(os.path.join(basePath,'INFO_pumpingwells_' + date.replace('.','') + '.txt'), usecols = (0,10), dtype = 'str', unpack = True)
            x,y,z,TotDepth,z_assumed, gwtin_masl_pump, meanK, pump_rate, depthofpumping, pw_diam = np.loadtxt(os.path.join(basePath,'INFO_pumpingwells_' + date.replace('.','') + '.txt'),usecols = (1,2,3,4,5,6,7,8,9,11), unpack = True)

            cur_id = np.where(cur_test == loadWellIDs)

            # Coordinates
            pw_coord = np.array([x[cur_id], y[cur_id], z[cur_id]])
            cur_test_name.attrs['Coordinates'] = pw_coord

            # Groundwater level in meters above sea level
            cur_test_name.attrs['GWT [masl]'] = gwtin_masl_pump[cur_id]

            # Pumping interval in meters below ground surface
            cur_test_name.attrs['Pumping interval [mbgs]'] = depthofpumping[cur_id]

            # Pumping rate in liters per second
            cur_test_name.attrs['Pumping rate [l/s]'] = pump_rate[cur_id]

            # Total depth of the well in meters below ground surface
            cur_test_name.attrs['Total Well depth [mbgs]'] = TotDepth[cur_id]

            # Estimated mean K  in meters per second
            cur_test_name.attrs['Effective K [m/s]'] = meanK[cur_id]

            # Recovery phase or not?
            cur_test_name.attrs['Recovery phase'] = Recovery[cur_id]

            # Pumping well diameter in meters
            cur_test_name.attrs['Well diameter [m]'] = pw_diam[cur_id]

            f.flush()

            print('Loading observations of pumping test <%s> ...' %(cur_test))

            #=================================================================#
            # Load one by one the data from each observtion point, and add its
            # corresponding attributes (metadata)
            #=================================================================#
            # Lists all observation point files
            observation_files = np.array(os.listdir(os.path.join(raw_data_path,cur_test)))
            bb = 0
            for cur_obs in observation_files:
                """ Node Check: Observation point GROUP """
                node_check = '/%s/%s/%s' %(date, cur_test,cur_obs)
                if node_check in cur_test_name:
                    print('Warning!!! Data for observation point < %s > has been previously loaded.' %(cur_obs))
                    print('Jumping to the next observation point... \n')
                """ """

                if node_check not in cur_test_name:

##                    cur_obs_group = cur_test_name.require_group(cur_obs)

                    day, hour =np.loadtxt(os.path.join(raw_data_path,cur_test,cur_obs),usecols = (0,1), skiprows = 37,unpack = True, dtype = 'str')
                    joint_date = np.array([day[ii]+ ' ' + hour[ii] for ii in range(0,len(day),1)])
                    drawdown =np.loadtxt(os.path.join(raw_data_path,cur_test,cur_obs),usecols = (2,), skiprows = 37)
                    time = np.arange(0,len(drawdown)*0.4,0.4)

                    module_number = np.genfromtxt(os.path.join(raw_data_path,cur_test,cur_obs),skip_header = 4,skip_footer = len(drawdown)+29,usecols=(1,),dtype = 'str')
                    sensor_number = np.genfromtxt(os.path.join(raw_data_path,cur_test,cur_obs),skip_header = 21,skip_footer = len(drawdown)+14,usecols=(1,),dtype = 'str')
                    acq_rate = np.genfromtxt(os.path.join(raw_data_path,cur_test,cur_obs),skip_header = 15,skip_footer = len(drawdown)+19,usecols=(1,))

                    #date_pandas = pd.to_datetime(joint_date)
                    #t= date_pd[1]-date_pd[0]
                    #t.microseconds/1.0e6 #to ransfrom to seconds
                    #f.strftime('%Y-%m-%d %H:%M:%S.%f')

                    #===============================================#
                    # LOAD attributes of Observation wells (Observation GROUP):
                    #===============================================#
                    X,Y,Z,Exact_depth, Depth_gwt = np.loadtxt(os.path.join(basePath,'INFO_observationwells_'+ date.replace('.','') + '.txt'), usecols = (1,2,3,4,5),unpack = True)
                    well_id = np.loadtxt(os.path.join(basePath,'INFO_observationwells_'+ date.replace('.','') + '.txt'), usecols = (0,),dtype = np.str)

                    cur_index = np.where(cur_obs == well_id)

##                    # Coordinates
##                    cur_obs_group.attrs['Coordinates'] = np.array([X[cur_index],Y[cur_index],Z[cur_index]])
##
##                    # Depth location in meters below ground surface
##                    cur_obs_group.attrs['Depth location [mbgs]'] = Exact_depth[cur_index]
##
##                    # Groundwater table in meters below ground surface
##                    cur_obs_group.attrs['Depth to GWT [mbgs]'] = Depth_gwt[cur_index]
##
##                    # Sensor number
##                    cur_obs_group.attrs['Sensor number'] = sensor_number
##
##                    # Module number
##                    cur_obs_group.attrs['Module number'] = module_number
##
##                    # Acquisition rate (frequency) in Hertz.....f = number of events/time
##                    cur_obs_group.attrs['Acquisition rate [Hz]'] = acq_rate
##
##                    # File name
##                    cur_obs_group.attrs['File name'] = cur_obs

                    #===============================================#
                    # Create the current data set:
                    #===============================================#
                    string = '%s/%s' %(date, cur_test)
                    #str_type = h5py.new_vlen(str)
                    time_string = 'Time[date format]'
                    drawd_string = 'Time[s],Drawdown [mm]'

                    if bb == 0:
                        f[string].create_dataset(time_string, data = joint_date,dtype = "S24", chunks = True,compression="gzip")

                    f[string].create_dataset(cur_obs,data = np.transpose((time,drawdown)),dtype = np.float32, chunks = True,compression="gzip")

                    # Column label
                    f[string+ '/' + cur_obs].attrs['Column_desc'] = drawd_string
                     # Coordinates
                    f[string+ '/' + cur_obs].attrs['Coordinates'] = np.array([X[cur_index],Y[cur_index],Z[cur_index]])

                    # Depth location in meters below ground surface
                    f[string+ '/' + cur_obs].attrs['Depth location [mbgs]'] = Exact_depth[cur_index]

                    # Groundwater table in meters below ground surface
                    f[string+ '/' + cur_obs].attrs['Depth to GWT [mbgs]'] = Depth_gwt[cur_index]

                    # Sensor number
                    f[string+ '/' + cur_obs].attrs['Sensor number'] = sensor_number

                    # Module number
                    f[string+ '/' + cur_obs].attrs['Module number'] = module_number

                    # Acquisition rate (frequency) in Hertz.....f = number of events/time
                    f[string+ '/' + cur_obs].attrs['Acquisition rate [Hz]'] = acq_rate

                    # File name
                    f[string+ '/' + cur_obs].attrs['File name'] = cur_obs

                    f.flush()
                    bb = bb + 1
    print((30 * '-'))
    print('Data from Master group < %s > has been added to the database' %(testdate))
    print((30 * '-'))
    f.close()
    return

def pneumaticslugtest(codePath,basePath,basefile,raw_folder,date,raw_data_path,answer,personInCharge,device,storagePath):
    """ Function that stores pneumatic slug data
    Arguments:
        codePath:
        basePath:
        basefile:
        raw_folder:
        date:
        raw_data_path:
        answer:
        personInCharge:
        device:
        storagePath:
    """
    SlugGUI = tki.Tk()
    SlugGUI.geometry('+500+300')
    egFile_slugdata = os.path.join(codePath,'_TemplateFiles','example_st_slugneumatic.txt')
    egFile_slugwell = os.path.join(codePath,'_TemplateFiles','example_st_INFO_wells_pneumatic.txt')

    SlugGUI.title('Pneumatic Slug Test Measurements')
    button_egFile = tki.Button(SlugGUI, text = 'Template recorded data...', command = lambda actualfile = egFile_slugdata: os.startfile(actualfile)).grid(row=4,column = 1, columnspan = 4)
    button_egFile_pumping = tki.Button(SlugGUI, text = 'Template INFO_slugwells...', command = lambda actualfile1 = egFile_slugwell: os.startfile(actualfile1)).grid(row=5,column = 1, columnspan = 4)
    closeButton = tki.Button(SlugGUI, text = 'Exit', command = SlugGUI.destroy).grid(row = 4, column = 5)

    labelList = tki.Label(SlugGUI,text = 'You will need to prepare the following files:').grid(row = 1, column = 1, columnspan = 3)
    labelList1 = tki.Label(SlugGUI,text = 'Open the template files for deatils:').grid(row = 3, column = 1, columnspan = 3)
    myList = tki.Listbox(SlugGUI, width = 40, height = 3)
    FilesNeeded = ['INFO_wells_pneumatic_date.txt']

    for index in range(1,len(FilesNeeded)+1,1):
        myList.insert(index,FilesNeeded[index-1])
    myList.grid(row = 2,column = 1)

    SlugGUI.mainloop()

    f = h5py.File(os.path.join(storagePath, basefile), 'a') # 'a': Open read-write (create if doesn't exist)

    """ Node Check: Master (date) group: Has the date when the tests were performed """
    node_check = '/%s' % (date)
    if node_check in f:
        check = str(input('Master group <%s> already exists.\nA new folder will be appended to the existing database.\nContinue anyway?(y/n)' %(date)))
        while check not in answer:
                    print("'%s' is not a valid answer." % (check))
                    check = str(input('Master group <%s> already exists.\nA new folder will be appended to the existing database.\nContinue anyway?(y/n)' %(date)))
        if check == 'n':
            print('Check again the date of the folder you want to add to the database')
            f.flush()
            f.close()
            raise EOFError

    testdate = f.require_group(date)
    print('\nGenerating the Master folder < %s > ...' %(date))
    """ """
    # Define attributes of the MASTER (DATE) GROUP:
    testdate.attrs['Person(s) in charge '] = personInCharge
    testdate.attrs['Measurement device'] = device
    """ """

    # Read ONLY the proper folders: start with development
    folders = np.array(os.listdir(raw_data_path))            # List all folders
    for names in folders:
        if names.startswith("Development_"):

            """ Node Check: Development_ID group """
            node_check = '/%s/%s' %(date, names)
            if node_check in testdate:
                check = str(input('Folder <%s> already exists.\nContinue anyway?(y/n)' %(names)))
                while check not in answer:
                    print("'%s' is not a valid answer." % (check))
                    check = str(input('Folder <%s> already exists.\nContinue anyway?(y/n)' %(names)))
                if check == 'n':
                    f.flush()
                    f.close()
                    print('Data loading error')
                    raise EOFError
            """ """
            num_well_develop = testdate.require_group(names)
            print('Generating subfolder < %s > and loading data sets ...' %(names))

            well_folders = np.array(os.listdir(os.path.join(raw_data_path,names)))

            for cur_well in well_folders:
                """ Filter to select only folders with a well ID """
                if cur_well.startswith('well_'):

                    """ Node Check: Well_ID folder """
                    node_check = '/%s/%s/%s' %(date, names,cur_well)
                    if node_check in num_well_develop:
                        check = str(input('Folder <%s> already exists.\nContinue anyway?(y/n)' %(cur_well)))
                        while check not in answer:
                            print("'%s' is not a valid answer." % (check))
                            check = str(input('Folder <%s> already exists.\nContinue anyway?(y/n)' %(cur_well)))
                        if check == 'n':
                            f.flush()
                            f.close()
                            print('Check again the file and folder names you want to add to the database')
                            raise EOFError
                    """ """
                    slugwell_id = np.loadtxt(os.path.join(basePath,'INFO_wells_pneumatic_'+ date.replace('.','') + '.txt'), usecols = (0,),dtype = np.str)
                    X,Y,Z,depth_screen, diam, depth_gwt = np.loadtxt(os.path.join(basePath,'INFO_wells_pneumatic_' + date.replace('.','') + '.txt'), usecols = (1,2,3,4,5,6),unpack = True)

                    # Create the well_ID group and attach its metadata
                    well_id = num_well_develop.require_group(cur_well)
                    print('Generating subfolder < %s > ...' %(cur_well))

                    cur_index = np.where(cur_well == slugwell_id)
                    # coordinates
                    well_id.attrs['Coordinates'] = np.array([X[cur_index],Y[cur_index],Z[cur_index]])
                    # depth to GWT in meters below surface
                    well_id.attrs['GWT[mbgs]'] = depth_gwt[cur_index]
                    # Well diameter
                    well_id.attrs['Well diameter [m]'] = diam[cur_index]
                    # Well diameter
                    well_id.attrs['Screen depth [mbgs]'] = depth_screen[cur_index]

                    # Load the files of each slug test applied to the current well
                    well_tests = np.array(os.listdir(os.path.join(raw_data_path,names,cur_well)))

                    for cur_test in well_tests:

                        cur_pressure = np.array(cur_test[-8:-6]).astype(np.int)
                        test_number = np.array(cur_test[-11:-9])
                        time, preshead, airpressure= np.loadtxt(os.path.join(raw_data_path,names, cur_well,cur_test),usecols = (0,1,2), skiprows = 3, delimiter=';',unpack = True)
                        sensor_depth = np.genfromtxt(os.path.join(raw_data_path,names, cur_well,cur_test), skip_footer = len(time)+1, usecols = (2), dtype = 'S5', delimiter = ' ')
                        sensor_depth = sensor_depth.astype(np.float)

                            #Define name of the data set:
                        string = '%s/%s/%s' %(date, names, cur_well)
                        string_curtest = '%s.Test:%s, %s[mb]'%(cur_well,test_number,cur_pressure)
                        string_curtest2 = '%s/%s' %(string,string_curtest)

                        """ Node Check: for the DATASET """
                        node_check = '/%s/%s/%s/%s' %(date, names,cur_well,string_curtest)
                        if node_check in well_id:
                            print('Warning!!! Data for the test < %s > has been previously loaded.' %(cur_test))
                            print('Writing process will be jumped to the next observation point')
                        """ """

                        if node_check not in well_id:
                            #print ('Loading data set < %s > ...' %(string_curtest))
                            f[string].create_dataset(string_curtest,data = np.transpose(np.array([time,preshead,airpressure])), dtype = np.float32,chunks = True,compression="gzip")
                            f[string_curtest2].attrs['Column Values'] = 'Time[s], Water column[m], Air pressure [mb]'
                            f[string_curtest2].attrs['File name'] = cur_test
                            f[string_curtest2].attrs['Sensor depth [m]'] = sensor_depth # Sensor depth, in meters below groundwater table
                            f[string_curtest2].attrs['Pressure [mb]'] = cur_pressure
                            f.flush()
            #tm.sleep(2)

    print((30 * '-'))
    print('Slug data from Master group < %s > has been added to the database' %(date))
    print((30 * '-'))
    f.close()


def tracernineteen(codePath,basePath,raw_folder,date,raw_data_path,tests_folders,basefile,personInCharge,device,answer,storagePath):
    """ Function to store tracer data from a nineteen channel fluorometer from Hermes Messtechnik firma. The id that defines the structure
    of the database is the injection location name.
    Arguments:

    Returns:

    """

    # Show a window with links to Template files:
    PumpGUI = tki.Tk()
    PumpGUI.geometry('+500+300')
    egFile_fiberData = os.path.join(codePath,'_TemplateFiles', 'example_tt_nineteenchannel.txt')
    egFile_injectiontracer= os.path.join(codePath,'_TemplateFiles','example_tt_INFO_injectionPoint_nineteenchannel.txt')
    egFile_obs = os.path.join(codePath,'_TemplateFiles','example_tt_INFO_observationwells_nineteenchannel.txt')
    egFile_pumpExtWells = os.path.join(codePath,'_TemplateFiles','example_tt_INFO_pumpinginjecting_nineteenchannel.txt')

    PumpGUI.title('Nineteen Channel Fluorometer measurements')
    button_egFile = tki.Button(PumpGUI, text = 'Template recorded data', command = lambda actualfile = egFile_fiberData: os.startfile(actualfile)).grid(row=4,column = 1, columnspan = 4)
    button_egFile_injectiontracer = tki.Button(PumpGUI, text = 'Template INFO_injpoint...', command = lambda actualfile1 = egFile_injectiontracer: os.startfile(actualfile1)).grid(row=5,column = 1, columnspan = 4)
    button_egFile_obs= tki.Button(PumpGUI, text = 'Template INFO_obs...', command = lambda actualfile2 = egFile_obs: os.startfile(actualfile2)).grid(row=6,column = 1, columnspan = 4)
    button_egFile_pumpExtWells= tki.Button(PumpGUI, text = 'Template INFO_pump_injwells...', command = lambda actualfile3 = egFile_pumpExtWells: os.startfile(actualfile3)).grid(row=7,column = 1, columnspan = 4)
    closeButton = tki.Button(PumpGUI, text = 'Exit', command = PumpGUI.destroy).grid(row = 5, column = 5)

    labelList = tki.Label(PumpGUI,text = 'You will need to prepare the following files:').grid(row = 1, column = 1, columnspan = 3)
    labelList1 = tki.Label(PumpGUI,text = 'Open the template files to see details:').grid(row = 3, column = 1, columnspan = 3)
    myList = tki.Listbox(PumpGUI, width = 40, height = 3)
    FilesNeeded = ['INFO_injectionPoint_nineteenchannel_date.txt','INFO_observationwells_nineteenchannel_date.txt', 'INFO_pumpinginjecting_nineteenchannel_date.txt']

    for index in range(1,len(FilesNeeded)+1,1):
        myList.insert(index,FilesNeeded[index-1])
    myList.grid(row = 2,column = 1)

    PumpGUI.mainloop()

    # Start the actual data storing:
    f = h5py.File(os.path.join(storagePath, basefile), 'a') # 'a': Open read-write (create if doesn't exist)

    """ Node Check: Master (date) group: Has the date when the tests were performed """
    node_check = '/%s' % (date)

    if node_check in f:
        check = str(input('Master group <%s> already exists.\nContinue anyway?(y/n).\nIf yes, the group will be overwritten' %(date)))

        if check == 'n':
            f.flush()
            f.close()
            print('Check again the date of the test you want to add to the database')
            raise Exception

    testdate = f.require_group(date)
    """ """
    # Define attributes of the MASTER (DATE) GROUP:
    testdate.attrs['Person(s) in charge '] = personInCharge
    testdate.attrs['Measurement device'] = device

    # Read ONLY the proper folders: start with "well_"
    #==========================================================================#
    for cur_test in tests_folders:

        if cur_test.startswith("inj_well_"):

            """ Node Check: Injection point group """
            node_check = '/%s/%s' %(date, cur_test)
            if node_check in testdate:
                check = str(input('SUBgroup <%s> already exists.\nNew data will be appended to the existing folder.\nContinue anyway?(y/n)' %(cur_test)))
                while check not in answer:
                    print("'%s' is not a valid answer." % (check))
                    check = str(input('SUBgroup <%s> already exists.\nNew data will be appended to the existing folder.\nContinue anyway?(y/n)' %(cur_test)))
                if check == 'n':
                    f.flush()
                    f.close()
                    print('Data loading error')
                    raise EOFError

            """ """
            cur_test_name = testdate.require_group(cur_test)
            print('Creating attributes of tracer injection point %s ...'%(cur_test))

            # LOAD attributes of injection point :
            #===============================================#
            loadWellIDs = np.loadtxt(os.path.join(basePath,'INFO_injectionPoint_nineteenchannel_' + date.replace('.','') + '.txt'), usecols = (0,), dtype = 'str')
            x, y, z, TotDepth, pw_diam, z_assumed, gwtin_masl, InjRate, InjPoint = np.loadtxt(os.path.join(basePath,'INFO_injectionPoint_nineteenchannel_' + date.replace('.','') + '.txt'), usecols = (1,2,3,4,5,6,7,8,9),unpack = True)

            cur_id = np.where(cur_test == loadWellIDs)

            # Coordinates

            pw_coord = np.array([x[cur_id], y[cur_id], z[cur_id]])
            cur_test_name.attrs['Tracer Inj. well Coordinates'] = pw_coord

            # Groundwater level in meters above sea level
            cur_test_name.attrs['Tracer Inj. well GWT [masl]'] = gwtin_masl[cur_id]

            # Injection interval
            cur_test_name.attrs['Tracer Inj. point [mbgs]'] = InjPoint[cur_id]

            # Injection rate in grams per second
            cur_test_name.attrs['Tracer Inj. rate [g/s]'] = InjRate[cur_id]

            # Total depth of the well in meters below ground surface
            cur_test_name.attrs['Tracer Inj. well total depth [mbgs]'] = TotDepth[cur_id]

            # Pumping well diameter in meters
            cur_test_name.attrs['Tracer Inj. well diameter [m]'] = pw_diam[cur_id]
            mysetup = str(input('Experimental Setup? (e.g. how many injections/extractions)'))
            cur_test_name.attrs['Setup'] = mysetup

            f.flush()

            # Load additional metadata for the injection point group
            tempIDinjection, tempID_well = np.loadtxt(os.path.join(basePath,'INFO_pumpinginjecting_nineteenchannel_' + date.replace('.','') + '.txt'), usecols = (0,1),unpack = True, dtype = 'str')
            tempX, tempY, tempZ, tempdepth, tempDiam, tempGWL, tempInjRate, tempInjInterval = np.loadtxt(os.path.join(basePath,'INFO_pumpinginjecting_nineteenchannel_' + date.replace('.','') + '.txt'), usecols = (2,3,4,5,6,8,9,10),unpack = True)
            # Applying a filter to
            cur_id_temp = np.where(cur_test == tempIDinjection); cur_id_temp = cur_id_temp[0]

            for tt in range(0,len(cur_id_temp)):
                if tempInjRate[cur_id_temp[tt]] < 0:
                    mystr = 'Inj. well '
                elif tempInjRate[cur_id_temp[tt]] > 0:
                    mystr = 'Ext. well '
                cur_test_name.attrs['(%d)'%(tt+1)+mystr + '[m] ID'] = tempID_well[cur_id_temp[tt]]
                cur_test_name.attrs['(%d)'%(tt+1)+mystr + '[m] Coordinates'] = np.array([tempX[cur_id_temp[tt]], tempY[cur_id_temp[tt]], tempZ[cur_id_temp[tt]]])
                cur_test_name.attrs['(%d)'%(tt+1)+mystr + '[m] total depth [mbgs]'] = tempdepth[cur_id_temp[tt]]
                cur_test_name.attrs['(%d)'%(tt+1)+mystr + '[m] diameter [m]'] = tempDiam[cur_id_temp[tt]]
                cur_test_name.attrs['(%d)'%(tt+1)+mystr + '[m] WGT [masl]'] = tempGWL[cur_id_temp[tt]]
                cur_test_name.attrs['(%d)'%(tt+1)+mystr + '[m] rate [l/s]'] = tempInjRate[cur_id_temp[tt]]
                if tempInjInterval[cur_id_temp[tt]] == 0:
                    cur_test_name.attrs['(%d)'%(tt+1)+mystr + 'interval [mbgs]'] = 'Full screen ' + mystr[0:3]
                if tempInjInterval[cur_id_temp[tt]] != 0:
                    cur_test_name.attrs['(%d)'%(tt+1)+mystr + 'interval [mbgs]'] = tempInjInterval[cur_id_temp[tt]]

            f.flush()

            # Load data of all channels, from main file generated by fluorometer:
            # Settings (they are fixed, so ideally should not be changed):
            print('Loading tracer test observations <%s> ...' %(cur_test))
            obsID = np.loadtxt(os.path.join(basePath,'INFO_observationwells_nineteenchannel_' + date.replace('.','') + '.txt'), usecols = (0,), dtype = 'str')
            channel, Xobs, Yobs, Zobs, depthobs, gwtobs = np.loadtxt(os.path.join(basePath,'INFO_observationwells_nineteenchannel_' + date.replace('.','') + '.txt'), usecols = (1,2,3,4,5,6), unpack = True)

            No_channels = 20
            Col_perChannel = 4
            Col_values = np.array(['Gain', 'Signal', 'Result', 'Conc'])
            datasetcols = np.arange(3,No_channels*Col_perChannel + 3,1)
            frequency = 1   # Frequency of measurement. i.e.numb of occurrences of a repeating event per unit time (f = n/period) (Hz)

            # Lists all observation files
            observation_file = os.listdir(os.path.join(raw_data_path,cur_test))
            if len(observation_file) == 1:
                myFile = os.path.join(raw_data_path,cur_test,observation_file[0])
            elif len(observation_file) > 1:
                observation_file = str(input('More than one file in folder, provide manually the filename:'))
                myFile = os.path.join(raw_data_path,cur_test,observation_file)

            cur_test_name.attrs['Filename'] = observation_file
            mydate, hour, time = np.loadtxt(myFile, usecols = (0,1,2), dtype = 'str', unpack = True)
            dataset = np.loadtxt(myFile, usecols = (datasetcols), dtype = 'float')

            # Store whole file in a dictionary:
            mydict = {}
            counter = 0
            for jj in range(0, No_channels,1):

                for ii in range(0,len(Col_values),1):

                    temp1 = Col_values[ii] + '_ch_%.2d'%(jj+1)
                    mydict[temp1] = dataset[:,counter]
                    counter = counter + 1

            time =+ time.astype('int')
            mydict['a_date'] = mydate
            mydict['b_hour'] = hour
            mydict['c_time'] = time

            # Define first the dataset containing date, hour and time for all observations
            #dt = [("'a_date'", '|S10'), ("b_hour", '|S8'), ("c_time", np.float)]
            string = '%s/%s' %(date, cur_test)
            datelist = mydict['a_date'].tolist(); hourlist = mydict['b_hour'].tolist()
            joint_datedata = np.array([datelist[ii]+ ' ' + hourlist[ii] for ii in range(0,len(datelist),1)])
            f[string].create_dataset('a_DateInfo', data = joint_datedata,dtype = '|S20', chunks = True,compression="gzip")
            f[string].create_dataset('b_Time', data = mydict['c_time'],dtype = np.float, chunks = True,compression="gzip")

            for ii in range(1,No_channels+1,1):
                att_id = np.where(ii == channel.astype(int))[0]

                #dt = np.dtype([("'Gain'", np.float), ("Signal", np.float), ("Result", np.float), ("Conc", np.float)])
                f[string].create_dataset('Channel_%.2d'%ii, data = np.transpose( ( mydict['Gain_ch_%.2d'%ii], mydict['Signal_ch_%.2d'%ii], mydict['Result_ch_%.2d'%ii], mydict['Conc_ch_%.2d'%ii] ) ),dtype = np.float, chunks = True,compression="gzip")
                f[string + '/Channel_%.2d'%ii].attrs['Observation Point ID'] = obsID[att_id]
                f[string + '/Channel_%.2d'%ii].attrs['Column_desc.'] = 'Gain, Signal, Result, Concentration'
                f[string + '/Channel_%.2d'%ii].attrs['Coordinates'] = np.array([ Xobs[att_id], Yobs[att_id], Zobs[att_id] ])
                f[string + '/Channel_%.2d'%ii].attrs['Depth [mbgs]'] = depthobs[att_id]
                f[string + '/Channel_%.2d'%ii].attrs['GWT [mbgs]'] = gwtobs[att_id]

            f.flush()
    f.close()

    print('Tracer test(s) data of day <%s> has been successfully stored!' %date)




