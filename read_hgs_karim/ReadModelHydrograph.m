function [t,q] = ReadModelHydrograph()
 hydro = [];
        t = [];
        q = [];
   
for i=0:12

        step = [];
        for i=0:12
            filename = sprintf('smodel_%io.hydrograph.hydrographout.dat',i);
            hfile = fopen(filename);
            fgetl(hfile);fgetl(hfile);fgetl(hfile); % read three header lines
            hydro=cell2mat(textscan(hfile,'%f %f %f %f')) ; % read the rest of the file
            if i == 0
                t = hydro(:,1);
                q = hydro(:,2);
                step = zeros(size(t));
            else
                t =[t ; hydro(:,1)+t(end)];
                q= [q  ; hydro(:,2)];
                step = [step ;ones(length(hydro(:,1)),1)*i];
            end
            fclose(hfile);
        end
        
end
