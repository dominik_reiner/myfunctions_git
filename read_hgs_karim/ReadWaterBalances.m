function [ t,precip,infilt,exfilt,et ] = ReadWaterBalances( dirname )
%UNTITLED10 Summary of this function goes here
%   Detailed explanation goes here
%tstart = 0;
t = [0];
precip = [];
infilt = [];
exfilt = [];
et = [];
for i = 0:12
    filename = strcat(dirname,'/smodel_',num2str(i),'o.water_balance.dat');
    [t_m,precip_m,infilt_m,exfilt_m,et_m] = ReadWaterBalance(filename);
    t_m = t_m + t(end);
    t = [t t_m];
    precip = [precip precip_m];
    infilt = [infilt infilt_m];
    exfilt = [exfilt exfilt_m];
    et = [et et_m];
end
t(1) = []; % delete the added 0 in the first element. 


