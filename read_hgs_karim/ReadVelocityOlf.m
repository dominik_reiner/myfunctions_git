function [v,t] = ReadVelocityOlf(filename,m)
%filename = 'smodel_1o.v_olf.545';
fid = fopen(filename,'rb');

pad=fread(fid, 4, 'uchar') ;% first header
time=fread(fid, 80, 'uchar');
t = str2num(char(time)');
pad=fread(fid, 4, 'uchar'); 

%pad=fread(fid, 4, 'uchar');  
v = fread(fid,m*5,'float32');
v = reshape(v,5,m);
v = v';
v = v(:,[2 3 4]);
pad = fread(fid,4,'uchar'); % ending real*8


fclose(fid);
%---------------------
