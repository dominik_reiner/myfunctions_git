function [dat]= ReadTecBlock(filename,variable,time)
% read the variable data that are in the file filename.
% and return it
dat = [];
cmp = 0;
[fid,msg] = fopen(filename);
if ( fid < 0 ) 
    disp(msg)
    error('could not open input file');
end
t = 0;
%ZONE  T="OVERLAND", SOLUTIONTIME=        100.00000, DATAPACKING=BLOCK, N=     4527, E=     8740, ZONETYPE=FEQUADRILATERAL, VARLOCATION=([        4,      9,     10,     11]=CELLCENTERED)
if nargin == 3
    while( ~feof(fid) )
            s = fgetl(fid);
            if regexp(s, 'ZONE' )  % if string contians the 'zone'
               c = textscan(s,' %s %s SOLUTIONTIME=        %f , %s');
               t = c{1,3} ; 
            end
            if t >= time
                break; % if we have scanned till the wanted t, end
            end
    end


while ( ~feof(fid) )
    s = fgetl(fid);
       
        cmp =  regexp(s,variable);
        %cmp = strcmp(s,variable);
        if cmp > 0
              disp(['found ',variable]);
              %disp(s);
              s = fscanf(fid,'%s',1); % read one string
              while ~isnan(str2double(s))  % untill s is a number
                dat = [dat, str2double(s)];
                s = fscanf(fid,'%s',1);
             end
        end
   
    if cmp > 0
        break
    end
end
fclose(fid);
end

i=0;
if nargin == 2  % only two inputs, read all variable for all times.
    while( ~feof(fid) )
            s = fgetl(fid);
            if regexp(s, 'ZONE' )  % if string contians the 'zone'
               c = textscan(s,' %s %s SOLUTIONTIME=        %f , %s');
               t = c{1,3} ; i=i+1;
            else
                continue   % untill we reach a new zone, go on
            end
            
        dat(i).t=t;   % we have a zone, and time is t
        var=[]; 
        cmp = [];
        
        while(isempty(cmp) )  %% read until the variable name
            s = fgetl(fid);
            %if ~isnumeric(s)
               % if s[1] == '#'
                    cmp =  regexp(s,variable);
               % else continue;
               % end
            %end
        end
        %cmp = strcmp(s,variable);
        if ~isempty(cmp)
              disp(['found ',variable, 'time is = ', num2str(t)]);
              %disp(s);
              s = fscanf(fid,'%s',1); % read one string
              while ~isnan(str2double(s))  % untill s is a number
                var = [var, str2double(s)];
                s = fscanf(fid,'%s',1);
             end
        end
        dat(i).var = var;
    %if cmp > 0
    %    break
    %end
    end
end
return     

