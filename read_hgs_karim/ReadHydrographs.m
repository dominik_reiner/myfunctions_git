function [T,H]=ReadHydrographs(basename)
T = [];
H = [];

for i=0:12
	
    filename = strcat(basename,'smodel_',num2str(i),'o.hydrograph.hydrograph_nodes.dat');
    [t,h] = ReadHydrograph(filename);
    if i > 0 
        t = t +max(T);
    end

    T = [T ; t];
    H = [H ; h];
end
