#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      IRTG
#
# Created:     11-09-2015
# Copyright:   (c) IRTG 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import numpy as np
from numpy.linalg import inv, solve
import matmul_opt as mm

def likelihood(mata, matb, method = 'solve', ncpus = 2):
    """
    Perform matrix multiplications of the form [a.T * inv(B) * a]. Two methods are
    available. Via the inverse, or the numerically more stable solve(a,b)
        Arguments:
        ----------
    mata:   np.array, matrix ans2
    matb:   np.array, matrix b
    method: Str, how to solve the matrix multiplication problem
    ncpus:  Int, number of CPU´s to use
        Returns:
        --------
    likelihod matrix
    """
    assert method == 'solve' or 'inv', 'Wrong method for computing the likelihood'
    if method == 'inv':
        return mm.matmul_locopt(mm.matmul_locopt(mata.T, inv(matb), ncpu = ncpus), mata, ncpu = ncpus)
        #return (mata.T).dot(inv(matb)).dot(mata)

    elif method == 'solve':

        return mm.matmul_locopt(mata.T, solve(matb,mata), ncpu = ncpus)
        #return (mata.T).dot(solve(matb, mata))

