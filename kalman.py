""" Functions available to perform Kalman Filter inversion"""


import os, time, numpy as np, datetime, shutil,sys, scipy.stats as sst, platform
import hgs_grok as hgr
from numpy.linalg import inv, solve
import dir_manip as dm
import matmult as mm

#   1- acc_rej(Likelihood_new, Likelihood_old,chi2, acceptedReal, acc_flag)
#   2- sequential_update(ii, R, Kgain, Y, nreal, nmeas, fieldmeas, modmeas, E, Y_old, acceptedReal, dY_max, dY_min, heads, concentrations)
#   3- readLastIterData(Inv_name, myType = '',my_timest = '')
#   4- checkLastIterFiles(Inv_name,my_timest = '')
#   5- expshr_param(ii, mode, Y, kkkFile, orig_indices, sel_indices, meanVal = '', what= '')
#-------------------------------------------------------------------------------

def acc_rej(Likelihood_old, Likelihood_new, chi2, acc_flag = False):
    """
    Accept/reject scheme to update parameter fields in the EnKF
        Arguments:
        ----------
    likelihood_new:
    likelihood_old:
    chi2:
    num_accreal:        int, counter for accepted realizations
    accept:             bool, True if realization is accepted
        Returns:
        --------
    accept:             bool, True if realization is accepted
    num_accreal:        int, counter for accepted realizations
    """
    #%% 1st Criteria: Evaluate the objective function:
    # Realization can be accepted if likelihood decreases:
    if Likelihood_new < Likelihood_old:
        acc_flag = True

    #%% 2nd Criteria: If chi2 < random number of range [0,1]
    # the realization can be accepted:
    elif chi2 < np.random.uniform(low=0.0, high=1.0, size=1).squeeze():
        acc_flag = True

    if acc_flag == True:
        restart = False
    elif acc_flag == False:
        restart = True
    return acc_flag

def step_control(Y_term_upd, dY_max):
    """
    Step controler for updating the parameter field
        Arguments:
        ----------
    Y_term_upd:
    dY_max:
        Returns:
        --------
    Y_term_upd
    """
    dY = np.max(np.abs(Y_term_upd))
    while dY > dY_max:
        Y_term_upd = Y_term_upd/2.
        dY = np.max(np.abs(Y_term_upd))
    return Y_term_upd

def update_step(ii, homedir, ind_from_orig, R, Kgain, Y, nreal, nmeas, fieldmeas, modmeas, E, Y_old, dY_max, dY_min,mymode = 'fl_'):
    """
    Model parameter field update.
        Arguments:
        ----------
    ii:
    R:                  PriorCovMatrix_of_MeasError
    Kgain:
    Y:
    nreal:
    nmeas:
    fieldmeas:          np.array (nmeas x 1), field measurements to be used in the data assimilation
    modmeas:            np.array (nmeas x 1), modeled measurements to be compared with tthe field data
    E:                  np.array (nmeas x 1), random measurement error from ~ N(0, R). R = covmat (diag) of meas error
    Y_old:              np.array (nelem x 1), old parameter values
    acceptedReal:
    dY_max:
    dY_min:
    heads:
    concentrations
        Returns:
        --------
    Y_i:                Numpy array, updated parameter field for the specified realization
    modmeas_new:        Numpy array, updated model states
        Dependencies:
        -------------

    """
    #%% Deal first with directories:
    NoReal, dir_dict, fmt_string, kfile = dm.init_dir(ii, mymode, homedir)
    kkkFile = os.path.join(dir_dict['fld_mode'], kfile)

    #fld_allproc, fld_curproc, fld_mode, fld_heads, fld_kkkfiles,  fld_modobs,
    #fld_recdata, fld_recheads, fld_reckkk, fld_recmodobs

    #%% Start the process:
    trials = 0
    restart_flag = True

    while restart_flag == True:
        #bannerflag = True   # This is just for a correct printing sequence

        ##E = np.random.multivariate_normal(meanNoise,R.toarray()) # (nmeas x 1) this is another possibility, just to calculate for each realization (more efficient)
        #%% Compute residuals between field measurements and modeled measurements:
        # Delta = fieldmeas - (modmeas_i + E_i),  (nmeas x 1)
        #res_old = fieldmeas.squeeze() - (modmeas[:,ii] + E)
        res_old = fieldmeas.squeeze() - (modmeas + E)

        #%% Compute Likelihood (1 x 1) (likelihood=Obj.func=chi2 dist because they are assumed independent and standard normal):
        #  L = (y-f(x))' R-1 (y-f(x)) ===> (fieldmeas-modeledmeas)' * inverse(PriorCovMatrix_of_MeasError) * (fieldmeas-modeledmeas)
        Likelihood_old = mm.likelihood.likelihood(res_old, R.toarray(), method = 'solve')

        #%% Compute  Kalman gain * Residuals ===> Qsy (Qyy + R)-1 [Yo - (Yui + Ei)]
        #Y_term_upd = (Qym.dot(np.linalg.inv(Qmm_new))).dot(res_old)  # (nelements x 1)
        Y_term_upd = mm.matmul_opt.matmul_locopt(Kgain, res_old,ncpu = 2)

        #%% Apply the step control to update the parameter field,
        # and then update it (according to Eq 3. Schoniger 2012):
        # s_i,c (or Y_i,c) = Y_i,u + Y_term_upd
        Y_term_upd = step_control(Y_term_upd, dY_max)
        Y_i = Y[:,ii] + Y_term_upd

        #%% Update state variables (model meas) with forward model run
        # Transform Y to normal space [m/s] for forward simulation
            # Missing here to get elements, nnodes, orig....
        orig_indices = np.subtract(np.loadtxt(kkkFile,usecols =(0,)),1).astype(int)
        #sel_indices = np.subtract(np.loadtxt(os.path.join( fld_mode,'supfile_elements.dat'),usecols =(0,)),1).astype(int)
        modmeas_new = hgr.getstates(ii, mymode, elements, nnodes, orig_elem_ind, elem_ind_fromorig, inner_gr_nodes, Y_i_upd = np.exp(Y_i))

        #%% Calculate new residuals:
        #res_new = fieldmeas.squeeze() - (modmeas_new[:,ii] + E)
        res_new = fieldmeas.squeeze() - (modmeas_new + E)

        #%% Estimate new likelihood (1 x 1) (likelihood=Obj.func=chi2 dist because they are assumed independent and standard normal):
        Likelihood_new = mm.likelihood.likelihood(res_new, R.toarray(), method = 'solve')

        #%% Initialize the acceptance/rejection scheme:
        chi2 = sst.chi2.cdf(Likelihood_new,nmeas)   # Compute critical CDF value
        acc_flag, restart_flag = acc_rej(Likelihood_old, Likelihood_new, chi2) # by default acc_flag= False

        # If realization not accepted, old parameters are adjusted with more detail
        if acc_flag == False:
            Y_i = np.copy(Y[:,ii]) #Y_old # Take again the old parameters, is this ok? or better Y_old as was before. Maybe I do not need Y_old at all

        while acc_flag == False:
            print ('Realization %d not accepted. Second attempt...' %(NoReal))
            Y_term_upd = step_control(Y_term_upd, dY_max)
            # Update parameter field: s_i,c or Y_i,c = Y_i,u + Y_term_upd
            Y_i = Y_i + Y_term_upd
            # Update state variables (or model measurements)...
            modmeas_new = hgr.getstates(ii, mymode, elements, nnodes, orig_elem_ind, elem_ind_fromorig, inner_gr_nodes, Y_i_upd = np.exp(Y_i))
            # Calculate new residuals:
            res_new = fieldmeas.squeeze() - (modmeas_new + E)
            # Estimate new likelihood:
            Likelihood_new = mm.likelihood.likelihood(res_new, R.toarray(), method = 'solve')
            # Compute the critical CDF value for acceptance or rejection
            chi2 = sst.chi2.cdf(Likelihood_new,nmeas)
            acc_flag, restart_flag = acc_rej(Likelihood_old, Likelihood_new, chi2) # by default acc_flag= False

            #%% If not yet accepted, restart updating modifying Y using random factors between 0-1
            if restart_flag == True:
                trials = trials + 1
                print ('Realization %d is being restarted' %(ii+1))

                seed = np.random.uniform(low=0.0, high=1, size=nreal).squeeze() #seed = seed/np.sum(seed)
                Y[:,ii] = mm.matmul_opt.matmul_locopt(Y, seed, ncpu = 2)
                if trials == 3: # If a certain number of trials has been attemped, restart from initial values
                    Y[:,ii] = np.copy(Y_old)
                    trials = 0
                    print ('Too many trials. Taking initial parameter values again...')

                # forward simulation... get new modmeas
                modmeas = hgr.getstates(ii, mymode, elements, nnodes, orig_elem_ind, elem_ind_fromorig, inner_gr_nodes, Y_i_upd = np.exp(Y[:,ii]))
                    # Check which Y_i I am passsing! Before it was: grokhgs.updateStates(ii, Flow, Transport, Y_i_upd = np.exp(Y_i), genfield = False)

        if restart_flag == False: # or ac_flag == True would also work
            print ('Realization %d updated.' %(NoReal))

    return Y_i, modmeas_new







def readLastIterData(Inv_name, myType = '',my_timest = '', what = ''):
    """
    Inv_name:       Str, name of the project(folder) containing the results of the inversion
    myType:         Str, type of data to load. Param -> 'Y'; ModelOutputs -> 'ModMeas'; CovMatrix -> 'Qmy'
    my_timest:      Str, number of time step to take data from
    what:           Str, shrink, expand or nothing to do with the stored data
    """
    if platform.system()== 'Linux':
        KalmanFilterPath = 'kalmanfilter'
    elif platform.system() == 'Windows':
        KalmanFilterPath = 'KalmanFilter'
    #%% Define directories:
    mainPath = os.path.abspath(os.path.dirname(__file__))
    mydataFiles = os.path.join(mainPath, '..', KalmanFilterPath,'Realizations', Inv_name)

    if not os.path.exists(mydataFiles):
        sys.exit( 'Directory where data is stored has been set incorrectly!')

    if myType == '':
        sys.exit( 'No type of data defined. Restarting is not possible!')
    lst_temp = os.listdir(mydataFiles)
    lst = []
    # First filter of the strings: get those corresponding to the type of data requested (e.g. Model outputs)
    for ss in lst_temp:
        if ss.startswith(myType):
            if my_timest in ss:
                lst.append(ss)
    lst = lst[-1]

    if '.npy' in lst:
        mydata = np.load(os.path.join(mydataFiles,lst))
    if '.txt' in lst:
        mydata = np.loadtxt(os.path.join(mydataFiles,lst))
    if  '.dat' in lst:
        mydata = np.loadtxt(os.path.join(mydataFiles,lst))

    #if (myType == 'Y') and (what == 'shrink'):
    return mydata

def checkLastIterFiles(Inv_name,my_timest = ''):
    """
    Inv_name:       Str, name of the project(folder) containing the results of the inversion
    my_timest:      Str, number of time step to take data from
    """

    KalmanFilterPath = 'kalmanfilter'
    #%% Define directories:
    mainPath = os.path.dirname(__file__)
    mydataFiles = os.path.abspath(os.path.join(mainPath, '..', KalmanFilterPath,'Realizations', Inv_name))

    if not os.path.exists(mydataFiles):
        sys.exit( 'Directory where data is stored has been set incorrectly!')

    lst_temp = os.listdir(mydataFiles)
    lst = []
    lst2 = []
    myiter = []
    # get a list of results stored from the last iteratiuon and last time step:
    for myid, ss in enumerate(lst_temp):
            if my_timest in ss:
                lst.append(ss)
                myiter.append(int(ss.split('_')[1].split('iter')[1]))
    if len(lst) > 3:
        for ii in lst:
            if ('%.3d' %(max(myiter))) in ii:
                lst2.append(ii)
        if len(lst2) == 3:
            myflag = 'Done'

    elif len(lst) == 3:
        myflag = 'Done'

    else:
        if len(lst) == 1:
            assert 'ModMeas' in lst[0], 'Unexpected error'
            myflag = 'NotDone'

    return 'Done', ('%.3d' %(max(myiter)))


##def updateStates(NoReal,Y_i, mytype):
##    """ Purpose: Process to update states with updated parameters using a forward
##    model run and the corresponding realization
##    Arguments:
##        NoReal:     Int, identifier of the realization being dealt with
##        Y_i:        Array, updated parameter field used to perform a new model run for new states
##        type:       Str, flag to define wether is the reference model or the update stage
##    Returns:
##        Array with updated modeled measurements or states
##    """
##    NoReal = NoReal + 1     # To correct the zero index from python
##    fmt_string = '%.5d' %(NoReal)
##    #%% Generate new kkk files to be used by GROK and HGS in the proper folder
##    # (search for the process folder with corresponding index, if not exists, then create it):
##    curKFile = 'kkkGaus_%.5d.dat' %NoReal
##    identifier = 'fl_'
##
##
##         # For flow (all elements):
##         #-----------------------------------------------------------------------------------------------#
##    myDir2RunFlow = os.path.join(processesPath,'process_%.5d' %(NoReal), identifier +'%.5d' %(NoReal))
##    outputFile = os.path.join(myDir2RunFlow, curKFile)
##
##    try: # If the file cannot be created is because the proper folder does not exist, hence create it:
##        np.savetxt(outputFile,np.transpose((np.arange(1,len(Y_i)+1,1),Y_i,Y_i,Y_i)), fmt='%d %.6e %.6e %.6e')
##    except:
##        Source_Folder = '_Flow_ToCopy'
##        GrokStr = 'Flowsim'
##        mode = 'flow'
##        allprocPath = processesPath
##        mainPath = KalmanFilterPath
##        curKFile, myDir2RunFlow = HGS.fwdprepare(Source_Folder, GrokStr, mainPath,allprocPath, fmt_string, identifier, mode)
##        np.savetxt(outputFile,np.transpose((np.arange(1,len(Y_i)+1,1),Y_i,Y_i,Y_i)), fmt='%d %.6e %.6e %.6e')
##
##    # Run Flow to get steady heads:
##    HGS.fwdHGS(myDir2RunFlow)
##    # Extract modeled heads:
##    myouttimes = np.loadtxt(os.path.join(KalmanFilterPath,'Model_data','_outputtimesFlow.dat'))
##    commonName = 'observation_well_flow'; fullPath = True; onlylastFile= False;
##    listOfFiles = mandir.getCommonStrDirs(myDir2RunFlow, commonName, fullPath, onlylastFile)
##    selHeads_All = np.array([])
##    for ii in listOfFiles:
##        selHeads = HGS.readModelMeas(ii,myouttimes.astype(np.int))
##        selHeads_All = np.append(selHeads_All,selHeads) # All selected heads
##
##        # For transport (inner Grid only):
##        #----------------------------------------------------------------------------------------------------#
##    identifier = 'tr_'
##    myDir2RunTrans =  os.path.join(processesPath,'process_%.5d' %(NoReal), identifier + '%.5d' %(NoReal))
##    headsFile1 = 'finalheadsFlow_%.5d.dat'%NoReal
##
##    if os.path.exists(myDir2RunTrans) == False:
##
##        Source_Folder = '_Transport_ToCopy'
##        GrokStr = 'Transpsim'
##        mode = 'transport'   # transport, heat
##        mainPath = KalmanFilterPath
##        allprocPath = processesPath
##        curKFile, myDir2RunTrans = HGS.fwdprepare(Source_Folder, GrokStr, mainPath, allprocPath,fmt_string, identifier, mode, FlowDir = myDir2RunFlow, headsFile = headsFile1)
##
##        # Extract final heads and kkk for the transient model (small grid) from the flow run (big grid):
##    myDir = os.path.join(KalmanFilterPath,'Model_data','_RandFieldsParameters.dat')
##    xlen, ylen, zlen, nxel, nyel, nzel, xlim, ylim, zlim, Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z, elX_size, elY_size, elZ_size = Genfi.readParam(myDir)
##
##    inputGridFile = os.path.join(myDir2RunFlow ,'meshtecplot.dat')
##    inputFile_KKK = os.path.join(myDir2RunFlow,curKFile)
##    outputFile_KKK = os.path.join(myDir2RunTrans, curKFile)
##
##    commonName = 'head_pm'; fullPath = True; onlylastFile = True
##    myHeadsFileIn = mandir.getCommonStrDirs(myDir2RunFlow, commonName, fullPath, onlylastFile)
##    myHeadsFileOut = os.path.join(myDir2RunTrans, headsFile1)
##
##    supportingFileElem = os.path.join(myDir2RunTrans, 'supfile_elements.dat')
##    supportFileNodes = os.path.join(myDir2RunTrans, 'supfile_nodes.dat')
##
##    nodes = int(mandir.findStringFile(inputGridFile, 'N='))
##    elements = int(mandir.findStringFile(inputGridFile, 'E='))
##
##    Gridf.SelectInnerGrid(inputGridFile, inputFile_KKK, outputFile_KKK, myHeadsFileIn, myHeadsFileOut,
##                    supportingFileElem, supportFileNodes, nodes, elements, elX_size, elY_size,
##                    elZ_size, xlim, ylim, zlim)
##
##    #%% Run transport model:
##    myDir2Run = myDir2RunTrans
##    HGS.fwdHGS(myDir2Run)
##
##        # Extract modeled concentrations at predefined output times and store them as binary file for python:
##    myouttimes = np.loadtxt(os.path.join(KalmanFilterPath,'Model_data','_outputtimesTrans.dat'))
##    commonName = 'observation_well_conc'; fullPath = True; onlylastFile= False;
##    listOfFiles = mandir.getCommonStrDirs(myDir2RunTrans, commonName, fullPath, onlylastFile)
##    selConc_All = np.array([])
##    for ii in listOfFiles:
##        selConc = HGS.readModelMeas(ii,myouttimes.astype(np.int))
##        selConc_All = np.append(selConc_All,selConc) # All selected concentrations
##
##
##    modmeas_new = np.r_[selHeads_All,selConc_All]
##
##    #%% Remove all created files and copy just those of interest (to save memory space) for both flow and transport:
##    commName = np.array(['.grok','.pfx','kkkGaus','.mprops','letmerun','wprops',headsFile1,'supfile','finalheads','parallelindx', 'mod']) #'meshtecplot'
##    newFolders = [myDir2RunFlow,myDir2RunTrans]
##    myPaths = [myDir2RunFlow + 'temp',myDir2RunTrans+'temp']
##
##    for yy in newFolders:
##        os.rename(yy, yy+'temp')
##        os.makedirs(yy)
##
##    for jj in range(0,len(myPaths)):
##        for curname in commName:
##            myPath = myPaths[jj]; commonName = curname; fullPath = False; onlylastFile = False
##            myfilesflow = mandir.getCommonStrDirs(myPath, commonName, fullPath, onlylastFile)
##
##            for ii in myfilesflow:
##                shutil.copy(os.path.join(myPath,ii), os.path.join(newFolders[jj],ii))
##
##        shutil.rmtree(myPath)
##
##    return modmeas_new
##
###    np.save(outputFileBin, np.transpose((np.exp(Ybasket))))
##
##

