# -*- coding: utf-8 -*-
"""
Created on Sun Jun 21 21:01:19 2015

@author: emilio
"""
import numpy as np, time, random, scipy, multiprocessing as mp
import hgs_grok as hgr
import dir_manip as dm
import grid_manip as gm

def GenReal(str2pass):
    """ Run realizations with the possibility of doing it in parallel """

    Str2Assign, homedir, mode, Ini_ensemble, shrink, genfields, grid_filedir = list2var(str2pass)

    #%%
    try:
        NoReal = np.asarray(Str2Assign).astype('int') - 1
    except:
        NoReal = np.asarray(Str2Assign).astype('str')

    #%% Some headers for multiprocessing module:
    scipy.random.seed()
    time.sleep(np.random.uniform(low=1.0, high=1.5, size=1)) #/Process_created = mp.Process()
    Process_current = mp.current_process()
    print('Process No. %s started by %s%s...' % (Str2Assign, Process_current.name, Process_current._identity))

    #%% Read mesh data:
    nnodes, elements = gm.readmesh(grid_filedir, fulloutput = False)
    orig_elem_ind = np.arange(1,elements+1,1)
    elem_ind_fromorig = orig_elem_ind

    #%% Run function to get states:
    modmeas = hgr.getstates(homedir, NoReal, mode, nnodes, elements, orig_elem_ind, elem_ind_fromorig, inner_gr_nodes = '', Y_i_upd = '', genfield = genfields, plot = False)

    print ('Exiting:', Process_current.name, Process_current._identity)

    # If initialization of ensemble, return only the index
    if Ini_ensemble == True:
        return Str2Assign
    # If not initialization of ensemble, return also model outputs and parameters
    elif Ini_ensemble == False:
        return Str2Assign, modmeas

def list2var(str2pass):
    """
    Change a composite list to several variables using '-' as a symbol to
    separate the strings...
    """

    Str2Assign  = str2pass.split('-')[0]
    homedir     = str2pass.split('-')[1]
    mode        = str2pass.split('-')[2]
    Ini_ensemble= bool(str2pass.split('-')[3])
    shrink      = bool(str2pass.split('-')[4])
    genfields   = bool(str2pass.split('-')[5])
    grid_filedir = str2pass.split('-')[6]

    return Str2Assign, homedir, mode, Ini_ensemble, shrink, genfields, grid_filedir