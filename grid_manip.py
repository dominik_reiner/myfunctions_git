""" Functions to manipulate a grid """
import sys,os
import pycleaner.myplot as myplt
import dir_manip as dm
#import pycleaner.menu as pmenu
import numpy as np, time, pdb, platform
import matplotlib.pyplot as plt
#import hgs_grokFunctions as grokhgs

def readmesh(my_meshfile, fulloutput = False):
    """
    Read relevant information from meshtecplot file
        Arguments:
        ----------
    myfile:        Str, full directory of the grid source file
        Returns:
        --------
    nnodes, nelem, nodes, elements
    """
    assert os.path.exists(my_meshfile), 'Mesh file does not exists'
    nnodes = int(dm.str_infile(my_meshfile, 'N='))
    nelem = int(dm.str_infile(my_meshfile, 'E='))

    if fulloutput == True:
        nodes_coord = np.genfromtxt(my_meshfile, skip_header = 3, skip_footer = nelem, usecols=(0,1,2))
        element_nodes = np.loadtxt(my_meshfile, skiprows = nnodes+3)

        assert element_nodes.shape[0] == nelem, 'Mismatch with element number'
        assert nodes_coord.shape[0] == nnodes, 'Mismatch with nodes number'

    if fulloutput == True:
        return nnodes, nelem, nodes_coord, element_nodes
    elif fulloutput == False:
        return nnodes, nelem

def sel_grid(in_gridfile, elX_size, elY_size, elZ_size, xlim, ylim, zlim):
    """
    Select grid elements within area of interest (and param value) based on element
    centroids, to create a kkk-file for running the transport model using only the
    small grid. Elements selected based on X, Y, Z coord.
        Arguments:
        ---------
        Note: All file strings should be given with full path
    in_gridfile:    str, path/filename with grid info from GROK (e.g. meshtecplot.dat)
        in_paramfile:   str, path/filename with parameter values (e.g. kkk_fileFlow.dat)
    out_paramfile:  str, path/filename with parameters of the selected elements of the grid (e.g. kkk_file_smallgrid.dat)
    nodes, elements:int, number of nodes or elements defined in in_gridfile
    layers:         int, Number of layers of the model grid
    elX_size, elY_size, elZ_size: float, element size on X, Y or Z direction
    xlim, ylim, zlim: np array, ini and final X, Y, or Z coordinate of the small area (e.g. np.array([30, 70]))
        Returns:
        --------
    new_param_ind:      np.array, index of the (elemental) parameter values within area of interest
    new_K:              np.array, parameter values within are of interest
    elem_ind_fromorig:  np.array, indices of the param within are of interest WITHIN original indices listdir
    new_nodes:          np.array, nodes within area of interest
    """
    # Load grid or mesh data:
    nodes, elements, nodes_coord, element_nodes = readmesh(in_gridfile)
    #%% Load parameter data #and heads:
    #orig_K_ids, orig_K = np.loadtxt(in_paramfile, usecols =(0,1), unpack = True)
    orig_K_ids = np.arange(1,elements+1, 1)
    #heads, time = grokhgs.readHeadsBin(myfile, nodes)

    #%% Obtain centroids of each element:
    element_centroid = np.empty((element_nodes.shape[0],3))

    for ii in range(0,element_nodes.shape[0],1):
        idx1 = element_nodes[ii,0]
        idx2 = element_nodes[ii,1]
        idx3 = element_nodes[ii,2]
        idx4 = element_nodes[ii,3]
        idx5 = element_nodes[ii,4]
        idx6 = element_nodes[ii,5]
        idx7 = element_nodes[ii,6]
        idx8 = element_nodes[ii,7]
        node1 = nodes_coord[idx1-1,:]
        node2 = nodes_coord[idx2-1,:]
        node3 = nodes_coord[idx3-1,:]
        node4 = nodes_coord[idx4-1,:]
        node5 = nodes_coord[idx5-1,:]
        node6 = nodes_coord[idx6-1,:]
        node7 = nodes_coord[idx7-1,:]
        node8 = nodes_coord[idx8-1,:]

        face1 = np.vstack((node1,node2,node3,node4))
        face2 = np.vstack((node5,node5,node6,node8))
        centroid = np.vstack((np.mean(face1,axis = 0), np.mean(face2,axis = 0)))
        element_centroid[ii,:] = np.mean(centroid,axis = 0)

    #%% Select those elements within the X,Y,Z limits defined above:
    center_int_ids = np.zeros((1,1))

    for ii in range(0,len(element_centroid),1):

        if all( [element_centroid[ii,0] >= xlim[0], element_centroid[ii,0] <= xlim[1],\
                    element_centroid[ii,1] >= ylim[0], element_centroid[ii,1] <= ylim[1],\
                    element_centroid[ii,2] >= zlim[0], element_centroid[ii,2] <= zlim[1]]):

            center_int_ids = np.vstack((center_int_ids,ii+1))

    center_int_ids = center_int_ids[1:].astype('int')
    # Get node number of (nodal) parameter values within area of interest
    new_nodes = np.unique(element_nodes[center_int_ids,:].flatten()).astype('int')

    #new_K = orig_K[center_int_ids-1]
    #new_param_ind = orig_K_ids[0:len(new_K)]
    # This array let me select directly the parameters of the area if interest directly from KKK file
    elem_ind_fromorig = orig_K_ids[center_int_ids-1]
    #pdb.set_trace()
    return elem_ind_fromorig, new_nodes
    #return new_param_ind, new_K, elem_ind_fromorig, new_nodes

def rotmesh2d(coordinates, myplot = False, save2file = False):
    """
    Rotate grid coordinates. Pivot point is the southernmost/westermost point. Rotated grid will be
    aligned with the X axis. Rotation is clockwise in the Cartesian coordinates
        Arguments:
        ----------
    coordinates:    2d np array (x,y)
    myplot:         boolean, True: plot original and rotated grid
    save2file:      str, full path of the file to store rotated coordinates. False if not to save into file
        Returns:
        --------
    2d numpy array of rotated coordinates
    """
    #%% Definition of the southernmost and easternmost point and estimation of rotation angle:
    # Find the pivot at the southernmost point location
    pivot_coord = coordinates[np.where(coordinates[:,1] == np.min(coordinates[:,1]))][0]

    # Find the easternmost point location, to define the rotation angle
    east_coord = coordinates[np.where(coordinates[:,0] == np.max(coordinates[:,0]))][0]

    # Calculate the angle of rotation
    d = np.sqrt((np.subtract(east_coord[0],pivot_coord[0]))**2+(np.subtract(east_coord[1],pivot_coord[1]))**2)
    angle = np.degrees(np.arcsin(np.abs(pivot_coord[1]-east_coord[1])/d))

    #%% Three matrix operations: back translation, rotation and forward translation:
    # First: back translation
    xyback = np.zeros((len(coordinates),2))
    for ii in range(0,2,1):
        for kk in range(0,len(xyback),1):
            xyback[kk,ii] = np.subtract(coordinates[kk,ii], pivot_coord[ii])

    # Second: rotation
    xyrot = np.zeros((len(coordinates),2))

    for ii in range(0,len(coordinates),1):
        xyrot[ii,0] = (xyback[ii,0]*np.cos(-angle* np.pi/180.) - xyback[ii,1]*np.sin(-angle* np.pi/180.))
        xyrot[ii,1] = (xyback[ii,0]*np.sin(-angle* np.pi/180.) + xyback[ii,1]*np.cos(-angle* np.pi/180.))

    # Third: forward translation
    xyforward = np.zeros((len(coordinates),2))
    for kk in range(0,2,1):
        for ii in range(0,len(xyforward),1):
            xyforward[ii,kk] = xyrot[ii,kk] + pivot_coord[kk]

    if myplot: # Use myplot module
        myplt.xyplot(coordinates[:,0], coordinates[:,1], xData_new = xyforward[:,0], yData_new = xyforward[:,1], mytitle = 'Original vs rotated points')
        if platform.system() == 'Windows':
            plt.close()

    if save2file != False:
        try:
            assert os.path.isdir(os.path.split(save2file)[0]), 'Wrong directory'
        except:
            save2file = str(input('Type a valid path/filename input: ')) #Here I can use the function to constrain user input

        np.savetxt(save2file, xyforward, fmt='%.6e')

    return xyforward

def expshr_param(Y, orig_indices, ind_from_orig, what= False):
    """
    Expand or shrink parameter vector. Used when it is wanted to work with a
    smaller parameter vector rather than with the parameters of the whole grid
        Arguments:
        ----------
    Y:              np array. Original parameter array
    orig_indices:   np array. Indices of the parameter elements
    ind_from_orig:  np.array. Indices of the selected area taken from the original full grid
    what:           str, 'shrink' or 'expand' depending on what is wanted
                    if shrink, a single mean value will be assigned to an element
                    representing all the elements outside the area of interest
        Returns:
        --------
    np array of parameters. The size depends on what was asked to perform
    """
    if what == False:
        print ('Y has NOT been modified (shrank nor expanded)')
        return Y

    elif (what == 'shrink') or (what =='expand'):

        nonsel_indices = np.subtract( np.delete(orig_indices,ind_from_orig,0), 1)

        if len(ind_from_orig) == len(orig_indices):
            ind_from_origs = ind_from_orig -1

        if what == 'shrink':
            meanVal = np.mean(Y[nonsel_indices])
            new_param = np.append(meanVal, Y[ind_from_origs])

        elif what == 'expand':
            new_param = np.empty((len(orig_indices)))

            new_param[nonsel_indices] = Y[0]

            new_param[ind_from_origs] = Y[ind_from_origs]
            #pdb.set_trace()
        return new_param


