""" Functions to generate parameter fields """
import sys, os
#mainPath = os.path.dirname(__file__)
import numpy as np
import matplotlib.pyplot as plt, time, os
import scipy#, pdb

def readParam(myDir):
    """
    Load the parameters for generating random gaussian fields
        Arguments:
        ----------
    myDir:  Str, full file path (including filename)
        Returns:
        --------
    Corresponding parameters: xlen, ylen, zlen, nxel, nyel, nzel, xlim, ylim,
    zlim, Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z,
    elX_size, elY_size, elZ_size
    """
    nm = ['xlen','ylen','zlen','nxel','nyel','nzel','xlim0','xlim1',\
          'ylim0','ylim1','zlim0','zlim1','Y_model','Y_var','beta_Y',\
          'lmbx','lmby','lmbz']
    try:
        fieldparam = np.genfromtxt(myDir,dtype = "f8, f8, f8, int, int, int, f8, f8, f8, f8, f8, f8, |S12, f8, f8, f8, f8, f8", names = [nm[0], nm[1], nm[2], nm[3], nm[4], nm[5], nm[6], nm[7], nm[8], nm[9], nm[10], nm[11], nm[12], nm[13], nm[14], nm[15], nm[16], nm[17]])
    except:
        print('Parameters for field generation are not properly defined in the input file')
        raise

    # Boundaries of the flow model domain
    xlen, ylen, zlen = [fieldparam[nm[0]], fieldparam[nm[1]], fieldparam[nm[2]]]
    nxel, nyel, nzel  = [fieldparam[nm[3]], fieldparam[nm[4]], fieldparam[nm[5]]]

    elX_size,  elY_size, elZ_size  = [xlen/nxel, ylen/nyel, zlen/nzel]

    # Variogram parameters
    Y_model, Y_var, beta_Y = [fieldparam['Y_model'], fieldparam['Y_var'], fieldparam['beta_Y']]
    lambda_x, lambda_y, lambda_z = [fieldparam['lmbx'], fieldparam['lmby'], fieldparam['lmbz']]

    # Boundaries of the transport model domain
    xlim  = np.array([fieldparam['xlim0'], fieldparam['xlim1']])
    ylim = np.array([fieldparam['ylim0'], fieldparam['ylim1']])
    zlim = np.array([fieldparam['zlim0'], fieldparam['zlim1']])

    return xlen, ylen, zlen, nxel, nyel, nzel, xlim, ylim, zlim, Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z, elX_size, elY_size, elZ_size

def genFields_FFT_3d(xlen, ylen, zlen, nxel, nyel, nzel, Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z, plot = False, mydir = '') :
    """
    Generate 3D gaussian fields using spectral methods (Detrich & Newsam). Modified from Cirpka, 2014.
    Works only for regularly spaced rectangular grids.
        Arguments:
        ----------
    xlen,ylen,zlen: Float, length width and height of domain [m], X, Y and Z axis
    nxel,nyel,nzel: Integer, number of elements in X, Y and Z
    Y_model:        Str, cov functionmodel: 'exponential', 'gaussian', 'spherical'
    Y_var:          Float, variance of field
    beta_Y:         Float, mean value. NOT in log scale!
    lambda_X,Y,Z:   correlation length in x, y, z
    outputFile:     Str, full directroy for the output file, ie. kkk.dat file
    outputFileBin:  Str, full directory for the output binary file, ie. kkk.pyn file
    plot: 			Boolean, True: create a plot and save the figure as a png file
        Returns:
        --------
    Parameter values in original units (e.g. in m/s) and in the right order for HGS.
    If plot is true, it creates the corresponding pcolor maps of log(K) and save
    them as .png file.
        """
    Y_model = Y_model.astype(str)
    scipy.random.seed()
    start_time = time.clock()
    #%% Define parameters:
    beta_Y      = np.array([np.log(beta_Y)])   # mean value
    Qbb         = np.array([1.])     # covariance of mean value
    invQbb      = np.linalg.inv([Qbb])  # Inverse of Qbb

    domain_xlen = xlen       # length of domain [m], or X axis
    domain_ylen = ylen       # width of domain [m] or Y axis
    domain_zlen = zlen        # height of domain [m] or Z axis

    el_xlen = domain_xlen/nxel  # lenght of elements in X axis
    el_ylen = domain_ylen/nyel  # lenght of elements in Y axis
    el_zlen = domain_zlen/nzel  # lenght of elements in Z axis

    if Y_model == 'exponential':
        embfac = 10
    elif Y_model == 'gaussian':
        embfac     = 10
    elif Y_model == 'spherical':
        embfac     = 1
    else:
        raise ValueError('no valid geostatistical model')

    #%% Define embedded domain size:
    nxele = np.ceil(np.amax([2 * embfac * lambda_x, domain_xlen + (embfac * lambda_x)]) / (el_xlen))
    nyele = np.ceil(np.amax([2 * embfac * lambda_y, domain_ylen + (embfac * lambda_y)]) / (el_ylen))
    nzele = np.ceil(np.amax([2 * embfac * lambda_z, domain_zlen + (embfac * lambda_z)]) / (el_zlen))

    nele  = nxele * nyele * nzele       # Total number of elements in embedded domain

    domain_xlene = nxele * el_xlen      # length of embedded domain [m], or X axis
    domain_ylene = nyele * el_ylen      # width of embedded domain [m], or Y axis
    domain_zlene = nzele * el_zlen      # height of embedded domain [m], or Z axis

    #%% Define original domain coordinates (distances):
    yel_vec = np.arange(0,nyel,1, dtype = 'float32') * el_ylen
    xel_vec = np.arange(0,nxel,1, dtype = 'float32') * el_xlen
    zel_vec = np.arange(0,nzel,1, dtype = 'float32') * el_zlen
    nel     = len(yel_vec) * len(xel_vec) * len(zel_vec)

    if plot == False:
        xel,yel,zel  = np.meshgrid(xel_vec,yel_vec,zel_vec,sparse = True)
    elif plot == True:
        xel,yel,zel  = np.meshgrid(xel_vec,yel_vec,zel_vec)

    h_eff        = np.float32(np.sqrt((xel / lambda_x)**2 + (yel/lambda_y)**2 + (zel/lambda_z)**2))

    #%% Define prior statistics:
    X1   = np.ones((nyel, nxel, nzel)) # This one is used internally in python
    X   = np.ravel(X1,1) # This is used for exporting to HGS

    #%% Define embedded domain coordinates (distances):
    xele_vec     = np.arange(0,nxele,1, dtype = 'float32') * el_xlen
    yele_vec     = np.arange(0,nyele,1, dtype = 'float32') * el_ylen
    zele_vec     = np.arange(0,nzele,1, dtype = 'float32') * el_zlen

    if plot == False:
        xele,yele,zele = np.meshgrid(xele_vec,yele_vec,zele_vec, sparse = True)
    elif plot == True:
        xele,yele,zele = np.meshgrid(xele_vec,yele_vec,zele_vec)

    xele         = np.minimum(xele,domain_xlene-xele)
    yele         = np.minimum(yele,domain_ylene-yele)
    zele         = np.minimum(zele,domain_zlene-zele)
    h_effe       = np.float32(np.sqrt(np.square(xele/lambda_x) + np.square(yele/lambda_y) + np.square(zele/lambda_z)))

    #%% Define original and embedded covariance matrix and its FFT2:
    if Y_model == 'exponential':
        Qs   = Y_var * np.exp(-h_eff)
        Qse  = Y_var * np.exp(-h_effe)

    elif Y_model == 'gaussian':
        Qs   = Y_var * np.exp(-h_eff**2)
        Qse  = Y_var * np.exp(-h_effe**2)

    elif Y_model == 'spherical':
        Qs   = Y_var * (1 - 1.5 * h_eff + 0.5 * h_eff**3)
        Qs[np.where(Qs > 1)] = 0
        Qse  = Y_var * (1 - 1.5 * h_effe + 0.5 * h_effe**3)
        Qse[np.where(Qse > 1)] = 0

    FFTQse   = np.fft.fftn(np.reshape(Qse,(nyele,nxele,nzele))).real
    lamb = np.float32(FFTQse/nele)

    epsilonreal = np.float64(np.random.standard_normal((FFTQse.shape[0],FFTQse.shape[1],FFTQse.shape[2])))
    epsilon = epsilonreal.astype(complex)
    epsilon.imag = np.float64(np.random.standard_normal((FFTQse.shape[0],FFTQse.shape[1],FFTQse.shape[2])))
    #epsilon = np.random.standard_normal((FFTQse.shape[0],FFTQse.shape[1],FFTQse.shape[2])) + np.random.standard_normal((FFTQse.shape[0],FFTQse.shape[1],FFTQse.shape[2]))*1j
    Y_bar   = epsilon * np.sqrt(lamb)
    Ye      = np.fft.ifftn(Y_bar).real * nele

    # Arrays needed for tecplot order
    Ytemp   = np.empty([nyel,nxel])
    Ybasket = np.zeros([1])

    # For plotting in python in the right orer
    if plot == True:
        Y= np.empty([nyel,nxel,nzel]) # First create the desired matrix

    for ii in range(0,nzel):
        # For Tecplot
        Ytemp[0:nyel,0:nxel] = Ye[0:nyel,0:nxel,ii]
        Ytempshaped = np.reshape(Ytemp,nyel*nxel,order='A')
        Ybasket = np.r_[(Ybasket,Ytempshaped)]          # This array has the order needed in HGS

        # For plotting in python
        if plot == True:
            Y[0:nyel,0:nxel,ii] = Ye[0:nyel,0:nxel,ii] + X1[0:nyel,0:nxel,ii]*beta_Y

    Ybasket = Ybasket[1:] + X * beta_Y

    if plot == True:
        for ii in range(0,nzel):
            plt.figure(ii)
            plt.pcolor(xel[:,:,ii],yel[:,:,ii],Y[:,:,ii])
            #plt.ylim([0,100])
            #plt.xlim([0,200])
            #plt.axis('equal')
            plt.colorbar()
            plt.savefig(os.path.join(mydir, '3D_logK_Figure%d.png' %(ii)), bbox_inches='tight')
            plt.title('Gaussian Log(K) field %d' %ii)
        plt.show()
        for ii in range(0,nzel):
            plt.figure(ii)
            plt.pcolor(xel[:,:,ii],yel[:,:,ii],np.exp(Y[:,:,ii]))
            #plt.ylim([0,100])
            #plt.xlim([0,200])
            #plt.axis('equal')
            plt.colorbar()
            plt.savefig(os.path.join(mydir, '3D_Km_s_Figure%d.png' %(ii)), bbox_inches='tight')
            plt.title('Gaussian K [m/s] field %d' %ii)
        plt.show()

    print('Time to generate 3d field with %d elements: %s seconds' %(nel, time.clock()- start_time))

    return np.exp(Ybasket) # return K values in meter per seconds

def genFields_FFT_2d(xlen, ylen, nxel, nyel, Y_model, Y_var, beta_Y, lambda_x, lambda_y, plot = False):
    """
    Generate 2D gaussian fields using spectral method (Detrich & Newsam). Works only for regularly spaced
    rectangular grids. Modified from Cirpka, 2014.
        Arguments:
        ----------
    xlen,ylen:      Float, length width (or height) of domain [m], X, Y (or Z) axis
    nxel,nyel:      Integer, number of elements in X, Y
    Y_model =       Str, cov function: 'exponential', 'gaussian', 'spherical'
    Y_var:          Float, variance of field
    beta_Y:         Float, mean parameter value. NOT in log scale!
    lambda_X,Y:     correlation length in x, y
    plot: 			Boolean, True: create a plot and save the figure as a png file
        Returns:
        --------
    KKK_file of log(k) and K in the right order for HGS. If plot is true, it creates
    the corresponding pcolor maps of log(K) and save them as a .png file.
    """
    Y_model = Y_model.astype(str)
    scipy.random.seed()
    start_time = time.clock()

    #%% Define parameters:
    beta_Y = np.array([np.log(beta_Y)])   # mean value
    Qbb         = np.array([10])     # covariance of mean value
    invQbb      = np.linalg.inv([Qbb])  # Inverse of Qbb

    domain_xlen = xlen       # length of domain [m], or X axis
    domain_ylen = ylen       # width of domain [m] or Y axis
    el_xlen = domain_xlen/nxel  # lenght of elements in X axis
    el_ylen = domain_ylen/nyel  # lenght of elements in Y axis

    if Y_model == 'exponential':
        embfac = 10
    elif Y_model == 'gaussian':
        embfac     = 10
    elif Y_model == 'spherical':
        embfac     = 1
    else:
        raise ValueError('no valid geostatistical model')

    #%% Define embedded domain size:

    nxele = np.ceil(np.amax([2 * embfac * lambda_x, domain_xlen + (embfac * lambda_x)]) / el_xlen)
    nyele = np.ceil(np.amax([2 * embfac * lambda_y, domain_ylen + (embfac * lambda_y)]) / el_ylen)

    nele         = nxele*nyele

    domain_xlene = nxele * el_xlen      # length of embedded domain [m], or X axis
    domain_ylene = nyele * el_ylen      # width of embedded domain [m], or Y axis

    #%% Define original domain coordinates (distances):

    yel_vec     = np.arange(0,nyel,1, dtype = 'float') * el_ylen
    xel_vec     = np.arange(0,nxel,1, dtype = 'float') * el_xlen
    nel         = len(yel_vec)*len(xel_vec)
    xel,yel     = np.meshgrid(xel_vec,yel_vec)
    h_eff       = np.sqrt((xel/lambda_x)**2 + (yel/lambda_y)**2 )

    #%% Define prior statistics:
    X   = np.ones((nyel, nxel))
    X   = np.ravel(X,1)

    #%% Define embedded domain coordinates (distances):

    xele_vec     = np.arange(0,nxele,1, dtype = 'float') * el_xlen
    yele_vec     = np.arange(0,nyele,1, dtype = 'float') * el_ylen
    xele,yele  = np.meshgrid(xele_vec,yele_vec)
    xele         = np.minimum(xele,domain_xlene-xele)
    yele         = np.minimum(yele,domain_ylene-yele)

    h_effe       = np.sqrt((xele/lambda_x)**2 + (yele/lambda_y)**2 );

    #%% Define original and embedded covariance matrix and its FFT2:

    if Y_model == 'exponential':
        Qs   = Y_var * np.exp(-h_eff)
        Qse  = Y_var * np.exp(-h_effe)

    elif Y_model == 'gaussian':
        Qs   = Y_var * np.exp(-h_eff**2)
        Qse  = Y_var * np.exp(-h_effe**2)

    elif Y_model == 'spherical':
        Qs   = Y_var * (1 - 1.5 * h_eff + 0.5 * h_eff**3)
        Qs[np.where(Qs > 1)] = 0
        #Qs(h_eff>1) = 0;
        Qse  = Y_var * (1 - 1.5 * h_effe + 0.5 * h_effe**3)
        Qse[np.where(Qse > 1)] = 0

    FFTQse   = np.fft.fftn(np.reshape(Qse,(nyele,nxele))).real
    lamb = FFTQse/nele

    epsilon = np.random.standard_normal((FFTQse.shape[0],FFTQse.shape[1])) + np.random.standard_normal((FFTQse.shape[0],FFTQse.shape[1]))*1j
    Y_bar   = epsilon * np.sqrt(lamb)

    Ye      = np.fft.ifftn(Y_bar).real * nele
    Y      = Ye[0:nyel,0:nxel]
    #Y_tecplot = np.transpose(Y) # Not correct order for tecplot

    Y = np.reshape(Y,nel)
    Y_tecplot = np.reshape(Y,nel)

    Y = Y + X * beta_Y
    Y_tecplot = Y_tecplot + X * beta_Y

##  logOutputFile = outputFile.split('.')[0]+ 'log.' + outputFile.split('.')[1]
##  logoutputFileBin = outputFileBin + 'log'
##  np.savetxt(logOutputFile,np.transpose((np.arange(1,len(Y_tecplot)+1,1),Y_tecplot,Y_tecplot,Y_tecplot)), fmt='%d %.6e %.6e %.6e')
    #np.savetxt(outputFile,np.transpose((np.arange(1,len(Y_tecplot)+1,1),np.exp(Y_tecplot),np.exp(Y_tecplot),np.exp(Y_tecplot))), fmt='%d %.6e %.6e %.6e')
    #np.save(outputFileBin, np.transpose((np.exp(Y_tecplot))))
    #np.save(logoutputFileBin, np.transpose((np.arange(1,len(Y_tecplot)+1,1),Y_tecplot,Y_tecplot,Y_tecplot)))

    Y_true = np.reshape(Y,(nyel,nxel))

    if plot is True:
        plt.pcolor(xel,yel,Y_true)
        #plt.ylim([0,100])
        #plt.xlim([0,200])
        #plt.axis('equal')
        plt.xlabel('X [m]')
        plt.ylabel('Y [m]')
        plt.colorbar()
        plt.savefig('2D_logK_Figure.png', bbox_inches='tight')
        plt.title('Gaussian Log(K) field')
        plt.show()
        plt.close()

    print('Time to generate 2d field with %d elements:: %s seconds' %(nel, time.clock()- start_time))

    return np.exp(Y_tecplot)

# The rest not really useful anymore...
def gstatControlFile(inputFile, outputFile, instructionFile, Nugget, NumberModels, Models, c, a,rand_seed):
    """ Generate the control file for Gstat. Contains variogram parameters.
        (equivalent to the 00_generate_GstatFile.py script)
    Arguments:
        inputFile:      Str, name of file that contains grid information (X,Y,Z) (full path!!!)
        outputFile:     Str, name of file to be generated by Gstat after each simulation (full path)
        instructionFile: Str, txt file name for gstat, will contain the simulated values (full path!!!))
        Nugget:         Float, variogram nugget (eg 0.050)
        NumberModels:   Int, if a combination of two or more variograms is wanted (eg 2)
        Models:         Str numpy array with the variogram models (gstat syntax, ie np.array(['Sph', 'Gau']))
        c:              Float numpy array, vertical(variance) scaling factor or partial sill in transitive models
                        ( c(1-Model(a)) ) (np.array([1.50, 1.50]))
        a:              Float numpy array, range (horizontal, distance scaling factor) of the simple model
                        (e.g. np.array([10.0, 1.0]))
        rand_seed:      Int, random seed number to simulate independant fields
                        (e.g. rand_seed = np.round(np.random.rand() * 1000 + 1))
    Returns:
        Generates the 'Gaussian_sim.txt' file that will be used by Gstat to simulate a field.
    """
    #%% Defined Gstat parameters:
    sk_mean = 0.0   # simple kriging mean
    radius = 150.0  # select obs in a local neighbourhood when they are within a distance of radius
    maxPoints = 100 # maximum number of observations in a local neighbourhood selection
    minPoints = 10  # minimum number of observations in a local neighbourhood selection
    fileName = 'Gaussian_sim.txt'  # File name to be executed by Gstat
    method = np.array(['gs']) # gs = gaussian simulation
    n_sim = 1       # Perform only one simulation

    #%% Start writing the file:
    fobj = open(instructionFile, 'w')
    fobj.write('# Gaussian simulation, conditional upon data            #\n')
    fobj.write('# (local neighbourhoods, simple and ordinary kriging)   #\n')
    fobj.write('#########################################################\n\n')

    fobj.write('data(alpha): dummy, sk_mean=%1.5f,radius=%6.3f,max=%d,min=%d;\n' %(sk_mean,radius,maxPoints,minPoints))

    fobj.write('variogram(alpha): %5.3f Nug(0.00) ' %(Nugget))

    for ii in range(0,NumberModels,1):
        fobj.write('+ ')
        fobj.write('%s %s(%6.3f)' %(c[ii],Models[ii],a[ii]))
    fobj.write(';\n')

    fobj.write('\n # gaussian simulation = gs\n' )
    fobj.write('method: %s;\n' %(method[0]))

    fobj.write('data(): \'' + inputFile + '\', x=1, y=2, z=3;\n\n')
    fobj.write('set format=\'%.10g\';\n')
    fobj.write('set output: \'' + outputFile + '\';\n\n')

    n_uk = 20 # use ordinary kriging when > 20 data in neighbouhood
    fobj.write('# use ordinary kriging when > 20 data in neighbourhood\n')
    fobj.write('set n_uk = %d;\n\n' %(n_uk))

    fobj.write('# missing values are written as blank space = \' \' \n')
    fobj.write('set mv= \' \';\n\n')

    fobj.write('## Number of simulations(are written in one output file [x y z k k k k.....]\n')
    fobj.write('set nsim=%d;\n\n' %(n_sim))
    fobj.write('set seed=%d;\n' %(rand_seed))

    fobj.close()

def gstatBatch(nFieldsToSimulate, myBatchFileGstat, myCodeDir, myBatchDir, k_file, MonteCarlo, InternalLoop, gstat2real):
    """ Create a batch file to run Gstat X number of times to generate X number of diferent
        Gaussian parameter fields, sequentially, not in Parallel.
    Arguments:
        nFieldsToSimulate:      Int, number of realizations
        myBatchFileGstat:       Str, name of the batch file to be generated (full path)
        k_file:                 Str, name of the file with the current parameter values, give full path(eg kkk_synthetic_Gaus.dat)
        MonteCarlo:             Boolean, True: MonteCarlo approach, False: single realization
        InternalLoop:           Boolean, True:loop defined in batch file, False: loop defined in python
        gstat2real:             Str, full path of the python script to convert Gstat output to real K values

            Note: The gstat2real string should call a python script that calls the function "gstat2realK"

    Return: A batch file to generate X number of realizations, sequentially.
    """

    #%% Generate the strings for each KKK file to be created
    # and create necessary variables:
    k_fileStrip = np.char.strip(k_file, '.dat')
    gstatfile = 'Gaussian_sim.txt'
    time_between_sim = 12

    #%% Start writting the file:
    if all(MonteCarlo == False, InternalLoop == False):
        nFieldsToSimulate = 1
        fobj = open(myBatchFileGstat, 'w')
        fobj.write('@echo off')
        fobj.write('\n\ngstat.exe %s\n' % gstatfile)
        fobj.write('python %s' %(gstat2real))
        fobj.close()

    if all(InternalLoop == True, MonteCarlo == True):
        fobj = open(myBatchFileGstat, 'w')
        fobj.write('@echo off')
        fobj.write('\n\nfor /L %s in (1,1,%d) do (' % ('%%i', nFieldsToSimulate))
        fobj.write('\n\ngstat.exe %s\n' % (gstatfile))
        fobj.write('python %s\n\n' %(gstat2real))
        fobj.write('copy %s %s_%s.dat > nul' %(k_file, k_fileStrip, '%%%i'))
        fobj.write('\n\n')
        fobj.write('del %s > nul\n\n' %(k_file))
        fobj.write('timeout /t %d)' % time_between_sim)
        fobj.close()

    else:
        ValueError('Check MonteCarlo and InternalLoop variables')
        raise

def gstat2realk(inputField, inputCoord, outputField, Type_of_dist, plotHist):
    """ Transform the simulation of Gstat into "real" K values. It should be possible use
        also in a parallel process.
    Arguments:
        inputField:     Str, full directory of the input Gstat field (X,Y,Z value) (eg gstatGaus_XXXXX.dat)
        inputCoord:     Str, full directory of file coordinates used for Gstat (X,Y,Z) (eg 'centroids_gstat.dat')
        outputField:    Str, full directory of the transformed K-field (eg kkkGaus_XXXX.dat)
        Type_of_dist:   Str, LogNormal or Weibulinv(only for a mean value of 3.0e-3m/s)
                        Default values for LogNormal: log_K_Mean = -6.2; log_K_Std = 0.5
    Returns:
        OutputField file with transformed K values. The values are odered according to HGS structure.
    """
    if Type_of_dist == 'LogNormal':
        log_K_Mean = -6.2       # Values from Lessoff et al 2009
        log_K_Std = 0.5         # mean = 2.0e-3 [m/s]

    print('Scaling GSTAT output to parameter space...')

    #%% Define files and load information:
    output_file = outputField
    centroids = np.loadtxt(inputCoord)
    gstat_table = np.loadtxt(inputField)

    #%% Sort whole table from "inputCoord" file: first X, then Y,
    # then Z keeping precision of Gstat simulation:
    element_numbers_centroids = np.arange(1, len(centroids)+1, 1)

    for i in [0,1,2]:

            sort_ind_centroids = np.argsort(centroids[:,i], kind='mergsort')

    centroids = centroids[sort_ind_centroids]
    element_numbers_centroids = element_numbers_centroids[sort_ind_centroids]

    #%% Sort gstat output values according to last column (element value)
    # and sort the generated K-field:
    indices_gaus = np.argsort(gstat_table[:,3])
    gstat_table = gstat_table[indices_gaus]     # Sort table according to gaussian values


    #%% Scale the Normal Gaussian values from Gstat to values in the K parameter space:
    if Type_of_dist == 'Weibulinv':

        print('Parameter values are mapped according to InvWeibul...')
        U = np.random.rand(gstat_table.shape[0])
        K = invVertWeib(44320127996.4,4.2896256,0,U)

        if plotHist == True:

            plt.hist(K, bins = 20, normed = True)
            plt.show()
        #indices_k = np.argsort(K)
        #K = K[indices_k]

    elif Type_of_dist == 'LogNormal':

        print('Parameter values are being mapped according to Log Normal ...\n')

        # Generate normal random numbers using mean and variance form Lessoff and calculate standard ranks:
        normdist = np.random.lognormal(log_K_Mean,log_K_Std,len(gstat_table[:,-1]))#np.sort(np.random.lognormal(-6.2,0.45,len(gstat_table[:,-1])))
        quantiles_normdist = 1./(len(normdist))
        ranks_normdist = np.arange(0,1.0+quantiles_normdist,quantiles_normdist)
        stdranks_normdist = ranks_normdist[1:,]-(0.5*quantiles_normdist)
        stdranks_normdist = np.concatenate((np.array([0.0]),stdranks_normdist,np.array([1.])))

        if plotHist == True:

            plt.plot(normdist,stdranks_normdist[1:-1],'b.')
            plt.hist(stdranks_normdist, bins = 10, cumulative = True, color = 'green', alpha = 0.5, normed = True)
            plt.show()

        # Calculate standard ranks of the gstat output
        quantilerange = 1.0/(len(gstat_table))
        ranks = np.arange(0, 1.0+quantilerange, quantilerange)
        stdranks_gstat = ranks[1:,]-(0.5*quantilerange)
        stdranks_gstat = np.concatenate((np.array([0.0]),stdranks_gstat,np.array([1.])))
        K = normdist

        if plotHist == True:

            #plt.plot(gstat_table[:,-1],stdranks_gstat[1:-1],'r.')
            plt.hist(stdranks_gstat, bins = 10, cumulative = True, color = 'green', alpha = 0.5, normed = True)
            plt.show()
            plt.close()

    #%% Assign to the gstat norm values the values on K-space obtained with the random generator:
    gstat_table[:,-1] = K # Substitute the last column for K-values

    #%% Back sort with respect the number of elements
    for i in [0,1,2]:
            sort_ind_gstat = np.argsort(gstat_table[:,i],kind='mergsort')

    gstat_table = gstat_table[sort_ind_gstat]
    back_sort_elements = np.argsort(element_numbers_centroids,kind = 'mergesort')
    element_numbers_centroids = element_numbers_centroids[back_sort_elements]
    gstat_table = gstat_table[back_sort_elements]

    #%% Saving the KKK-file for HGS:
    np.savetxt(output_file, np.transpose((element_numbers_centroids,gstat_table[:,-1],gstat_table[:,-1],gstat_table[:,-1])), fmt = '%d %.10e %.10e %.10e')

def invVertWeib(a, b, x0, xVect):
    """ Purpose: Transform gstat output to kkk values using the inverse Weibul
    distribution. From Claus Haslauer, 2014.
    Arguments:
        a:
        b:
        x0:
        xVect:
    Returns:
        Numpy array with transformed values.
    """
    invWeib = ( np.log( 1.0 - (xVect-x0) )/ (-a)  )**(1.0/b)
    return invWeib


def HGSBatchFile(nFieldsToSimulate): # Needs to be rewritten!!!
    #Defining working directories:
    workingDir = os.path.dirname(__file__)        # Path where the code is located
    myCodesDirectory = os.path.abspath(os.path.join(workingDir, '..', '_codes'))
    myGstatDirectory = os.path.abspath(os.path.join(workingDir, '..', 'Gstat'))
    myHGSDirectory = os.path.abspath(os.path.join(workingDir, '..', 'HGS'))
    MonteCarloFolder = 'MonteCarloHGS'

    #code_gstat2real = '04_Gstat_2_actualk_DEV.py'
    GstatBatch = "gstat_multrealiz.bat"
    k_file = 'kkk_synthetic_Gaus.dat'
    k_fileStrip = np.char.strip(k_file,'.dat')

    gstatfile = 'Gaussian_sim.txt'

    #nFieldsToSimulate = 500
    startAtNumber = 0
    time_between_sim = 12

    # Create the batch file for running HGS with all the different realizations:

    HGSbatchFile = 'letmerunPrecalSim.bat'

    fobj_HGS = open(os.path.join(myHGSDirectory,HGSbatchFile), 'w')
    fobj_HGS.write('@echo off')
    fobj_HGS.write( "\n")
    fobj_HGS.write( "\n")

    fobj_HGS.write('for /L %s in (1,1,%d) do (' %('%%i',nFieldsToSimulate))
    fobj_HGS.write('\n')
    fobj_HGS.write('del kkk_file.dat > nul')
    fobj_HGS.write('\n')
    fobj_HGS.write('copy %s\%s_%s.dat kkk_file.dat' %(os.path.join(myHGSDirectory,MonteCarloFolder),k_fileStrip,'%%i'))
    fobj_HGS.write('\n')

    fobj_HGS.write('grok')
    fobj_HGS.write('\n')
    fobj_HGS.write('phgs')
    fobj_HGS.write('\n')

    fobj_HGS.write('python  %s' %(workingDir + '\8_headsout_file_cal_transient.py'))
    fobj_HGS.write('\n')
    fobj_HGS.write('copy modeledheads.dat modeledheads_%s.dat' %('%%i'))
    fobj_HGS.write('\n')
    fobj_HGS.write('del modeledheads.dat > nul)')
    fobj_HGS.close()

    print('\nHello! I am ALSO done with the batch file for running %d HGS simulations!' %(nFieldsToSimulate))




