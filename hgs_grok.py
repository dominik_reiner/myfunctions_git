# -*- coding: utf-8 -*-
"""
Functions to deal with GROK and HGS
Created on Wed Mar 25 20:39:31 2015
@author: eesl
"""
# List of functions:

import os, time, pdb, numpy as np, datetime, shutil, subprocess as sp
import dir_manip as dm, grid_manip as gm, gen_fields as gf, kalman as klm


#Modified in the version of git: 14 de septiembre, 2015
def getstates(homedir, ii, mode, nnodes, elements, orig_elem_ind, elem_ind_fromorig, inner_gr_nodes = '', Y_i_upd = '', genfield = False, plot = False):
    """
    Update states(model measurements) with updated parameters using a forward model
    run for the corresponding realization. Give the parameter values NOT in LOG SCALE!!!
        Arguments:
        ----------
    homedir:
    ii:                 int, process index
    mode:               str, type of model run. 'fl_' or 'tr_'
    elements:           int, number of elements in the grid
    nnodes:             int, number of nodes in the grid
    orig_elem_ind:      int, indices of the original mesh grid
    elem_ind_fromorig:  int, new indices taken from the original mesh grid list (ind_from_orig)
    inner_gr_nodes:     int, new grid nodes (new_nodes)
    Y_i_upd:            np.array, parameter values of the model
    genfield:           bool, generate a gaussian field or not
    plot:               bool, plot the generated fields or not
        Returns:
        --------
    modmeas:      np.array of updated modeled measurements (or states). Fortran order ([no_meas*no_obsPoints] x 1 )
    Y:            np.array of model parameters. (no_elem x 1).
    """
    #%% Initialize necessary variables and directories:
    Noreal, dir_dict, fmt_string, curKFile = dm.init_dir(ii, mode, homedir)

    processesPath =         dir_dict['fld_allproc']
    mainPath =              os.path.abspath(os.path.join(processesPath,'..','..'))
    Model_data_dir =        os.path.join(mainPath,'Model_data')
    cur_processpath =       dir_dict['fld_curproc']

    cur_outputtimes =      '_outputtimes%sUpdate.dat'%mode
    headsFile1 =            'finalheadsFlow_%s.dat'%fmt_string
    randparam =             '_RandFieldsParameters.dat'
    randfile =               os.path.join(Model_data_dir, randparam)

    headfieldsPath =        dir_dict['fld_heads']
    ModObsPath =            dir_dict['fld_modobs']
    dest_kkkFolderBinary =  dir_dict['fld_kkkfiles']
    curDestDir =            dir_dict['fld_mode']

    files2keep = np.array(['.grok','.pfx','kkkGaus','.mprops','letme','wprops','finalheads','parallelindx','mesh', '.png'])

    #%% Zeroth Step: Check whether the process folder already exists
    # (needed when dealing with the initial ensemble), if not create it
    # If running transport it also should look to change initial heads file in GROK:
    dummyout = prepfwd(ii, mainPath, mode, headsFile = headsFile1)

    #%% First Step: Generate new kkk files to be used by GROK and HGS in the proper folder
    # (search for the process folder with corresponding index, if does not exist, then create it):
    #---------------------#
    assert os.path.exists(curDestDir), 'Something is wrong in the definition of the tree directory!'
    assert os.path.exists(Model_data_dir), 'Model data directory doesn´t exist or not in proper location!'

    #%% Decide whether to create random fields or not:
    # Load random field parameter data from the main file:
    xlen, ylen, zlen, nxel, nyel, nzel, xlim, ylim, zlim, Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z, elX_size, elY_size, elZ_size = gf.readParam(randfile)
    if genfield == True:
        # Generate the field: returns K in m/s
        Y = gf.genFields_FFT_3d(xlen, ylen, zlen, nxel, nyel, nzel, Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z, plot, mydir = curDestDir)
        # Binary kkk file
        binkkkfile = os.path.abspath(os.path.join(dest_kkkFolderBinary,curKFile.split('.')[0]))

    elif genfield == False:
        assert len(Y_i_upd) > 0, 'No parameters were provided!'
        Y = np.copy(Y_i_upd)

    # Read properties of the meshgrid -----> Provide this in the input arguments?
    #elements = int(dm.str_infile(os.path.join(curDestDir,'meshtecplot.dat'), 'E='))
    #nnodes = int(dm.str_infile(os.path.join(curDestDir,'meshtecplot.dat'), 'N='))
    #orig_elem_ind = np.arange(0,elements,1)

    #elem_ind_fromorig # Subtract a 1?
    #pdb.set_trace()
    if mode == 'fl_':   # Always expand the parameter vector for forward model run when flow
        Y = gm.expshr_param(Y, orig_elem_ind, elem_ind_fromorig, what= 'expand')

    if mode == 'tr_': # If transport, load the corresponding initial head field and store in cur_proc folder
        headfield = np.load(os.path.join(headfieldsPath,headsFile1.split('.')[0] + 'npy' ))
        headsnew = headfield[inner_gr_nodes-1] # This has not been checked
        np.savetxt(os.path.join(curDestDir, headsFile1), np.transpose((headsnew)))
    #pdb.set_trace()
    assert len(Y) == elements, 'There is a problem with parameter vector dimensions!'
    np.savetxt(os.path.join(curDestDir,curKFile), np.transpose((orig_elem_ind, Y, Y, Y)), fmt='%d %.6e %.6e %.6e')

    #%% Second Step: Run GROK and HGS :
    fwdHGS(curDestDir, fmt_string, mytype = mode)

    #%% Third step: Read new model outputs at specified output times, store them as bin files.
    # If runing flow, extract in addition final heads:
    all_modout = getmodout(curDestDir, mode = mode, outputtime = np.loadtxt(os.path.join(Model_data_dir,cur_outputtimes)))

    if mode == 'fl_': # Save final heads for future transport run
        headfile, dummypaths = dm.getdirs(curDestDir, mystr = 'head_pm', fullpath = True, onlylast = True)

        headfield, dummytime = readHGSbin(headfile, nnodes)
        np.save(os.path.join(headfieldsPath,headsFile1.split('.')[0] ), headfield)

    #%% Fourth: save stuff in the correspoding directories:
    np.save(os.path.join(ModObsPath,'mod%s_%s' %(mode, fmt_string)), all_modout)
    np.save(binkkkfile, np.transpose((Y)))   # Copy of the kkk-file for future sending session to the server
                                             # the vector stored is always expanded
    #%% Fifth: Remove unnecessary files:
    #commonName = 'head_pm'; fullPath = False; onlylastFile = True
    #myHeadsFileIn = dm.getCommonStrDirs(curDestDirFlow, commonName, fullPath, onlylastFile)
    dummypaths = dm.rmfiles(files2keep, fmt_string, cur_processpath )

    #%% Pre-process the model outputs (states) prior to return them.
    # Fortran convention is always used in this process:
    modmeas = all_modout.flatten('F')

    ##%% IF flow, shrink again the parameter vector to return always a reduced parameter vector. If working
    ## with in solute transport, the vector dimensions are not modified at all
    ##if mode == 'fl_':
    ##    Y = gm.expshr_param(Y, orig_elem_ind, elem_ind_fromorig, what= 'shrink')
    return modmeas #,Y# I might not even need to return Y, since I am not really modifying it here (just expanding vector if flow)

def fwdHGS(mydir, mystr, mytype = '' , hsplot = False):
    """
    Run GROK and HGS and if stated, run also hsplot
        Arguments:
        ----------
    mydir:          str, full path directory where GROK and HGS should be run
    mystr:          str, flag to print no. of realization in the cmd
    mytype:         str, flag to print flow or transport identifier in the cmd
    hsplot:         bool, (optional) if false, hspot is not executed
    """
    print('Process No. %s: running GROK (%s)... ' %(mystr, mytype))
    qq = sp.Popen('grok', shell=True, cwd=mydir, stdout=sp.PIPE, stderr=sp.PIPE)
    output_qq, error_qq = qq.communicate()
    print('Process No. %s: running PHGS (%s)... ' %(mystr, mytype))
    qq_2 = sp.Popen('phgs', cwd=mydir, stdout=sp.PIPE, stderr=sp.PIPE)
    output_qq_2, error_qq_2 = qq_2.communicate()

    if hsplot == True:
        print('Process No. %s: running HSPLOT(%s)... ' %(mystr, mytype))
        qq_3 = sp.Popen('hsplot', cwd=mydir, stdout=sp.PIPE, stderr=sp.PIPE)
        output_qq_3, error_qq_3 = qq_3.communicate()

    print('Succesful fwd model run, Process No. %s (%s)' %(mystr, mytype))

def rmodout(myfile, mytimes):
    """
    Support for the <getmodout> function (to extract model measurements)
        Arguments:
        ----------
    myfile:     str, fullpath with the name of the file to be loaded
    mytimes:    np array,  float or int array with times of interest
        Returns:
        --------
    mydata:     np.array, with model outputs at specific times (nmeas x 1)
    """

    try:
        myTime, mymodmeas = np.loadtxt(myfile, skiprows = 2, usecols = (0,1), unpack =True)
    except:
        print('Error reading model output file!')
        raise

    try: # If it is a single integer, add a dimension
        len(mytimes)
    except TypeError:
        mytimes = np.expand_dims(mytimes,1)

    # Mask to get only the elements of interest:
    return mymodmeas[np.where(np.in1d(myTime, mytimes))]

def getmodout(mypath, mode ='', outputtime = ''):
    """
    Read model output from a file at specific output times.
    Time values will be integers. Corrects negative concentrations when
    working in tranport mode.
        Arguments:
        ----------
    mypath:         str, directory where model outputs are located
    mode:           Str, 'fl_' or 'tr_'
    outputtime:     output times of interest
        Returns:
        --------
    all_modout:   2D np array, observed values at defined times
                   size of the array = [No times x No Obs Points]. Fortran order
    """
    assert outputtime != '', 'No output times provided'
    if not mode:
        print('Model outputs not extracted. No running mode provided')
    elif mode == 'fl_':
        mystrs = '_flow'
    elif mode == 'tr_':
        mystrs = '_conc'

    # Get a list of the files of all observation points:
    files, dummydir = dm.getdirs(mypath, mystr = mystrs, fullpath = True, onlylast = False)

    # Start collecting them in a single 2D array
    all_modout = np.array([])

    for ii in files:
        selMeas = rmodout(ii, outputtime.astype(np.int))
        all_modout = np.append(all_modout, selMeas) # All selected heads

    all_modout = np.reshape(all_modout, (-1, len(files)), order = 'F')

    # If transport mode, correct negative concentrations:
    if mode == 'tr_':
        # If the model gives negative concentrations, set all them to 0
        zero_id = np.where(all_modout < 0)
        all_modout[zero_id[0],zero_id[1]] = 0

    return all_modout

def readHGSbin(myfile, nnodes):
    """
    Read a HGS output bin file. The function has been tested with
    subsurface hydraulic heads and overland heads.
        Arguments:
        ----------
    myfile:     str, filename *full path(
    nnodes:     int, number of nodes in the mesh
        Returns:
        --------
    n_param:    np.array with the nodal parameter values
    time:       float, model time that corresponds to the param field values
    """
    with open(myfile, "rb") as f:
        pad = np.fromfile(f,dtype ='uint8', count = 4)
        time = np.fromfile(f,dtype ='uint8', count = 80)

        tt = [ ''.join(chr(t)) for t in time]
        time = float(''.join(tt))

        pad = np.fromfile(f,dtype = 'uint8',count = 4)
        pad = np.fromfile(f,dtype = 'uint8',count = 4)

        param = np.fromfile(f,dtype = 'float64', count = nnodes )
        pad = np.fromfile(f,dtype = 'uint8',count = 4)

    return param, time

def CreatePrefixBatch(mydir, myString):
    """
    Create a new Prefix file for running GROK and HGS
        Arguments:
        ----------
    mydir:      str, directory where the file should be located
    myString:   str, string to add to the prefix file
        Returns:
        --------
    The batch file batch.pfx with problem prefix
    """
    prefFile = 'batch.pfx'
    if prefFile in os.listdir(mydir):
        os.remove(os.path.join(mydir,prefFile))

    prefix = open(os.path.join(mydir,prefFile), 'w')
    prefix.write(myString)
    prefix.close()

def Createletmerun(mydir, myString, hsplot = False):
    """
    Create a letmerun batch file for running GROK, HGS and HSPLOT
        Argument:
        ---------
    mydir:      str, directory where the file should be located
    myString:   str, name of the GROK file to be executed
    hsplot:     bool, True: include instruction to run hsplot
                             False: just include grok and phgs
        Returns:
        --------
    The batch file letmerun.bat
    """
    batchfile = 'letmerun.bat'
    if batchfile in os.listdir(mydir):
        os.remove(os.path.join(mydir, batchfile))

    letmerun = open(os.path.join(mydir, batchfile), 'w')
    letmerun.write('@echo off\n\ndel *' + myString + 'o* > nul\n\n')
    letmerun.write('grok\nphgs\n')

    if hsplot == True:
        letmerun.write('hsplot\n')

    letmerun.close()

def prepfwd(ii, homedir, mymode, headsFile = ''):
    """
    Prepare folder for forward model run. Meaning: copy the source folder with
    all its files, into a folder that includes process number id (number of realization)
        Arguments:
        ----------
    ii:             int, process id
    homedir:        str, home directory of the whole project
    mymode:         str, fl_ or tr_, defines flow or transport
    headsFile:      str, name of the final(from heads) or initial(for transport) heads
        Returns:
        --------
    Creates corresponding directories for a forward model run
    """

    #%% Create process folder if does not exist:
    Noreal, dir_dict, fmt_string, curKFile = dm.init_dir(ii, mymode, homedir)
    SourceDir = os.path.abspath(os.path.join(homedir, mymode + 'ToCopy'))

    # Dictionary of files which name is to be updated:
    if mymode == 'fl_':
        StrFiles2Replace = {'kkkGaus.dat': curKFile}
    elif mymode == 'tr_':
        StrFiles2Replace = {'kkkGaus.dat': curKFile,'finalheadsFlow.dat':headsFile}

    prepareGROK(SourceDir, dir_dict['fld_mode'], mymode, fmt_string, StrFiles2Replace)

    return Noreal, dir_dict, fmt_string, curKFile

def prepareGROK(SourceDir, DestDir, mymode, fmt_string, StrFiles2Replace, hsplot = False):
    """
    Support for the <prepfwd>, this is the function that does copy the folder. It copies all
    dependancies, adds an identifier to each file, creates a batch file with proper prefix
    to make GROK run automatically, and replace corresponding file-names within GROK file
        Arguments:
        ----------
    SourceDir:          str, folder to be copied (full path)
    DestDir:            str, destination directory
    mymode:             str, fl_ or tr_, defines flow or transport
    fmt_string:         str, Identifier to assign to each new copied folder
    StrFiles2Replace:   dict, files to be replaced within GROK (eg. {'kkk_file.dat': cur_kkkFile,'InitialHeads.head_pm.001': 'iniHeads_%s.head_pm'}) Not full path
    hsplot:             bool, default is False: not include hsplot in the batchfile sequence
        Returns:
        --------
    Creates a brand new folder with the necessary files to make a forward HGS run using a
    current KKK field
    """
    #%% Copy directories and files:
    try:
        shutil.copytree(SourceDir, DestDir)
    except:
        myfiles = os.listdir(SourceDir)
        for item in myfiles:
            try:
                shutil.copy2(os.path.join(SourceDir, item), os.path.join(DestDir, item))
            except:
                pass

    #%% Create a new GROk file with the KKK file updated:
    new_str = mymode + 'sim' + ('_%s.grok' % fmt_string)
    In_grokFile = os.path.join(DestDir, mymode + 'sim' + '.grok')
    Out_grokFile = os.path.join(DestDir, new_str)

    # %% Copy heads file if working with transport:
    # No!!! This will be done in the get states function, here just the name
    # is replaced in the GROK file

    InFile = open(In_grokFile).read()
    OutFile = open(Out_grokFile, 'w')
    for i in list(StrFiles2Replace.keys()):
        InFile = InFile.replace(i, StrFiles2Replace[i])

    OutFile.write(InFile)
    OutFile.close()
    #InFile.close()
    os.remove(In_grokFile)

    #%% Create the prefix and "let me run " batch files with the proper string:
    CreatePrefixBatch(DestDir, new_str.split('.')[0])
    Createletmerun(DestDir, new_str.split('.')[0])









def readFieldparamBin(myPath):
    """ Read parameter fields from binary files
    Arguments:
        myPath:     Str, direcotry where the information is stored
        my_ID:      Str, what to extract, "param", "heads" or "conc"
    Returns:
        nel:        Integer, number of elements of the model grid
        nreal:      Integer, number of realizations in the ensemble
        Y:          Numpy array, 2D Array with the proper data
    """
    myFiles = os.listdir(myPath)
    bb = 0

    for kk in range(0, len(myFiles)):

        cur_file = os.path.join(myPath,myFiles[kk])

        if 'kkk' in cur_file:
            if bb == 0:
                Y = np.log(np.load(cur_file))
                bb = bb+1
            else:
                kdata = np.log(np.load(cur_file))
                Y = np.c_[Y,kdata]                          # Parameter field matrix (nel,nreal)

    nel     = Y.shape[0]                        # Number of elements in the domain
    nreal   = Y.shape[1]                        # Number of realizations in the Ensemble

    return nel, nreal, Y

def readModelOutputsBin(myPath, heads, concentrations):
    """ Read Modeled outputs from binary files. Either heads or concentrations.
    Arguments:
        myPath:             Str, directory where the information is stored
        heads:              Boolean, read heads or not
        concentrations:     Boolean, read concentration data or not
    Returns:
        nmeas:              Integer, number of measurements
        modOut_final:       Numpy array, modeled measurement values (ndata x nrealizations)
        numheads:           Integer, number of head values
        numconc:            Integer, number of concentration values
    Note: The matrices of measurements are flattened following Fortran
        convention (column-wise or column major). If work with both types of
        measurements, heads are listed first and then concentrations
    """
    myFiles = os.listdir(myPath)
    hh = 0; cc = 0; dd = 0

    for kk in range(0, len(myFiles)):
        cur_file = os.path.join(myPath,myFiles[kk])
        if heads == True:
            if 'modheads' in cur_file:
                if hh == 0:
                    modOut = np.load(cur_file).flatten('F')
                    hh = hh + 1
                else:
                    myheads= np.load(cur_file).flatten('F')
                    modOut = np.c_[modOut,myheads]

        if concentrations == True:
            if 'modconc' in cur_file:
                if cc == 0:
                    modOut2 = np.load(cur_file).flatten('F')
                    cc = cc + 1
                else:
                    myconc= np.load(cur_file).flatten('F')
                    modOut2 = np.c_[modOut2,myconc]

    if (concentrations == True) and (heads == True):
        modOut_final = np.r_[modOut,modOut2]

    if heads == True: numheads = len(myheads)
    if concentrations == True: numconc = len(myconc)

    if heads == False:
        modOut_final = modOut2
        numheads = 0
    if concentrations == False:
        modOut_final = modOut
        numconc = 0
    nmeas   = modOut_final.shape[0]                   # Number of observations

    return nmeas, modOut_final, numheads, numconc

def syncModel2Data(heads, concentrations):
    """ Synchronize the number of model outputs with the number of
    time-steps-updates to perform
    Arguments:
        heads:          Boolean, include head data or not
        concentrations: Boolean, include conc. data or not
    Returns:
        len(cur_heads_time):    Zero if heads == False
        len(cur_conc_time):     Zero if concentration == False
        curTimes:               Number of CURRENT output times dealing with
    Dependencies:
        dm.myMainDir()
    """
    myDir = dm.myMainDir()
    KalmanFilterPath = 'kalmanfilter'


    if heads == True:
        cur_heads_time = np.loadtxt(os.path.join(myDir, '..', KalmanFilterPath,'Model_data','_outputtimesFlowUpdate.dat'))
        try:
            len(cur_heads_time)
        except:
            cur_heads_time = np.expand_dims(cur_heads_time, 1)

    if concentrations == True:
        cur_conc_time = np.loadtxt(os.path.join(myDir, '..', KalmanFilterPath,'Model_data', '_outputtimesTransUpdate.dat'))
        try:
            len(cur_conc_time)
        except:
            cur_conc_time = np.expand_dims(cur_conc_time, 1)

    #%% Synchronize the number of model outputs with the number
    #  of time-steps-updates to perform:
    try:
        curTimes = len(cur_heads_time) + len(cur_conc_time)
    except:
        if heads == True:
            curTimes = len(cur_heads_time)
        if concentrations == True:
            curTimes = len(cur_conc_time)

    try:
        return len(cur_heads_time), len(cur_conc_time), curTimes
    except:
        if heads == True:
            return len(cur_heads_time), 0, curTimes
        if concentrations == True:
            return 0, len(cur_conc_time), curTimes



def writeinGrok(myInDir, myOutDir, myoutfile, probdescFile, grid_coordFile, gridmethod,
    Transient, Transport, Echo2Output, FD, CV, porousfile, kfile, heads_init, transport_init, heads_bc,
    TimeStepCtrl, well_propsFile, pumpwellsFile, tracer_injFile, outputimesFile, obswellsFile):
    """ Type in the main parts of a grok file
    Arguments:
        myInDir:        Str, directory where input files are located
        myOutDir:       Str, directory where GROK file is/will be located
        myoutfile:      Str, name assigned to the GROK file
        probdescFile:   Str, filename containing description of the project
        grid_coordFile: Str, filename containing grid coordinates
        gridmethod:     Str, method to generate grid-'HGS_uniform' 'HGS_grade' 'GB_file'
        Transient:      Boolean, run transient mode or not
        Transport:      Boolean, run transport mode or not
        Echo2Output:    Boolean, create echo output file or not
        FD:             Boolean, finite difference mode or not
        CV:             Boolean, control volume mode or not
        porousfile:     Str, porous media properties file
        kfile:          Str, KKK filename for homogeneous fields, if homogeneous type ''
        heads_init:     Tuple [mode, arg]:
                        mode:'output',arg: string file name
                        mode: 'onevalue', arg: number
                        mode: 'raster', arg: string file name
                        mode:'txt', arg: string file name
        transport_init: Tuple [mode, arg]
                        mode:'output',arg: string file name
                        mode: 'onevalue', arg: number.
                        If transport is False, just type ''
        heads_bc:       tuple [X1, X2], X corrdinate of the X plane to be selected to assign constant head equals to initial value.
                        tuple ['all', 'head equals initial']: all heads stay the same
                        (In the future should support Y and Z planes and possibility
                        to assign boundary conditions from GB files)
        TimeStepCtrl:   Boolean, et the default time step controlers or not
        well_propsFile: Str, filename containing well construction details
        pumpwellsFile:  Str, filename containing well coordinates, rates, etc.
        tracer_injFile: Str, filename containing tracr injection data, if Transport is False, type ''
        outputimesFile: Str, filename containing defined output times
        obswellsFile:   Str, filename containing observation well information

    Returns:
        A new grok file
    """
    #%% PROBLEM DESCRIPTION:
    # Read problem description content from file:
    with open (os.path.join(myInDir,probdescFile), "r") as myfile:
        banner = myfile.read()#.replace('\n', '')
    # Generate the grok file:
    Ofile = open(os.path.join(myOutDir,myoutfile),'w') # Generate the file
    Ofile.write(banner %(datetime.datetime.now())) #%(time.strftime("%d/%m/%Y"))
    Ofile.flush()
    del banner
    print('<Problem description> written ok...')

    #%% GRID GENERATION: gb or directly in HGS
    banner = '\n\n!---  Grid generation\n'
    Ofile.write('%s'%banner)
    del banner
    if gridmethod == 'HGS_grade':
        Ofile.write('generate blocks interactive\n')
        try:
            dim = ['x','x','x','y','y','y','z']
        except:
            dim = ['x','y','z']

        try:
            coordinates = np.loadtxt(os.path.join(myInDir,grid_coordFile))
        except IOError('Unable to load instructions'):
            raise

        if len(dim) != len(coordinates):
            raise IndexError('number of lines in coordinate file must be equal to length of dim vector')

        for cur_dim in range(0,len(dim),1):
            Ofile.write('grade %s\n' %dim[cur_dim])

            for cur_val in range(0,coordinates.shape[1],1):
                Ofile.write('%.8e\t'%coordinates[cur_dim,cur_val])
            Ofile.write('\n')

        Ofile.write('end generate blocks interactive\nend\n')
        Ofile.write('Mesh to tecplot\nmeshtecplot.dat\n')

    elif gridmethod == 'HGS_uniform':
        print('Still under construction')
    elif gridmethod == 'gridbuilder':
        print('Still under construction')
    else:
        ValueError('Unsupported method to generate grid...')
    Ofile.flush()
    print('<Grid generation> written ok...')

   #%% SIMULATION PARAMETERS GROK:
    units = 'units: kilogram-metre-second'
    banner = '\n\n!---  General simulation parameters\n'

    Ofile.write('%s'%banner)
    Ofile.write('%s\n'%units)

    if FD == True: Ofile.write('Finite difference mode\n')
    if CV == True: Ofile.write('Control volume\n')
    if Transient == True: Ofile.write('Transient flow\n')
    if Transport == True: Ofile.write('do Transport\n')
    if Echo2Output == True: Ofile.write('Echo to output\n')

    #%% POROUS MEDIA PROPERTIES:
    Ofile.write('\n!--- Porous media properties\n')
    Ofile.write('use domain type\nporous media\nproperties file\n')
    Ofile.write('./%s\n\nclear chosen zones\nchoose zones all\nread properties\nporous medium\n'%(porousfile))

    if len(kfile) > 0:
        Ofile.write('\nclear chosen elements\nchoose elements all\nRead elemental k from file\n./%s\n'%(kfile))

    if Transport == True:
        name = 'fluorescein'
        difCoef = 1.000e-10
        Ofile.write('\n!--- Solute properties\n')
        Ofile.write('solute\n name\n %s\n free-solution diffusion coefficient\n %.3e\nend solute\n'%(name,difCoef))

    Ofile.flush()
    print('<Simulation param and porous media written ok...')

    #%% INITIAL CONDITIONS:
    del banner
    banner = 'clear chosen nodes\nchoose nodes all\n'
    Ofile.write('\n!--- IC\n!--- Flow:\n%s' %banner)

    if heads_init[0] == 'output':
        try:
            Ofile.write('initial head from output file\n')
            Ofile.write('%s\n' %(heads_init[1]))
        except TypeError('Innapropiate argument type in heads'):
            raise
    elif heads_init[0] == 'onevalue':
        Ofile.write('Initial head\n')
        try:
            Ofile.write('%.3e\n' %(heads_init[1]))
        except TypeError('Innapropiate argument type in heads'):
            raise
    elif heads_init[0] == 'raster':
         try:
            Ofile.write('Map initial head from raster\n')
            Ofile.write('%s\n' %(heads_init[1]))
         except TypeError('Innapropiate argument type in heads'):
            raise
    elif heads_init[0] == 'txt':
        try:
            Ofile.write('Initial head from file\n')
            Ofile.write('%s\n' %(heads_init[1]))
        except TypeError('Innapropiate argument type in heads'):
            raise

    if Transport == True:
        if transport_init[0] == 'onevalue':
            Ofile.write('\n!--- Transport:\n%s' %banner)
            Ofile.write('Initial concentration\n%.3e' %transport_init[1])

        elif transport_init[0] == 'output':
            try:
                Ofile.write('Initial concentration from output file\n')
                Ofile.write('%s\n' %(transport_init[1]))
            except TypeError('Innapropiate argument type in transport_init'):
                raise

    Ofile.flush()
    print('<Initial conditions written ok...')

    #%% BOUNDARY CONDITIONS:
    del banner
    banner = 'clear chosen nodes\nuse domain type\nporous media\n'
    Ofile.write('\n\n!--- BC\n!--- Flow:\n%s' %banner)

    if heads_bc[0] == 'all':
            Ofile.write('\nchoose nodes all\n')
    else:
        Ofile.write('\nchoose nodes x plane\n%.8e\n1.e-5\n'%(heads_bc[0]))
        Ofile.write('\nchoose nodes x plane\n%.8e\n1.e-5\n'%(heads_bc[1]))

    Ofile.write('\ncreate node set\nconstant_head_boundary\n')
    Ofile.write('\nBoundary condition\n type\n head equals initial\n node set\n constant_head_boundary\nEnd\n')

    Ofile.flush()
    print('<Boundary conditions written ok...')

    #%% TIME STEP CONTROLS:
    if TimeStepCtrl == True:
        flowsolver_conv_= 1.0e-6
        flowsolver_max_iter = 500
        #head_control = 0.01
        initial_timestep = 1.0e-5
        min_timestep = 1.0e-9
        max_timestep = 100.0
        max_timestep_mult = 2.0
        min_timestep_mult = 1.0e-2
        underrelax_factor = 0.5

        Ofile.write('\n!--- Timestep controls\n')
        Ofile.write('flow solver convergence criteria\n%.2e\n' %(flowsolver_conv_))
        Ofile.write('flow solver maximum iterations\n%d\n' %(flowsolver_max_iter))
        #Ofile.write('head control\n%.2e\n'%(head_control))
        Ofile.write('initial timestep\n%.2e\n' %(initial_timestep))
        Ofile.write('minimum timestep\n%.2e\n' %(min_timestep))
        Ofile.write('maximum timestep\n%.2e\n' %(max_timestep))
        Ofile.write('maximum timestep multiplier\n%.2e\n'%(max_timestep_mult))
        Ofile.write('minimum timestep multiplier\n%.2e\n' %(min_timestep_mult))
        Ofile.write('underrelaxation factor\n%.2f\n' %(underrelax_factor))
        Ofile.write('No runtime debug\n\n')
        Ofile.flush()
        print('<Default time step controllers written ok...')

    #%% WELL DEFINITION:

    if pumpwellsFile:
        Ofile.write('\n!--- Pumping wells\n\n')
        try:
            wells_data =  np.genfromtxt(os.path.join(myInDir,pumpwellsFile), comments="#", delimiter = '',dtype = '|S12, f8, f8, f4, f4, f4, f8, f8, f8, f8' )
            len(wells_data)
        except (IOError, TypeError):
            print('There was an error with the <wells> file')
            print('File must contain at least two lines, if only one entrance, just duplicate it')
            Ofile.flush()
            Ofile.close()
            raise

        if wells_data[0] == wells_data[1]:
            length_wellsdata = 1
        else:
            length_wellsdata = len(wells_data)

        for ii in range(0,len(wells_data)):
            wellID = wells_data[ii][0]
            X = wells_data[ii][1]
            Y = wells_data[ii][2]
            top = wells_data[ii][3]
            bot = wells_data[ii][4]
            ext_point = wells_data[ii][5]
            pumprate_ini = wells_data[ii][6]
            pumprate_end = wells_data[ii][7]
            time_ini = wells_data[ii][8]
            time_end = wells_data[ii][9]

            Ofile.write('!-Well: %s\nuse domain type\nwell\nproperties file\n%s\n\n' %(wellID,well_propsFile))
            Ofile.write('clear chosen zones\nclear chosen segments\nchoose segments polyline\n2\n')
            Ofile.write('%s, %s, %s ! xyz of top of well\n' %(X,Y,top))
            Ofile.write('%s, %s, %s ! xyz of bottom of well\n' %(X,Y,bot))
            Ofile.write('new zone\n%d\n\n' %(ii+1))
            Ofile.write('clear chosen zones\nchoose zone number\n%d\n' %(ii+1))
            Ofile.write('read properties\ntheis well\nclear chosen nodes\nchoose node\n%s, %s, %s\n' %(X,Y,ext_point))
            Ofile.write('create node set\n %s\n\nboundary condition\n  type\n  flux nodal\n\n  name\n  %s\n  node set\n  %s\n' %(wellID,wellID,wellID))
            Ofile.write('\n  time value table\n  %f   %f\n  %f   %f\n end\nend\n\n' %(time_ini, pumprate_ini, time_end, pumprate_end))

        Ofile.flush()
        print('<Pumping wells written ok...')
    if not pumpwellsFile:
        print('<Pumping wells not included in grok>')

    #%% TRACER INJECTION:
    if Transport == True:
        Ofile.write('\n!--- Tracer injection\n\n')

        try:
            tracer_data =  np.genfromtxt(os.path.join(myInDir,tracer_injFile), comments="#", delimiter = '',dtype = 'f8, f8, f4, f4, f4, f4' )
            len(tracer_data)
        except:
            print('File must contain at least two lines, if only one entrance, just duplicate it')
            print('There was an error with the Tracer Injection File')
            Ofile.flush()
            Ofile.close()
            raise
        if tracer_data[0] == tracer_data[1]:
            length_injdata = 1
        else:
            length_injdata = len(tracer_data)

        for ii in range(0,length_injdata):
            X = tracer_data[ii][0]
            Y = tracer_data[ii][1]
            Z = tracer_data[ii][2]
            time_ini = tracer_data[ii][3]
            time_end = tracer_data[ii][4]
            massflux = tracer_data[ii][5]

            Ofile.write('clear chosen zones\nclear chosen segments\nclear chosen nodes\n\nuse domain type\nporous media\n')
            Ofile.write('\nchoose node\n')
            Ofile.write('%s, %s, %s\n' %(X,Y,Z))
            Ofile.write('\nspecified mass flux\n1\n%s,%s,%s\n\n' %(time_ini,time_end,massflux))

        Ofile.flush()
        print('<Tracer injection written ok...')

    #%% OUTPUT TIMES:
    if outputimesFile:
        try:
            times = np.loadtxt(os.path.join(myInDir,outputimesFile))
        except IOError:
            print('Check the directories of the output-times file')
            raise
        Ofile.write('!--- Outputs\noutput times\n')
        for ii in range(0,len(times)):
            Ofile.write('%s\n' %times[ii])

        Ofile.write('end\n' %times[ii])
        Ofile.flush()
        print('<Output times written ok...')
    if not outputimesFile:
        print('<No output times written in grok...>')

        #%% OBSERVATION WELLS:
    if obswellsFile:
        try:
            obs_wellsData = np.genfromtxt(os.path.join(myInDir,obswellsFile), comments="#", delimiter = '',dtype = '|S12, f8, f8, f4' )
            len(obs_wellsData)
        except (IOError, TypeError,ValueError,OSError):
            print('Check the directories of the grok and output-times files')
            print('observation wells file must contain at least two lines, if only one entrance, just duplicate it')
            raise

        Ofile.write('\n!--- Observation wells\nuse domain type\nporous media\n\n')
        if obs_wellsData[0] == obs_wellsData[1]:
            length_obsData = 1
        else:
            length_obsData = len(obs_wellsData)

        for ii in range(0,length_obsData):
            ID = obs_wellsData[ii][0]
            X = obs_wellsData[ii][1]
            Y = obs_wellsData[ii][2]
            Z = obs_wellsData[ii][3]

            Ofile.write('make observation point\n')
            Ofile.write('%s\n%f  %f  %f\n' %(ID,X,Y,Z))
        Ofile.flush()
        print('<Observation wells written ok...')

    if not obswellsFile:
        print('<No observation wells written in grok...>')

    Ofile.close()
    print('GROK file finished!')

def addtime2GROK(zz, myiter, mode, subtract = False):
    """ Add/subtract new output times to GROK. The times are taken from the
    '_outputtimesFlowUpdate.dat' and/or '_outputtimesTransUpdate.dat'
    Arguments:
        zz:     Integer, number of realization (or process)
        mode:   Str, 'flow' or 'transport'
        subtract: Str, if it is wanted to delete last time step from grok (for restart mode). Default False
    Returns:
        GROK file with added time step
    """
    KalmanFilterPath = 'kalmanfilter'
    fmt_string = '%.5d_%s' %(zz+1, myiter)
    # Main directories:
    funcPath = dm.myMainDir()
    processPath = os.path.join(funcPath,'..', KalmanFilterPath, 'Realizations', 'AllProcesses','process_%s' %fmt_string)
    modelDataPath = os.path.join(funcPath,'..', KalmanFilterPath, 'Model_data')

    # First deal with flow:
    if mode == 'flow':
        grokStr = 'Flowsim_%s.grok' %fmt_string
        mymode = 'fl_%s' %fmt_string
        myTimesFile = '_outputtimesFlowUpdate.dat'
    elif mode == 'transport':
        grokStr = 'Transpsim_%s.grok' %fmt_string
        mymode = 'tr_%s' %fmt_string
        myTimesFile = '_outputtimesTransUpdate.dat'

    myGROKfile = os.path.join(processPath, mymode, grokStr)
    mytimes = np.loadtxt(os.path.join(modelDataPath, myTimesFile))

##    if subtract == True:
##        mytimes = mytimes[:-1]

    outputFile = os.path.join(processPath, mymode, myGROKfile + 'temp')


    writing = True
    with open(myGROKfile) as f:

        with open(outputFile,'w') as out:
            for line in f:
                if writing:
                    if "output times" in line:
                        writing = False
                        out.write("output times")
                        for ii in range(0, len(mytimes)):
                            out.write('\n%s' %(mytimes[ii]))
                        buffer = [line]
                        out.write("\nend\n")
                    else:
                        out.write(line)
                elif "end" in line:
                    writing = True
                else:
                    buffer.append(line)
            else:
                if not writing:
                    #There wasn't a closing "event", so write buffer contents
                    out.writelines(buffer)
            out.close()
            f.close()
    os.remove(myGROKfile)
    os.rename(outputFile, myGROKfile)

def timeStepUpdate(heads, concentrations, update_heads, update_concentrations, subtract = False):
    """ Add/subtract a time value in the outputtimes_xxx.dat files
    Arguments:
        heads:                  Boolean, add time step to heads or not
        concentrations:         Boolean, add time step to concentrations or not
        update_heads:           Integer, how many time steps should be added to heads
        update_concentrations:  Integer, how many time steps should be added to concentrations
        subtract:               Str, if it is wanted to delete last time step from file (for restart mode). Default False
    Returns:
        Length of the current times being analyzed
    """
    KalmanFilterPath = 'kalmanfilter'

    myCodeDir =  dm.myMainDir()
    myPath = os.path.join(myCodeDir,'..',KalmanFilterPath,'Model_data')

    if heads == True:
        AllTimes = np.loadtxt(os.path.join(myPath, '_outputtimesFlow.dat'))
        len_curTimes = len(np.loadtxt(os.path.join(myPath, '_outputtimesFlowUpdate.dat')))

        if subtract == False:
            len_curTimes += update_heads
            with open(os.path.join(myPath,'_outputtimesFlowUpdate.dat'), 'a') as mytext:
                mytext.write('\n%s' %AllTimes[len_curTimes - 1]) # to correct the zero index from python
            mytext.close()
        elif subtract == True:
            len_curTimes -= update_heads
            w = open(os.path.join(myPath,'_outputtimesFlowUpdate.dat'), 'w')
            for zz in range(0,len_curTimes):
                if zz < len_curTimes-1: # -1 is to avoid writting an empty line
                    w.write('%s\n' %AllTimes[zz])
                else:
                    w.write('%s' %AllTimes[zz])
            w.close()

    if concentrations == True:
        AllTimes1 = np.loadtxt(os.path.join(myPath, '_outputtimesTrans.dat'))
        len_curTimes1 = len(np.loadtxt(os.path.join(myPath, '_outputtimesTransUpdate.dat')))


        if subtract == False:
            len_curTimes1 += update_concentrations
            with open(os.path.join(myPath,'_outputtimesTransUpdate.dat'), 'a') as mytext:
                mytext.write('\n%s' %AllTimes1[len_curTimes1 - 1]) # to correct the zero index from python
            mytext.close()
        if subtract == True:
            len_curTimes1 -= update_concentrations
            w = open(os.path.join(myPath,'_outputtimesTransUpdate.dat'), 'w')
            for zz in range(0,len_curTimes1):
                if zz < len_curTimes1-1: # -3 is to avoid writting an empty line
                    w.write('%s\n' %AllTimes1[zz])
                else:
                    w.write('%s' %AllTimes1[zz])
            w.close()

    print('Times updated...')

    if (heads == True) and (concentrations == True):
        return len_curTimes + len_curTimes1
    elif (concentrations == False) and (heads == True):
        return len_curTimes
    elif (concentrations == True) and (heads == False):
        return len_curTimes1



def setParam2value():
    print('construction...')