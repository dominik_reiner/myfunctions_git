import numpy as np, os
import hgs_grok as hg
import grid_manip as gm
import gen_fields as gf

xlen, ylen, zlen, nxel, nyel, nzel, xlim, ylim, zlim, Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z, elX_size, elY_size, elZ_size = gf.readParam(r'C:\PhD_Emilio\Emilio_PhD\_CodeDev_bitbucket\kalmanfilter_git\Model_data\_RandFieldsParameters.dat')

elem_ind_fromorig, new_nodes = gm.sel_grid(r'C:\PhD_Emilio\Emilio_PhD\_CodeDev_bitbucket\kalmanfilter_git\fl_ToCopy\meshtecplot.dat', 36057, 30000, elX_size, elY_size, elZ_size, xlim, ylim, zlim)

mymeas = hg.getstates(r'C:\PhD_Emilio\Emilio_PhD\_CodeDev_bitbucket\kalmanfilter_git', 4, 'fl_', 36057, 30000, np.arange(1,30001,1), elem_ind_fromorig, inner_gr_nodes = new_nodes, Y_i_upd = '', genfield = True, plot = True)